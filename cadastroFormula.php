<?php
      SESSION_START();
	include "conexao/dbConexao.php";
      include "utils/funcoes.php";  

      $_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
      
      $dadosCampeonato = RetornaCampeonatoAtivo($conexao);
	
      $campeonatoId = $dadosCampeonato[0];   
      $campeonatoDescricao = $dadosCampeonato[4];         
            
      $mensagem = "";
      $tipoAviso = "";
      
      if(isset($_GET['msg'])){
            $mensagem = $_GET['msg'];
      }
      if (isset($_GET['tipoAviso'])) {
            $tipoAviso = $_GET['tipoAviso'];
      } 
      
      // inicializa valores
	$formulaId = "";
	$descricao = "";
	$quantidadeTimes = "";
	$apelido = "";
	$foto = "sem_imagem.png";

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		if ($id != "") {
			$sql = "SELECT * FROM formulas WHERE id = '" . $id . "' ";
			
			$rs=$conexao->query($sql);
			$reg=mysqli_fetch_array($rs);
			
			$formulaId = $id;
			$descricao = $reg['descricao'];
			$quantidadeTimes = $reg['quantidadeTimes'];
		} 		
	}		

	$sql="SELECT
			id, 
			descricao, 
			quantidadeTimes
		FROM 
			formulas
		ORDER BY
			descricao ";
	
	$rs=$conexao->query($sql);
?>      

<!DOCTYPE HTML>

<html>
	<head>
		<title>Cadastro de Fórmulas</title>
            <link rel="icon" type="image/png" href="imagens/favicon.png">
            <meta name="robots" content="noindex">
		<meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="assets/css/tabs.css" />
            <link rel="stylesheet" href="assets/css/modal.css" />   

	</head>

	<body>
		<div id="page-wrapper">
                  <div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
                  </div>
                  
                  <div id="main">
				<div class="container">                       
                        
                              <?php include ("componentes/mensagem.php") ?>
                              
                              <div class="row main-row">
                                    <div class="12u">

                                          <section class="content-header">
                                                <h2 class="tituloPagina">Cadastrar ou editar fórmulas</h2>				
                                          </section>

                                          <section class="cadastro"> 
                                                <form class="contact_form" method="post" action="paginas/cadastroFormula1.php" enctype="multipart/form-data">  
                                                      <div class="row2">  <!-- ID -->
                                                            <div class="col-25">
                                                                  <label for="fname">ID</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" class="desabilitado" id="formulaId" name="formulaId" readonly value="<?php print $formulaId; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Descrição -->
                                                            <div class="col-25">
                                                                  <label for="descricao">Descrição</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" id="descricao" name="descricao" name="descricao" value="<?php print $descricao; ?>" required> 
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Quantidade de times -->
                                                            <div class="col-25">
                                                                  <label for="quantidadeTimes">Quantidade de Times</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="quantidadeTimes" name="quantidadeTimes" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $quantidadeTimes; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Botões -->
                                                            <div class="col-25">
                                                                  <label for="data"></label>
                                                            </div>
                                                            <div class="col-75">                                                                
                                                                  <div class="botoes">
                                                                        <button type="submit" id="submit" class="btnSalvar" name="acao" value="inc">Salvar</button>

                                                                        <button type="submit" class="btnExcluir" name="acao" value="exc">Excluir</button>

                                                                        <button type="button" class="btnCancelar" onClick="Nova('cadastroFormula.php')" >Cancelar</button>
                                                                  
                                                                        <?php
                                                                        if(!$formulaId == "") { ?>							
                                                                              <a class="btn" href="cadastroFormulaJogos.php?id=<?PHP print $formulaId; ?>">
                                                                              <i class="fa fa-flask" style="margin: 0 2px 0 15px;"></i> Inserir fórmula</a> <?php
                                                                        
                                                                        } ?>	
                                                                  </div>
                                                            </div>
                                                      </div>                          
                                                      
				                        </form>   
                                          </section>

                                          <div class = "box tabela">                            
                                                <div class="box box-solid box-success">   
                                                      <div class="box-body">
                                                            <div class="box-body no-padding">
                                                                  <table class="table tabela1" >
                                                                        <thead class="thead-default" >
                                                                              <tr class="cabecalhoTabela fundoCadastro">
                                                                                    <th class="linhaCabecalho">ID</th>
                                                                                    <th class="linhaCabecalho">Descrição</th>
                                                                                    <th class="linhaCabecalho">Quantidade de Times</th>
                                                                              </tr> 
                                                                        </thead> 

                                                                        <?PHP
                                                                        if (isset($rs)) {
                                                                              while($reg=mysqli_fetch_array($rs)) { 
                                                                                    $id = $reg["id"];
                                                                                    $descricao = $reg["descricao"];
                                                                                    $quantidadeTimes = $reg["quantidadeTimes"];?>														
                                                                                                                  
                                                                                    <tr onclick="location.href = 'cadastroFormula.php?id=<?PHP print $id;?>'; " 
                                                                                          style='cursor: pointer;'> 
                                                                                          
                                                                                          <td class="linha"><?PHP print $id; ?></td>
                                                                                          <td class="linha"><?PHP print $descricao; ?></td>
                                                                                          <td class="linha"><?PHP print $quantidadeTimes; ?></td>
                                                                                    </tr>	 <?PHP 
                                                                              } 
                                                                        } else { ?>                                         
                                                                              <tr>       
                                                                                    <td class="linha">&nbsp;</td>                  
                                                                                    <td class="linha">&nbsp;</td>
                                                                                    <td class="linha">&nbsp;</td>
                                                                              </tr>	<?php
                                                                        } ?>                        
                                                                  </table>
                                                            </div>
                                                      </div> 
                                                </div>   
                                          </div>  
                                          
                                    </div>
					</div>
				</div>
                  </div>     
            </div>
            
            <div id="footer-wrapper">
                  <?php include("componentes/rodape.php") ?>
            </div>

            
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/skel-viewport.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/funcoes.js"></script>
            <script src="assets/js/main.js"></script>     
          
	</body>
</html>