<?php

function InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id){	
	$nome_imagem="";

	// Se a foto estiver sido selecionada
	if (!empty($foto["name"]))
	{
		//$foto = $_FILES['foto'];		
		// Largura máxima em pixels
		$largura = 1500;
		// Altura máxima em pixels
		$altura = 1125;
		// Tamanho máximo do arquivo em bytes
		$tamanho = 1000000;

		$error="";
 
		// Verifica se o arquivo é uma imagem
		if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])){
		   $error[1] = "Isso não é uma imagem.";
		} 
	
		// Pega as dimensões da imagem
		$dimensoes = getimagesize($foto["tmp_name"]);
	
		// Verifica se a largura da imagem é maior que a largura permitida
		if($dimensoes[0] > $largura) {
			$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}
 
		// Verifica se a altura da imagem é maior que a altura permitida
		if($dimensoes[1] > $altura) {
			$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}
		
		// Verifica se o tamanho da imagem é maior que o tamanho permitido
		if($foto["size"] > $tamanho) {
			$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
		// Se não houver nenhum error_log
		$resultado = false;
		if ($error == "") {	
			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);		
			
			// Gera um nome único para a imagem
			//$nome_imagem = md5(uniqid(time())) . "." . $ext[1];

			$nome_imagem = $id. "." .$ext[1];
 
			// Caminho de onde ficará a imagem
			$caminho_imagem = $caminho_imagem . $nome_imagem;
			
			// Faz o upload da imagem para seu respectivo caminho
			move_uploaded_file($foto["tmp_name"], $caminho_imagem);

			$sql=$sql." '$nome_imagem' WHERE id = '" . $id . "' ";	
			$resultado = $conexao->query($sql);
		}
		
			return $resultado;
	} 
}

function RetornaFormulaDescricao($conexao, $formulaId){	
	$sql = "SELECT 
			descricao 
		FROM 
			formulas 
		WHERE 
			id = '".$formulaId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['descricao'];	 
}

function RetornaTimesGrupo($conexao, $campeonatoId, $grupo) {
	$sql = "SELECT 
			timeId 
		FROM 
			timescampeonato 
		WHERE 
			grupo = '".$grupo."' AND
			campeonatoId = '".$campeonatoId."'
		ORDER BY 
			id";

	$rs =$conexao->query($sql);
	
	while ($reg = mysqli_fetch_array($rs)) {
		$array[] = $reg['timeId'];
	}
			
	return $array;
}

function RetornaBotonista($conexao, $timeId) {
	$sql = "SELECT 
			botonistaId 
		FROM 
			times 
		WHERE 
			id = '".$timeId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['botonistaId'];
}

function RetornaApelidoBotonista($conexao, $timeId) {
	$sql = "SELECT 
			botonistas.apelido AS apelido
		FROM 
			times INNER JOIN
    			botonistas ON times.botonistaId = botonistas.id
		WHERE 
			times.id = '".$timeId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['apelido'];
}

function GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, $turno, $grupo, $ordem) {
	if ($ordem =="") $ordem = 0;
	
	$sql = "INSERT INTO jogo
                  (campeonatoId, mandanteId, visitanteId, rodada, turno, confronto, grupo, ordem) VALUES 
                  ($campeonatoId, $mandanteId, $visitanteId, $rodada, $turno, ' x ', '$grupo', $ordem)";

	return $conexao->query($sql);
}

function ApagarJogosCampeonato($conexao,$campeonatoId){
	$sql = "DELETE FROM jogo WHERE campeonatoId = '".$campeonatoId."' ";

	return $conexao->query($sql);
}

function GravarTabela($conexao, $campeonatoId, $equipes, $turnoGrupo) {
	$pontoGanho = 0;
	$vitoria = 0;
	$empate = 0;
	$derrota = 0;
	$golFavor = 0;
	$golContra = 0;

	$resultado = false;
	foreach($equipes as $equipe) {
		$timeNome = RetornaNomeTime($conexao, $equipe);

		$sql = "INSERT INTO tabela (campeonatoId, timeId, timeNome, pontoGanho, vitoria, empate, derrota, golFavor, golContra, turnoGrupo) 
			VALUES ('$campeonatoId','$equipe', '$timeNome', '$pontoGanho', '$vitoria', '$empate', '$derrota', '$golFavor', '$golContra', '$turnoGrupo')";

		$resultado = $conexao->query($sql);
	}

	return $resultado;
}

function RetornaNomeTime($conexao, $timeId) {
	$sql = "SELECT 
			nome
		FROM 
			times 
		WHERE 
			id = '".$timeId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['nome'];
}

/*
function RetornaCampeonatoAtivo($conexao) {
	$sql = "SELECT 
			id,
			classeRanking,
			doisGrupos,
			doisTurnos, 
			descricao
		FROM 
			campeonatos 
		WHERE 
			ativo = '1' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return array($reg['id'], $reg['classeRanking'], $reg['doisGrupos'], $reg['doisTurnos'], $reg['descricao']);
}
*/

function RetornaCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT 
			id,
			classeRanking,
			doisGrupos,
			doisTurnos, 
			descricao
		FROM 
			campeonatos 
		WHERE 
			id = '".$campeonatoId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return array($reg['id'], $reg['classeRanking'], $reg['doisGrupos'], $reg['doisTurnos'], $reg['descricao']);
}

function VerificaSeHaJogosCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT id FROM jogo WHERE campeonatoId = '".$campeonatoId."' ";
	
	$rs =$conexao->query($sql);
	
	$registros = $rs->num_rows;
	
	if ($registros == 0) return false;
	else return true;
}

function EhUltimoJogo($conexao, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			jogo 
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			placarMandante IS NULL AND 
			placarVisitante IS NULL";	
	
	$rsQuantidade =$conexao->query($sql);		
	$quantidade = $rsQuantidade->num_rows;

	if ($quantidade == 0) return true;
	else return false;
}

function EhSemiFinal($conexao, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			jogo 
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			turno = '3' ";

	$rsQuantidade =$conexao->query($sql);		
	$quantidade = $rsQuantidade->num_rows;

	if ($quantidade == 0) return true;
	else return false;
}

function EhFinal($conexao, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			jogo 
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			turno = '4' ";

	$rsQuantidade =$conexao->query($sql);		
	$quantidade = $rsQuantidade->num_rows;

	if ($quantidade == 0) return true;
	else return false;
}

function JogosEncerrados($conexao, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			jogo 
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			turno = '4' AND
    			placarMandante IS NULL";

	$rsQuantidade =$conexao->query($sql);		
	$quantidade = $rsQuantidade->num_rows;

	if ($quantidade == 0) return true;
	else return false;
}

function AtualizarCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT
			doisGrupos,
			doisTurnos,
			evitarConfrontos,
			semiFinal,
			final,
			jogos
		  FROM 
			campeonatos 
		  WHERE 
			id = '".$campeonatoId."' ";

	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	$doisGrupos = $reg['doisGrupos'];
	$doisTurnos = $reg['doisTurnos'];
	$semiFinal = $reg['semiFinal'];
	$final = $reg['final'];
	$quantidadeJogos = $reg['jogos'];
	$evitarConfrontos = $reg['evitarConfrontos'];

	if ($semiFinal == "quatroPrimeirosGeral") {

	}
	else if ($semiFinal == "doisMelhoresCadaTurno") {
		
		if (EhSemiFinal($conexao, $campeonatoId)) {
			GerarJogosSemiFinal($conexao, $campeonatoId, $semiFinal);
		}
		else {						
			if (JogosEncerrados($conexao, $campeonatoId)){
				if (TemVencedor($conexao, $campeonatoId, "4")) {
					EncerrarCampeonato($conexao, $campeonatoId);
				}
			}
			else {
				GerarJogoFinal($conexao, $campeonatoId, $quantidadeJogos);
			}
		}
	}
	else {
		if ($final == "campeaoCadaTurno") {
			if (EhFinal($conexao, $campeonatoId)) {
				$jogosEncerrados = JogosEncerrados($conexao, $campeonatoId);

				if ($jogosEncerrados){
					$finalistaA = CampeaoTurno($conexao, $campeonatoId, "1");
					$finalistaB = CampeaoTurno($conexao, $campeonatoId, "2");

					if ($finalistaA[0] == $finalistaB[0]) 
					{		
						$campeao = $finalistaA[0];
						$campeapVice = ObterCampeaoVicePrimeirosDaTabela($conexao, $campeonatoId);
						$viceCampeao = $campeapVice[1];

						$sql = "UPDATE campeonatos SET campeao = '".$campeao."', vice = '".$viceCampeao."', ativo = 0 WHERE id = '".$campeonatoId."' ";

						$resultado = $conexao->query($sql);
					}
				}
				else {
					GerarFinal($conexao, $campeonatoId, $quantidadeJogos);
				}				
			}
			else {
				
			}
		}
		else{

		}
	}



	//se for campeonato com dois grupos
	if ($doisGrupos) {
		// validar ultimo jogo de cada grupo
	}

	// se campeonato sem final e último jogo
		// encerra o campeonato


	// se final 2 melhores da geral e último jogo
		// se turno igual a 1 ou 2 e último jogo
			// gerar os jogos da final

		// se turno igual a 4 e for o último jogo
			// encerrar o campeonato


	// se final campeão de cada turno e último jogo
		// se turno igual a 1 ou 2 e último jogo
			// gerar os jogos da final

		// se turno igual a 4 e for o último jogo
			// encerrar o campeonato


	// se semi-final e final e último jogo
		// se turno igual a 1 ou 2 e último jogo
			// gerar os jogos da semi-final

		// se turno igual a 3 e último jogo
			// gerar os jogos da final

		// se turno igual a 4 e for o último jogo
			// encerrar o campeonato
	
	

	// efetua o filtro de acordo com o turno
	// $turno = 3 --> semi-final
	// $turno = 4 --> final


}

function TemVencedor($conexao, $campeonatoId, $turno) {
	$sql = "SELECT 
			placarMandante, 
			placarVisitante, 
			penaltiMandante, 
			penaltiVisitante 
		  FROM 
			jogo 
		  WHERE 
			turno = '".$turno."' AND
			campeonatoId = '".$campeonatoId."' ";

	$rs = $conexao->query($sql);	

	$reg=mysqli_fetch_array($rs);
	$placarMandante = $reg['placarMandante'];
	$placarVisitante = $reg['placarVisitante'];
	$penaltiMandante = $reg['penaltiMandante'];
	$penaltiVisitante = $reg['penaltiVisitante'];

	$retorno = true;

	if ($placarMandante == $placarVisitante) {
		if ($penaltiMandante == $penaltiVisitante) {
			$retorno = false;
		}
	}

	return $retorno;
}

function GerarJogosSemiFinal($conexao, $campeonatoId, $semiFinal) {
	$times[] = "";
	$rodada = "1"; 
	$turno = "3";
	$grupo = "A";
	$ordem = "1";

	if ($semiFinal == "quatroPrimeirosGeral"){
		$sql = "SELECT timeId FROM tabela WHERE campeonatoId = '".$campeonatoId."' AND turnoGrupo = 'G' 
			ORDER BY pontoGanho DESC, (golFavor - golContra) DESC, vitoria DESC LIMIT 4";

		$rs =$conexao->query($sql);		
		
		while ($reg = mysqli_fetch_array($rs)) {
			$times[] = $reg['timeId'];
		}

	}
	else{
		$sqlA = "SELECT timeId FROM tabela WHERE campeonatoId = '".$campeonatoId."' AND turnoGrupo = '1' 
			ORDER BY pontoGanho DESC, (golFavor - golContra) DESC, vitoria DESC LIMIT 2";

		$sqlB = "SELECT timeId FROM tabela WHERE campeonatoId = '".$campeonatoId."' AND turnoGrupo = '2' 
			ORDER BY pontoGanho DESC, (golFavor - golContra) DESC, vitoria DESC LIMIT 2";

		$rsA =$conexao->query($sqlA);
		$rsB =$conexao->query($sqlB);	

		$indice = 0;
		while ($regA = mysqli_fetch_array($rsA)) {
			$times[$indice] = $regA['timeId'];
			$indice++;
		}

		while ($regB = mysqli_fetch_array($rsB)) {
			$times[$indice] = $regB['timeId'];
			$indice++;
		}
	}

	GravarJogo($conexao,$campeonatoId, $times[0], $times[3], $rodada, $turno, $grupo, $ordem);
	GravarJogo($conexao,$campeonatoId, $times[1], $times[2], $rodada, $turno, $grupo, $ordem);
}

function ObterVencedorPerdedorConfronto($conexao, $campeonatoId, $turno) {
	$times[] = "";

	$sql = "SELECT 
			mandanteId, 
			visitanteId, 
			placarMandante, 
			placarVisitante, 
			penaltiMandante, 
			penaltiVisitante 
		  FROM 
			jogo 
		  WHERE 
			turno = '".$turno."' AND
			campeonatoId = '".$campeonatoId."' ";	

	$rs =$conexao->query($sql);

	$indice = 0;
	while ($reg = mysqli_fetch_array($rs)) {
		$mandanteId = $reg['mandanteId'];
		$visitanteId = $reg['visitanteId'];
		$placarMandante = $reg['placarMandante'];
		$placarVisitante = $reg['placarVisitante'];
		$penaltiMandante = $reg['penaltiMandante'];
		$penaltiVisitante = $reg['penaltiVisitante'];

		if ($placarMandante > $placarVisitante) {
			$times[$indice] = $reg['mandanteId'];
			$times[$indice + 1] = $reg['visitanteId'];
		}

		else if ($placarMandante < $placarVisitante) {
			$times[$indice] = $reg['visitanteId'];
			$times[$indice + 1] = $reg['mandanteId'];
		}

		else {
			if ($penaltiMandante > $penaltiVisitante) {
				$times[$indice] = $reg['mandanteId'];
				$times[$indice + 1] = $reg['visitanteId'];
			}
			else {
				$times[$indice] = $reg['visitanteId'];
				$times[$indice + 1] = $reg['mandanteId'];
			}
		}
	}

	return $times;
}

function GerarJogoFinal($conexao, $campeonatoId, $quantidadeJogos) {

	/*
	

	$sql = "SELECT 
			mandanteId, 
			visitanteId, 
			placarMandante, 
			placarVisitante, 
			penaltiMandante, 
			penaltiVisitante 
		  FROM 
			jogo 
		  WHERE 
			turno = 3 AND
			campeonatoId = '".$campeonatoId."' ";

	$rs =$conexao->query($sql);

	$indice = 0;
	while ($reg = mysqli_fetch_array($rs)) {
		$mandanteId = $reg['mandanteId'];
		$visitanteId = $reg['visitanteId'];
		$placarMandante = $reg['placarMandante'];
		$placarVisitante = $reg['placarVisitante'];
		$penaltiMandante = $reg['penaltiMandante'];
		$penaltiVisitante = $reg['penaltiVisitante'];

		if ($placarMandante > $placarVisitante) {
			$times[$indice] = $reg['mandanteId'];
		}

		else if ($placarMandante < $placarVisitante) {
			$times[$indice] = $reg['visitanteId'];
		}

		else {
			if ($penaltiMandante > $penaltiVisitante) {
				$times[$indice] = $reg['mandanteId'];
			}
			else {
				$times[$indice] = $reg['visitanteId'];
			}
		}

		$indice++;
	}

	*/
	//$times[] = "";
	$times = ObterVencedorPerdedorConfronto($conexao, $campeonatoId, "3");

	$sqlTimes = "SELECT 
				timeId
			 FROM 
				tabela 
			 WHERE 
				turnoGrupo = 'G' AND
				campeonatoId = '".$campeonatoId."' AND
				(timeId = '".$times[0]."' OR timeId = '".$times[1]."')
			 ORDER BY
				pontoGanho DESC,
				vitoria DESC,
				(golFavor - golContra) DESC,
				golFavor DESC
			 LIMIT 1";

	$rsTimes =$conexao->query($sqlTimes);

	$regTimes=mysqli_fetch_array($rsTimes);
	
	$mandante = $regTimes['timeId'];

	$visitante = $times[0];
	if ($mandante ==  $visitante) $visitante = $times[1];

	if ($quantidadeJogos == "umJogo") {
		GravarJogo($conexao,$campeonatoId, $mandante, $visitante, "1", "4", "A", "1");
	}
	else {
		GravarJogo($conexao,$campeonatoId, $visitante, $mandante, "1", "4", "A", "1");
		GravarJogo($conexao,$campeonatoId, $mandante, $visitante, "1", "4", "A", "1");
	}
}

function CampeaoTurno($conexao, $campeonatoId, $turno) {
	$sql = "SELECT 
			timeId,
			pontoGanho, 
			vitoria,
			(golFavor - golContra) AS saldo,
			golFavor
		  FROM 
			tabela 
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND
			turnoGrupo = '".$turno."'
		  ORDER BY
			pontoGanho DESC,
			vitoria DESC,
			saldo DESC,
			golFavor DESC
		  LIMIT 1";

	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	$timeId = $reg['timeId'];
	$pontoGanho = $reg['pontoGanho'];
	$vitoria = $reg['vitoria'];	
	$saldo = $reg['saldo'];	
	$golFavor = $reg['golFavor'];	
	
    return array($timeId, $pontoGanho, $vitoria, $saldo, $golFavor);
}

function GerarFinal($conexao, $campeonatoId, $quantidadeJogos) {
	$finalistaA = CampeaoTurno($conexao, $campeonatoId, "1");
	$finalistaB = CampeaoTurno($conexao, $campeonatoId, "2");

	if ($finalistaA[0] == $finalistaB[0]) 
	{		
		$campeao = $finalistaA[0];
		$campeapVice = ObterCampeaoVicePrimeirosDaTabela($conexao, $campeonatoId);
		$viceCampeao = $campeapVice[1];

		$sql = "UPDATE campeonatos SET campeao = '".$campeao."', vice = '".$viceCampeao."', ativo = 0 WHERE id = '".$campeonatoId."' ";

		$resultado = $conexao->query($sql);
	}
	else {
		$finalistaAPontoGanho = $finalistaA[1];
		$finalistaAVitoria = $finalistaA[2];
		$finalistaASaldo = $finalistaA[3];
		$finalistaAGolFavor = $finalistaA[4];
		
		$finalistaBPontoGanho = $finalistaB[1];
		$finalistaBVitoria = $finalistaB[2];
		$finalistaBSaldo = $finalistaB[3];
		$finalistaBGolFavor = $finalistaB[4];

		if ($finalistaAPontoGanho < $finalistaBPontoGanho) {
			$mandante = $finalistaB[0];
			$visitante  = $finalistaA[0];			
		}		
		else if ($finalistaAPontoGanho == $finalistaBPontoGanho) {
			if ($finalistaASaldo < $finalistaBSaldo) {
				$mandante = $finalistaB[0];
				$visitante  = $finalistaA[0];		
			}
			else if ($finalistaASaldo == $finalistaBSaldo) {
				if ($finalistaAVitoria < $finalistaBVitoria){
					$mandante = $finalistaB[0];
					$visitante  = $finalistaA[0];	
				}
				else if ($finalistaAVitoria == $finalistaBVitoria) {
					if ($finalistaAGolFavor < $finalistaBGolFavor){
						$mandante = $finalistaB[0];
						$visitante  = $finalistaA[0];	
					}
				}
			}			
		}

		if ($quantidadeJogos == "1") {
			GravarJogo($conexao,$campeonatoId, $mandante, $visitante, "1", "4", "A", "1");
		}
		else {
			GravarJogo($conexao,$campeonatoId, $visitante, $mandante, "1", "4", "A", "1");
			GravarJogo($conexao,$campeonatoId, $mandante, $visitante, "1", "4", "A", "1");
		}
	}

	return $resultado;
}

function ObterCampeaoVicePrimeirosDaTabela($conexao, $campeonatoId) {
	/*
	$dadosCampeonato = RetornaDadosCampeonato($conexao, $campeonatoId);
	$doisGrupos = $dadosCampeonato[0];
	$doisTurnos = $dadosCampeonato[1];
	$evitarConfrontos = $dadosCampeonato[2];
	$semiFinal = $dadosCampeonato[3];
	$final = $dadosCampeonato[4];
	$jogos = $dadosCampeonato[5];
	$formulaId = $dadosCampeonato[6];
	$classeRanking = $dadosCampeonato[7];
	*/

	$times[] = "";
	$sql = "SELECT 
			timeId
		  FROM 
			tabela 
		  WHERE 
			turnoGrupo = 'G' AND 
			campeonatoId = '".$campeonatoId."' 
		  ORDER BY 
			pontoGanho DESC,
			(golFavor - golContra) DESC,
			vitoria DESC,
			golFavor DESC ";

	$rs =$conexao->query($sql);
	$indice = 0;
	while ($reg = mysqli_fetch_array($rs)) {
		$times[$indice] = $reg['timeId'];
		$indice++;
	}
	
	return $times;	
}

function EncerrarCampeonato($conexao, $campeonatoId) {				
	$times = ObterVencedorPerdedorConfronto($conexao, $campeonatoId, "4");
	
	$campeao = $times[0];
	$viceCampeao = $times[1];

	$sql = "UPDATE campeonatos SET campeao = '".$campeao."', vice = '".$viceCampeao."', ativo = 0 WHERE id = '".$campeonatoId."' ";

	return $conexao->query($sql);
}			

function VerificaSeTemTimesConfigurados($conexao, $campeonatoId) {
	$sql = "SELECT id FROM timescampeonato WHERE campeonatoId = '".$campeonatoId."' ";
	
	$rs =$conexao->query($sql);
	
	$registros = $rs->num_rows;
	
	if ($registros == 0) return false;
	else return true;
}

function RetornaQuantidadeTimesFormula($conexao, $id) {
	$sql = "
		SELECT 
			CASE 
				campeonatos.doisGrupos
			WHEN 
				'1' 
			THEN 
				(ROUND(quantidadeTimes / 2))
			ELSE 
				quantidadeTimes
			END as quantidadeTimes
		FROM 
			formulas INNER JOIN	
			campeonatos ON
			campeonatos.formulaId = formulas.id
		WHERE
			campeonatos.id = '".$id."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['quantidadeTimes'];
}

function RetornaClasseRanking($conexao, $campeonatoId) {
	$sql = "SELECT classeRanking FROM campeonatos WHERE id = '".$campeonatoId."' ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['classeRanking'];
}

function RetornaDescricaoCampeonato($conexao, $campeonatoId) {
	$sql = "
		SELECT 
			descricao,
			edicao 
		FROM 
			campeonatos 
		WHERE 
			id = ".$campeonatoId." ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);
	
	return $reg['descricao']." - ".$reg['edicao']."ª edição";
}

function RetornaGolsJogador($conexao, $jogadorId, $campeonatoId) {
	$sql = "SELECT 
			SUM(jogojogador.gols) AS gols
		  FROM 
			jogojogador LEFT JOIN 
		  	jogo ON jogo.id = jogojogador.jogoId
		  WHERE
			jogojogador.jogadorId = '".$jogadorId."' AND
			jogo.campeonatoId = '".$campeonatoId."'
		  GROUP BY
			jogojogador.jogadorId ";
	
	$rs =$conexao->query($sql);

	$reg=mysqli_fetch_array($rs);

	$gols = "0";

	if ($reg['gols'] != "") {
		$gols = $reg['gols'];
	}
	
	return $gols;
}

function CarregaGridJogos($conexao, $campeonatoId, $turno, $grupo) {	
	$sql="SELECT 
			id,
			grupo,
			(SELECT foto FROM times WHERE id = mandanteId) AS fotoMandante,
			(SELECT foto FROM times WHERE id = visitanteId) AS fotoVisitante,
			(select nome from times where id = mandanteId) AS mandante,  
			(select nome from times where id = visitanteId) AS visitante,
			placarMandante,
			placarVisitante,
			penaltiMandante,
			penaltiVisitante,
			rodada,
			turno
		FROM 
			jogo 
		WHERE 
			campeonatoId = '".$campeonatoId."' AND
			grupo = '".$grupo."' AND
			turno = '".$turno."' 
		ORDER BY 
			grupo, 
			rodada, 
			turno, 
			id ";			
	return $conexao->query($sql);
}

function CarregaGridTabela($conexao, $campeonatoId, $turnoGrupo) {
	$sql="SELECT
			campeonatoId,
			timeId,
			timeNome,
			pontoGanho,
			vitoria,
			empate,
			derrota,
			golFavor,
			golContra,
			turnoGrupo,
			(golFavor - golContra) AS saldo
		FROM 
			tabela 
		WHERE 
			campeonatoId = $campeonatoId AND
			turnoGrupo = '".$turnoGrupo."'
		ORDER BY
			pontoGanho DESC,
			saldo DESC,
			vitoria DESC,
			golFavor DESC,
			timeNome ASC";	

	return $conexao->query($sql);	
}

function RetornaDadosJogo($conexao, $jogo) {
	$sql = "SELECT 
			campeonatoId,
			mandanteId,
			visitanteId,
			turno,
			grupo,
			placarMandante,
			placarVisitante,
			penaltiMandante,
			penaltiVisitante
		FROM 
			jogo 
		WHERE 			
			id =  '".$jogo."' ";

	$rs =$conexao->query($sql);

	$reg = mysqli_fetch_array($rs);
	
	$campeonatoId = $reg['campeonatoId'];
	$mandanteId = $reg['mandanteId'];
	$visitanteId = $reg['visitanteId'];	
	$turno = $reg['turno'];	
	$grupo = $reg['grupo'];	
	$placarMandante = $reg['placarMandante'];	
	$placarVisitante = $reg['placarVisitante'];	
	$penaltiMandante = $reg['penaltiMandante'];	
	$penaltiVisitante = $reg['penaltiVisitante'];	
	
    return array($campeonatoId, $mandanteId, $visitanteId, $turno, $grupo, $placarMandante, $placarVisitante, $penaltiMandante, $penaltiVisitante);
}

function SalvarTabela($conexao, $dadosTime, $grupo, $turno) {
	$campeonatoId = $dadosTime[0];
	$timeId = $dadosTime[1];

	$dadosTabela = RetornaDadosTabela($conexao, $campeonatoId, $timeId, $turno);

	$pontosGanhos = $dadosTime[2] + $dadosTabela[0];
	$vitoria = $dadosTime[3] + $dadosTabela[1];
	$empate = $dadosTime[4] + $dadosTabela[2];
	$derrota = $dadosTime[5] + $dadosTabela[3];
	$golsPro = $dadosTime[6] + $dadosTabela[4];
	$golsContra = $dadosTime[7] + $dadosTabela[5];	

	$sql = "UPDATE tabela SET
			pontoGanho = '$pontosGanhos',
			vitoria = '$vitoria',
			empate = '$empate',
			derrota = '$derrota',
			golFavor = '$golsPro',
			golContra = '$golsContra'
		WHERE 
			campeonatoId = $campeonatoId AND
			timeId = $timeId AND
			turnoGrupo = '".$turno."' ";

	return $conexao->query($sql);
}

function RetornaDadosTabela($conexao, $campeonatoId, $timeId, $turno) {
	$sql = "SELECT 
			pontoGanho, 
			vitoria, 
			empate, 
			derrota, 
			golFavor, 
			golContra 
		FROM 
			tabela
		WHERE 			
			campeonatoId = $campeonatoId AND
			timeId = $timeId AND
			turnoGrupo = '".$turno."' ";

	$rs =$conexao->query($sql);

	$reg = mysqli_fetch_array($rs);
	
	$pontoGanho = $reg['pontoGanho'];
	$vitoria = $reg['vitoria'];
	$empate = $reg['empate'];	
	$derrota = $reg['derrota'];	
	$golFavor = $reg['golFavor'];	
	$golContra = $reg['golContra'];
	
    return array($pontoGanho, $vitoria, $empate, $derrota, $golFavor, $golContra);
}

function RetornaDadosCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT 
			doisGrupos, 
			doisTurnos, 
			evitarConfrontos,
			finais, 
			jogos, 
			formulaId, 
			classeRanking 
		FROM 
			campeonatos
		WHERE 			
			id = '".$campeonatoId."' ";
	$rs =$conexao->query($sql);

	$reg = mysqli_fetch_array($rs);
	
	$doisGrupos = $reg['doisGrupos'];
	$doisTurnos = $reg['doisTurnos'];
	$evitarConfrontos = $reg['evitarConfrontos'];	
	$finais = $reg['finais'];
	$jogos = $reg['jogos'];	
	$formulaId = $reg['formulaId'];	
	$classeRanking = $reg['classeRanking'];	
	
    return array($doisGrupos, $doisTurnos, $evitarConfrontos, $finais, $jogos, $formulaId, $classeRanking);
}

function SalvarGoleadores($conexao, $jogadores, $jogoId) {
	$resultado = false;
	foreach ($jogadores as $jogador) {
            $jogadorId = $jogador['id'];
            $golJogador = $jogador['gols'];   

            $sql = "INSERT INTO jogojogador (jogoId, jogadorId, gols)
                        VALUES ($jogoId, $jogadorId, $golJogador) "; 
                            
            $resultado = $conexao->query($sql);
        }

	  return $resultado;
}

function AtualizarTabelasClassificacao($conexao, $jogoId, $inclusao) {
	$pontoGanhoMandante = 0;
	$vitoriaMandante = 0;
	$empateMandante = 0;
	$derrotaMandante = 0;   
	$pontoGanhoVisitante = 0;
	$vitoriaVisitante = 0;
	$empateVisitante = 0;
	$derrotaVisitante = 0;    
	$placarMandante = null;
	$placarVisitante = null;

	$dadosJogo = RetornaDadosJogo($conexao, $jogoId); 

	$campeonatoId = $dadosJogo[0];
	$mandanteId = $dadosJogo[1];
	$visitanteId = $dadosJogo[2];
	$turno = $dadosJogo[3];
	$grupo = $dadosJogo[4];
	$placarMandante = $dadosJogo[5];
	$placarVisitante = $dadosJogo[6];
	$penaltiMandante = $dadosJogo[7];	
	$penaltiVisitante = $dadosJogo[8]; 

	$golsMandante = $placarMandante;
	$golsVisitante = $placarVisitante;

    if ($placarMandante == $placarVisitante) {
		$pontoGanhoMandante = 1;
		$empateMandante = 1;
		$pontoGanhoVisitante = 1;
		$empateVisitante = 1;
    }

    else if ($placarMandante > $placarVisitante) {
		$pontoGanhoMandante = 3;
		$vitoriaMandante = 1;   
		$derrotaVisitante = 1;
    }

    else  {
		$derrotaMandante = 1;
		$pontoGanhoVisitante = 3;
		$vitoriaVisitante = 1;
    }

    if (!$inclusao) {
		$pontoGanhoMandante = $pontoGanhoMandante * (-1);
		$vitoriaMandante = $vitoriaMandante * (-1);
		$empateMandante = $empateMandante * (-1);
		$derrotaMandante = $derrotaMandante * (-1);   
		$pontoGanhoVisitante = $pontoGanhoVisitante * (-1);
		$vitoriaVisitante = $vitoriaVisitante * (-1);
		$empateVisitante = $empateVisitante * (-1);
		$derrotaVisitante = $derrotaVisitante * (-1); 
		$golsMandante = $golsMandante * (-1);
		$golsVisitante = $golsVisitante * (-1);
    }

    $dadosMandante = array($campeonatoId, $mandanteId, $pontoGanhoMandante, 
        $vitoriaMandante, $empateMandante, $derrotaMandante, $golsMandante, $golsVisitante);
        
    $dadosVisitante = array($campeonatoId, $visitanteId, $pontoGanhoVisitante, 
        $vitoriaVisitante, $empateVisitante, $derrotaVisitante, $golsVisitante, $golsMandante);

    $resultado1 = true;
    $resultado2 = true;

    if ($turno == "1" OR $turno == "2") {
		$resultado1 = SalvarTabela($conexao, $dadosMandante, $grupo, $turno);
		$resultado2 = SalvarTabela($conexao, $dadosVisitante, $grupo, $turno);
    }
         
    $resultado3 = SalvarTabela($conexao, $dadosMandante, $grupo, "G");
    $resultado4 = SalvarTabela($conexao, $dadosVisitante, $grupo, "G");     

    if ($resultado1 AND $resultado2 AND $resultado3 AND $resultado4)  {
	    	return true;
    } 
    else {
	   	return false;
    }
}


?>