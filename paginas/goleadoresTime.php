<?php
	if(!empty($campeonatoId)) {
            $sql = "SELECT 
                              times.id AS timeId,
                              jogador.numero,
                              jogador.nome AS jogadorNome,
                              SUM(jogojogador.gols) AS gols,
                              times.nome AS timeNome
                        FROM 
                              jogojogador INNER JOIN 
                              jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                              times ON jogador.timeId = times.id
                        WHERE
                              jogador.campeonatoId = '".$campeonatoId."'
                        GROUP BY
                              jogojogador.jogadorId
                        ORDER BY
                              timeNome ASC,
                              gols DESC,
                              jogadorNome ASC ";

            $rs=$conexao->query($sql); 
      }
?>

<h2>Goleadores por time</h2>

<div class = "box tabela">                            
      <div class="box box-solid box-success">
            <div class="cabecalhoTabela">
            <h3 class="textoCabecalho">GOLEADORES</h3>
      </div>

      <div class="box-body">
            <div class="box-body no-padding">
                  <table class="table tabela1" >
                        <thead class="thead-default" >
                              <tr class="cabecalho">
                                    <th class="linha">Nº</th>
                                    <th class="linha">Nome</th>
                                    <th class="linha">Gols</th>
                              </tr> 
                        </thead> 

                         <?PHP
                        $timeAnterior = 0;
                        if (isset($rs)) {
                              while($reg=mysqli_fetch_array($rs)) {
                                    $timeId = $reg["timeId"];
                                    $numeroTime = $reg["numero"];
                                    $jogadorNomeTime = $reg["jogadorNome"];
                                    $timeNomeTime = $reg["timeNome"];
                                    $golsTime = $reg["gols"];   

                                    if ($timeAnterior != $timeId) { ?>                  
                                    <tr>
                                          <td class="linha"></td>
                                          <td class="linha" style="text-align: center;"><strong style="color: red;"><?PHP print $timeNomeTime; ?></strong></td>
                                          <td class="linha"></td>  
                                    </tr>  <?php                              
                              } 
                  
                              $timeAnterior = $timeId;?>                           
                                                                  
                              <tr>                                     
                                    <td class="linha"><?PHP print $numeroTime; ?></td>
                                    <td class="linha"><?PHP print $jogadorNomeTime; ?></td>
                                    <td class="linha"><?PHP print $golsTime; ?></td>                             
                              </tr>	<?PHP 
                              } 
                        } else { ?>                                                                          
                              <tr>                   
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                              </tr>	 <?php
                        } ?>
                              
                  </table>
            </div>
      </div>    
</div>