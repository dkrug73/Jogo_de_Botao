<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";

    $jogoId = $_POST['jogo'];
    $campeonatoId = $_POST['campeonato'];
    $placarMandante = $_POST['penaltiMandante'];
    $placarVisitante = $_POST['penaltiVisitante'];

    $sql = "UPDATE jogo SET 
                penaltiMandante = $placarMandante,
                penaltiVisitante = $placarVisitante
            WHERE 
                id = $jogoId ";

    $atualizarJogo = $conexao->query($sql);     
    
    if (EhUltimoJogo($conexao, $campeonatoId)) {
        AtualizarCampeonato($conexao, $campeonatoId);
    }

    if ($atualizarJogo) echo json_encode(true);    
    else echo json_encode(false);
?>