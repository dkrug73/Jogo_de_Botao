<?php
    	SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];
   
	$id = $_POST['timeId'];
	$nome = $_POST['nome'];
	$nomeOficial = $_POST['nomeOficial']; 
    	$botonistaId = $_POST['botonistaId'];

    	$nome_imagem = "";
	$caminho_imagem = "";

	$foto = "";
	if (isset($_FILES['foto'])) {
		$foto = $_FILES['foto'];
	}
		
	$caminho_imagem = "../imagens/times/";
	
	$mensagem="";
	$retorno=false;
	$tipoAviso="";
	$resultado = false;	
   
    if ($acao == "inc")  {	 // INCLUSÃO         
        if($id != "") { // se existir botonistaId, então é uma alteração			
			$sql = "UPDATE times SET 
						nome = '$nome', 
						nomeOficial = '$nomeOficial', 
						botonistaId = '$botonistaId' 	
					WHERE id = '".$id."' ";
		
			$resultado=$conexao->query($sql);
        }
        else { // se não existir botonistaId, então é uma inclusão			
			$sql = "INSERT INTO times (nome, nomeOficial, botonistaId) 
			VALUES ('$nome', '$nomeOficial', '$botonistaId') ";
			
			$resultado=$conexao->query($sql);
			$id = $conexao->insert_id;                
		}
	} 
    else if ($acao == "exc") { // Ação de exclusão
		$sqlFoto="SELECT foto FROM times WHERE id='" . $id . "' ";		
		$rs=$conexao->query($sqlFoto);
		$reg=mysqli_fetch_array($rs);
		
		$fotoBotonista = $reg['foto'];		
		$caminhoFoto="../imagens/times/".$fotoBotonista;
		
		$sql = "DELETE FROM times WHERE id = '" . $id . "' ";	
		$mensagem="Cadastro excluído com sucesso.";
		$tipoAviso = "sucesso";
		$resultado=$conexao->query($sql);
		
		if ($resultado){
			if($fotoBotonista != ""){
				unlink($caminhoFoto);
			}		
			
			print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=cadastroTime.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
		}
	} 
			
	if ($resultado) {
		$sql="UPDATE times SET foto=";
		
		if (!empty($foto["name"])) {
			InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id); 
		}

		$mensagem="Cadastro incluído/alterado com sucesso.";
		$tipoAviso = "sucesso";
				
		print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroTime.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
	} 
	else {
		$mensagem="Houve um erro ao inserir/alterar o cadastro.";
		$tipoAviso = "erro";
		print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroTime.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
	}
	
	mysqli_close($conexao);
?>