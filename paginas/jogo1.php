<?php
    SESSION_START();
    include "../conexao/dbConexao.php";
    include "../utils/funcoes.php";

    $jogoId = $_POST['jogo'];
    $mandanteId = $_POST['mandante'];
    $visitanteId = $_POST['visitante'];
    $campeonatoId = $_POST['campeonato'];
    $placarMandante = $_POST['placarMandante'];
    $placarVisitante = $_POST['placarVisitante'];
    
    if (isset($_POST['goleadoresMandante'])) {       
        $golsMandante = $_POST['goleadoresMandante'];
        
        if (!SalvarGoleadores($conexao, $golsMandante, $jogoId)) {
            echo json_encode(false);
        }       
    }

    if (isset($_POST['goleadoresVisitante'])) {
        $golsVisitante = $_POST['goleadoresVisitante'];

        if (!SalvarGoleadores($conexao, $golsVisitante, $jogoId)) {
            echo json_encode(false);
        }
    } 

    // caso confronto entre mesmo time (ex: campeão do 1 turno x vice-campeao do 2 turno)
    if ($mandanteId == $visitanteId) {
        $sql = "UPDATE jogo SET 
                placarMandante = 1,
                placarVisitante = 0
            WHERE 
                id = $jogoId ";
    }  
    else { 
        $sql = "UPDATE jogo SET 
                    placarMandante = $placarMandante,
                    placarVisitante = $placarVisitante
                WHERE 
                    id = $jogoId ";
    }

    if (!$conexao->query($sql)){
        echo json_encode(false);
    }  

    if (!AtualizarTabelasClassificacao($conexao, $jogoId, true)) {
        echo json_encode(false);
    }       
	
	echo json_encode(true);
?>