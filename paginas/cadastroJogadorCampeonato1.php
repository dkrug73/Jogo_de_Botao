<?php
	SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];

	$mensagem="";
	$retorno=false;
	$tipoAviso="";	

	$titular = 0;
	$id = null;
	if (isset($_POST['jogadorId'])){
		$id = $_POST['jogadorId'];
	}

	$campeonatoId = $_POST['campeonatoId'];
	$timeId = $_POST['timeId']; 
	$nome = $_POST['nome'];
	$numero = $_POST['numero'];

	$foto = "sem_imagem.png";
	if (isset($_POST['foto'])){
		$foto = $_POST['foto'];
	}	
	
	if (isset($_POST['titular']) == 'on') $titular = 1;	

	if( $_SERVER['REQUEST_METHOD']=='POST' ) 	{	
		if ($acao == "inc")  {
			$tipoAviso = "sucesso";	

			if($id == "") {
				$mensagem="Jogador incluído com sucesso.";						

				$sql = "INSERT INTO jogador (campeonatoId, timeId, nome, numero, titular, foto)
					VALUES ('".$campeonatoId."','".$timeId."','".$nome."','".$numero."','".$titular."','sem_imagem.png')";

				$resultado=$conexao->query($sql);
				$id = $conexao->insert_id; 
			}
			else {
				$mensagem="Jogador alterado com sucesso.";

				$sql = "UPDATE 
						jogador 
					SET 
						campeonatoId = '$campeonatoId', 
						timeId = '$timeId', 
						nome = '$nome', 
						numero = '$numero', 
						titular = '$titular', 
						foto = '$foto'
					WHERE 
						id = ".$id;

				$resultado=$conexao->query($sql);
			}
		}

		else if ($acao == "exc") { 	
			$mensagem="Jogador excluído com sucesso.";
			$tipoAviso = "sucesso";		
			
			$sql = "DELETE FROM jogador WHERE id = '".$id."' ";				
			$resultado=$conexao->query($sql);
			
			if ($resultado){
				$sqlFoto="SELECT foto FROM jogador WHERE id = '". $id."' ";		
				$rs=$conexao->query($sqlFoto);
				$reg=mysqli_fetch_array($rs);
				
				$fotoJogador = $reg['foto'];		
				$caminhoFoto="../imagens/jogador/".$fotoJogador;

				if($fotoJogador != ""){
					unlink($caminhoFoto);
				}		
				
				print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=cadastroJogador.php?msg=$mensagem&tipoAviso=$tipoAviso&campeonato=$campeonatoId&time=$timeId'>";
			}
		} 

		if ($resultado) {
			if (isset($_FILES['foto'])) {
				$foto = $_FILES['foto'];
				$caminho_imagem = "../imagens/jogador/";

				$sql="UPDATE jogador SET foto=";
				
				if (!empty($foto["name"])) {
					InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id); 
				}
			}

			print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroJogador.php?msg=$mensagem&tipoAviso=$tipoAviso&campeonato=$campeonatoId&time=$timeId'>";
		} 
		else {
			$mensagem="Houve um erro ao incluir/alterar o jogador.";
			$tipoAviso = "erro";

			print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroJogador.php?msg=$mensagem&tipoAviso=$tipoAviso&campeonato=$campeonatoId&time=$timeId'>";
		}
	}
	
	mysqli_close($conexao);
?>