<?php
      if(!empty($classeRanking)) {

            $sqlRanking = "SELECT
                              timeNome AS timeNome,
                              timeId,
                              SUM(pontoGanho) AS pontos
                        FROM 
                              tabela INNER JOIN campeonatos ON tabela.campeonatoId = campeonatos.id 
                        WHERE
                              tabela.turnoGrupo <> 'G' AND
                              campeonatos.classeRanking = '".$classeRanking."'
                        GROUP BY	
                              timeId
                        ORDER BY
                              pontos DESC,
                              timeNome ASC";

            $rsRanking=$conexao->query($sqlRanking); 

            $sqlGoleadores = "SELECT 
                        jogojogador.id,
                        jogador.numero,
                        jogador.nome AS jogadorNome,
                        SUM(jogojogador.gols) AS gols,
                        times.nome AS timeNome
                  FROM 
                        jogojogador INNER JOIN 
                        jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                        times ON jogador.timeId = times.id INNER JOIN
                        campeonatos ON campeonatos.id = jogador.campeonatoId
                  WHERE
                        campeonatos.classeRanking = '".$classeRanking."'
                  GROUP BY
                        jogojogador.jogadorId
                  ORDER BY
                        gols DESC,
                        jogadorNome ASC,
                        timeNome ASC ";

            $rsGoleadores=$conexao->query($sqlGoleadores);
      }
?>

<h2>Ranking</h2>

<div class = "box tabela">                            
      <div class="box box-solid box-success">
            <div class="cabecalhoTabela">
            <h3 class="textoCabecalho">RANKING</h3>
      </div>

      <div class="box-body">
            <div class="box-body no-padding">
                  <table class="table tabela1" >
                        <thead class="thead-default" >
                              <tr class="cabecalho">
                                    <th class="linha">P</th>
                                    <th class="linha">Time</th>
                                    <th class="linha">Técnico</th>
                                    <th class="linha">Pontos</th>
                              </tr> 
                        </thead>  <?php

                        if (isset($rsRanking)) {
                              $posicao = 1;
                              while($regRanking = mysqli_fetch_array($rsRanking)) {
                                    $timeNome = $regRanking["timeNome"];
                                    $timeId = $regRanking["timeId"];
                                    $pontos = $regRanking["pontos"];
                                    
                                    $botonista = RetornaApelidoBotonista($conexao, $timeId); ?>                          
                                                                  
                                    <tr>       
                                          <td class="linha"><?PHP print $posicao; ?></td>                  
                                          <td class="linha"><?PHP print $timeNome; ?></td>
                                          <td class="linha"><?PHP print $botonista; ?></td>
                                          <td class="linha"><?PHP print $pontos; ?></td>
                                    </tr>	  <?PHP 

                                    $posicao++;
                              } 
                        } else { ?>                                      
                              <tr>       
                                    <td class="linha">&nbsp;</td>                  
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                              </tr>	<?php 
                        } ?>
                  </table>
            </div>
      </div>    
</div>


<h2>Goleadores do ranking</h2>

<div class = "box tabela">                            
      <div class="box box-solid box-success">
            <div class="cabecalhoTabela">
            <h3 class="textoCabecalho">GERAL</h3>
      </div>

      <div class="box-body">
            <div class="box-body no-padding">
                  <table class="table tabela1" >
                        <thead class="thead-default" >
                              <tr class="cabecalho">
                                    <th class="linha">Nº</th>
                                    <th class="linha">Nome</th>
                                    <th class="linha">Gols</th>
                                    <th class="linha">Time</th>
                              </tr> 
                        </thead>     <?PHP

                        if (isset($rsGoleadores)) {
                              while($reg=mysqli_fetch_array($rsGoleadores)) {
                                    $id = $reg["id"];
                                    $numero = $reg["numero"];
                                    $jogadorNome = $reg["jogadorNome"];
                                    $timeNome = $reg["timeNome"];
                                    $gols = $reg["gols"];?>                          
                                                                  
                                    <tr>                                     
                                          <td class="linha"><?PHP print $numero; ?></td>
                                          <td class="linha"><?PHP print $jogadorNome; ?></td>
                                          <td class="linha"><?PHP print $gols; ?></td>
                                          <td class="linha"><?PHP print $timeNome; ?></td>                                                      
                                    </tr>							
                              <?PHP 
                              } 
                        } else {?>                                      
                              <tr>       
                                    <td class="linha">&nbsp;</td>                  
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                              </tr>	<?php 
                        }  ?>
                  </table>
            </div>
      </div>    
</div>