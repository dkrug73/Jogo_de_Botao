
<style>
    .spinner {
        width: 100px;    
    }

    .spinner input {
        text-align: right;
        height: 80px;
        font-size: 65px;
        text-align: center;
    }

    .input-group-btn-vertical {
        position: relative;
        white-space: nowrap;
        width: 1%;
        vertical-align: middle;
        display: table-cell;
    }

    .input-group-btn-vertical > .btn {
        display: block;
        float: none;
        padding: 8px;
        margin-left: -1px;
        position: relative;
        border-radius: 0;
        width: 40px;
        height: 41px;
    }

    .input-group-btn-vertical > .btn:first-child {
        border-top-right-radius: 4px;
    }

    .input-group-btn-vertical > .btn:last-child {
        margin-top: -2px;
        border-bottom-right-radius: 4px;
    }

    .input-group-btn-vertical i{
        position: absolute;
        top: 0;
        left: 4px;
        right: 4px;
        font-size: 40px;
    }

    .jogo > .jogo-item-penalti:nth-child(1) {
        text-align: center;
        width: 100px;
        height: 85px;
    }

    .jogo > .jogo-item-penalti:nth-child(2) {
        width: 150px;
        text-align: center;
        font-size: 50px;
        align-items: center;
        color: black;
    }

    .jogo > .jogo-item-penalti:nth-child(3) {
        width: 100px;
        text-align: center;
        color: black;
        font-size: 80px;
    }

    .jogo > .jogo-item-penalti:nth-child(4) {
        width: 150px;
        text-align: center;
        font-size: 50px;
        align-items: center;
        color: black;
    }

    .jogo > .jogo-item-penalti:nth-child(5) {
        text-align: center;
        width: 100px;
        height: 80px;
    }

</style>

<div class="modal-jogos" role="document" style="width: 800px;">    
    
    <div class="login-box-body cabecalho" style="background: #deefe2;"> 
        <div class="jogo" style="font-size: 50px;margin: 0 0 15px 0;padding: 20px 0 0 0;color: #4a4242;">DECISÃO POR PÊNALTIS</div>

        <div class="jogo">
           
            <div class="jogo jogo-item-penalti" > <img  id="mandanteFotoPenaltis" style="width: 80px;"> </div>

            <div class="jogo jogo-item-penalti" >
                <div class="input-group spinner">
                    <input type="text" class="form-control" value="0" style="width: 95px;" name="mandante" id="mandante">
                    
                    <div class="input-group-btn-vertical">
                        <button class="btn btn-default" type="button" name="mandanteMais" id="mandanteMais" onclick="cliquePenalti(event)"><i class="fa fa-caret-up" ></i></button>
                        <button class="btn btn-default" type="button" name="mandanteMenos" id="mandanteMenos" onclick="cliquePenalti(event)"><i class="fa fa-caret-down" ></i></button>
                    </div>
                </div>
            </div>

            <div class="jogo jogo-item-penalti" ><label for="" >x</label></div>

            <div class="jogo jogo-item-penalti" >
                <div class="input-group spinner">
                    <input type="text" class="form-control" value="0" style="width: 95px;" name="visitante" id="visitante">
                    
                    <div class="input-group-btn-vertical">
                        <button class="btn btn-default" type="button" name="visitanteMais" id="visitanteMais" onclick="cliquePenalti(event)"><i class="fa fa-caret-up" ></i></button>
                        <button class="btn btn-default" type="button" name="visitanteMenos" id="visitanteMenos" onclick="cliquePenalti(event)"><i class="fa fa-caret-down" ></i></button>
                    </div>
                </div>
            </div>

            <div class="jogo jogo-item-penalti"><img  id="visitanteFotoPenaltis" style="width: 80px;">  </div>
        </div>

        <div class="jogo" style="padding: 5px 0 5px 0;">

            <div class="jogo jogo-descricao" ><label for="nomeMandantePenaltis" id="nomeMandantePenaltis"></label></div>

            <div class="jogo jogo-descricao" ></div>

            <div class="jogo jogo-descricao" ><label for="nomeVisitantePenaltis" id="nomeVisitantePenaltis"></label></div>
        </div>
        
    </div>

    <form action="" name="dadosJogo" id="dadosJogo">
        <div style="background: #deefe2;"> </div>    

        <div class="box-footer" style="padding: 15px; text-align: center;">
            <button type="submit" id="salvarJogoPenalti" class="btn btn-primary btn-lg" name="acao" value="inc" 
                style="margin-right: 15px;width: 100%;font-size: 50px;" >GRAVAR</button>          
        </div>

    </form>
</div>

<script>

    function cliquePenalti(e){ 
        var componenteId = getTarget(e).id; 

        if (componenteId == "mandanteMais") {
            document.getElementById("mandante").value = parseInt(document.getElementById("mandante").value ) + 1;     
        }

        else if (componenteId == "mandanteMenos") {
            if (document.getElementById("mandante").value > 0){
                document.getElementById("mandante").value = parseInt(document.getElementById("mandante").value ) - 1;
            }
        }

        else if (componenteId == "visitanteMais") {
            document.getElementById("visitante").value = parseInt(document.getElementById("visitante").value ) + 1;    
        }
    
        else if (componenteId == "visitanteMenos") {
            if (document.getElementById("visitante").value > 0){
                document.getElementById("visitante").value = parseInt(document.getElementById("visitante").value ) - 1;
            }
        }
    }          

</script>


