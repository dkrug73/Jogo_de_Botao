<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../classes.php";

	$mensagem = null;
	$tipoAviso = "sucesso";
	$resultado = false;	

	$formulaJogo = new FormulaJogo();
	$formulaJogo->id = $_POST['formulaJogoId'];
	$formulaJogo->formulaId = $_POST['formulaId'];
	$formulaJogo->rodada = $_POST['rodada'];
	$formulaJogo->mandante = $_POST['mandante'];
	$formulaJogo->visitante = $_POST['visitante'];	
	
	// INCLUSÃO
	if (empty($formulaJogo->id)) {
		$mensagem="Cadastro incluído com sucesso.";

		$formulaJogo->id = $formulaJogo->GravarFormula($formulaJogo, $conexao);	

		$resultado = $formulaJogo->id > 0;

	// EXCLUSÃO / ALTERAÇÃO
	} else {
		if ($_POST['acao'] === "exc") {
			$mensagem="Cadastro excluído com sucesso.";

			$resultado = $formulaJogo->ExcluirFormula($formulaJogo->id, $conexao);
			if ($resultado) $formulaJogo->id = null;	

		} else {
			$mensagem="Cadastro alterado com sucesso.";

			$resultado = $formulaJogo->AlterarFormula($formulaJogo, $conexao);			
		}		
	}
			
	if (!$resultado) {
		$mensagem="Houve um erro ao inserir/alterar o cadastro.";
		$tipoAviso = "erro";
	}

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroFormulaJogos.php?msg=$mensagem&tipoAviso=$tipoAviso&rodada=$formulaJogo->rodada&id=$formulaJogo->formulaId'>";
	mysqli_close($conexao);
?>