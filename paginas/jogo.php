<div class="modal-jogos" role="document" >    
    
    <div class="login-box-body cabecalho" > 
        <div class="jogo" style="padding-top: 5px;">
            <div class="jogo jogo-item" > <img src="../imagens/times/57.png" id="mandanteFoto" style="width: 80px;"> </div>

            <div class="jogo jogo-item" > <input class="placarMandante" name="placarMandante" value="0" id="placarMandante" readonly> </div>

            <div class="jogo jogo-item" ><label for="" >x</label></div>

            <div class="jogo jogo-item" > <input class="placarVisitante" name="placarVisitante" value="0" id="placarVisitante" readonly> </div>

            <div class="jogo jogo-item"><img src="../imagens/times/59.png" id="visitanteFoto" style="width: 80px;">  </div>
        </div>

        <div class="jogo">

            <div class="jogo jogo-descricao" ><label for="nomeMandante" id="nomeMandante"></label></div>

            <div class="jogo jogo-descricao" ></div>

            <div class="jogo jogo-descricao" ><label for="nomeVisitante" id="nomeVisitante"></label></div>
        </div>

        <div class="jogo goleadores">  
            <div class="jogo jogo-goleador" ><p id="goleadorMandante"></p></div>

            <div class="jogo jogo-goleador" ></div>

            <div class="jogo jogo-goleador" ><p id="goleadorVisitante"></p></div>  
        </div> 

    </div>

    <form action="" name="dadosJogo" id="dadosJogo">
        <div class="login-box-body" style="background: #e7eef5; background-image: URL(imagens/jogofundo3.png); padding-bottom: 5px;"> 

            <?php
            for ($i = 1; $i <= 11; $i++) { 
                $idMaisMandante = $i + 100;
                $idMenosMandante = $i + 200;
                $idMaisVisitante = $i + 300;            
                $idMenosVisitante = $i + 400;                
                $idJogadorMandante = $i + 500;
                $idJogadorVisitante = $i + 600;
                ?>             

                <div class="jogo">    
                    <div class="jogo jogo-escalacao">  
                        <div class="jogo jogo-mandante">
                            <div class="input-group">                         
                                <label class="jogadorMandanteId" id="jogadorMandanteId<?php print $i; ?>" ></label>                       
                                <label class="jogadorMandante" id="jogadorMandante<?php print $i; ?>" ></label>
                            </div>
                        </div>

                        <div class="jogo jogo-mandante">
                            <div class="input-group-btn">                    
                                <button type="button" class="botaoMais" name="<?php print $idMaisMandante; ?>" 
                                    id="<?php print $idMaisMandante; ?>" onclick="clique(event)" value="button"><i class="fa fa-plus" ></i></button>
                                <button type="button" class="botaoMenos" name="<?php print $idMenosMandante; ?>" 
                                    id="<?php print $idMenosMandante; ?>" onclick="clique(event)" value="button"><i class="fa fa-minus" ></i></button>   
                            </div> 
                        </div>

                        <div class="jogo jogo-mandante">
                            <input class="placarJogo" value="0" id="jogadorMandante<?php print $idJogadorMandante; ?>" 
                                name="jogadorMandante<?php print $idJogadorMandante; ?>"  maxlength="2" readonly> 
                        </div>
                    </div>
                    
                    <div class="jogo jogo-escalacao"> 
                        <div class="jogo jogo-visitante">
                            <input class="placarJogo" value="0" id="jogadorVisitante<?php print $idJogadorVisitante; ?>" 
                                name="jogadorVisitante<?php print $idJogadorVisitante; ?>"  maxlength="2" readonly> 
                        </div> 

                        <div class="jogo jogo-visitante">
                            <div class="input-group-btn">                    
                                <button type="button" class="botaoMais" name="<?php print $idMaisVisitante; ?>" 
                                    id="<?php print $idMaisVisitante; ?>" onclick="clique(event)" value="button"><i class="fa fa-plus"></i></button>
                                <button type="button" class="botaoMenos" name="<?php print $idMenosVisitante; ?>" 
                                    id="<?php print $idMenosVisitante; ?>" onclick="clique(event)" value="button"><i class="fa fa-minus"></i></button>                    
                            </div>
                        </div>
                        
                        <div class="jogo jogo-visitante">
                            <div class="input-group">                        
                                <label class="jogadorVisitante" id="jogadorVisitante<?php print $i; ?>"></label>                       
                                <label class="jogadorVisitanteId" id="jogadorVisitanteId<?php print $i; ?>"></label>
                            </div>
                        </div>
                    </div>
                </div>
    
            <?php
            } ?>

        </div>    

        <div class="rodapeJogo">
            <button type="submit" id="salvarJogo" class="button botaoJogo" name="acao" value="inc" >Salvar</button>           
        </div>

    </form>
</div>

<script>
//onclick="validarFormulario()"

function validarFormulario() {
  // validações
  if (document.getElementById('placarMandante').value == document.getElementById('placarVisitante').value) {
    alert("É um empate");
    document.formulario.unsubmit();
    return false;
  }
  //alert("Enviou");
  document.formulario.unsubmit();
}
</script>


