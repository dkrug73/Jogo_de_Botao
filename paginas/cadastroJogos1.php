<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];

    $id = $_POST['jogoId'];
	$campeonatoId = $_POST['campeonatoId'];
    $turno = $_POST['turno'];
	$grupo = $_POST['grupo'];
    $ordem = $_POST['ordem']; 

	if ($ordem =="") $ordem = 0;
		
	$rodada = $_POST['rodada'];
	$mandanteId = $_POST['mandanteId'];
	$visitanteId = $_POST['visitanteId'];
		
	$mensagem = null;
	$tipoAviso = null;
	$resultado = false;	
   
    if ($acao == "inc")  {	 // INCLUSÃO         
        if($id != "") { // se existir jogoId, então é uma alteração	
			$mensagem="Jogo alterado com sucesso.";
			$tipoAviso = "sucesso";

			$sql = "UPDATE jogo SET 
						turno = '$turno', 
						grupo = '$grupo', 
						ordem = '$ordem',
						rodada = '$rodada',
						visitanteId = '$visitanteId',
						mandanteId = '$mandanteId'	
					WHERE id = '".$id."' ";
		
			$resultado=$conexao->query($sql);
        }
        else { // se não existir jogoId, então é uma inclusão	
			$mensagem="Jogo incluído com sucesso.";
			$tipoAviso = "sucesso";

			$resultado = GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, $turno, $grupo, $ordem);
		}
		
	} 
    else if ($acao == "exc") { // Ação de exclusão		
		$sql = "DELETE FROM jogo WHERE id = '" . $id . "' ";	
		$mensagem="Jogo excluído com sucesso.";
		$tipoAviso = "sucesso";
		$resultado=$conexao->query($sql);		
	} 
			
	if (!$resultado) {
		$mensagem="Houve um erro ao inserir/alterar o jogo.";
		$tipoAviso = "erro";
	}
		
	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroJogos.php?msg=$mensagem&tipoAviso=$tipoAviso&campeonatoId=$campeonatoId'>";	
	
	mysqli_close($conexao);
?>