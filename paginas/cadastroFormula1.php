<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../classes.php";

	$mensagem = null;
	$tipoAviso = "sucesso";
	$resultado = false;	

	$formula = new Formula();
	$formula->id = $_POST['formulaId'];
	$formula->descricao = $_POST['descricao'];
	$formula->quantidadeTimes = $_POST['quantidadeTimes'];

	// INCLUSÃO
	if (empty($formula->id)) {
		$mensagem="Cadastro incluído com sucesso.";

		$formula->id = $formula->GravarFormula($formula, $conexao);	

		$resultado = $formula->id > 0;

	// EXCLUSÃO / ALTERAÇÃO
	} else {
		if ($_POST['acao'] === "exc") {
			$mensagem="Cadastro excluído com sucesso.";

			$resultado = $formula->ExcluirFormula($formula->id, $conexao);
			if ($resultado) $formula->id = null;	

		} else {
			$mensagem="Cadastro alterado com sucesso.";

			$resultado = $formula->AlterarFormula($formula, $conexao);			
		}		
	}
			
	if (!$resultado) {
		$mensagem="Houve um erro ao inserir/alterar o cadastro.";
		$tipoAviso = "erro";
	}

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroFormula.php?msg=$mensagem&tipoAviso=$tipoAviso&id=$formula->id'>";	
	
	mysqli_close($conexao);
?>