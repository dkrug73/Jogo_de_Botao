<?php
      if(!empty($campeonatoId)) {
            
            $classeRaning = RetornaClasseRanking($conexao, $campeonatoId);

            $sql = "SELECT 
                        jogojogador.id,
                        jogador.numero,
                        jogador.nome AS jogadorNome,
                        SUM(jogojogador.gols) AS gols,
                        times.nome AS timeNome
                  FROM 
                        jogojogador INNER JOIN 
                        jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                        times ON jogador.timeId = times.id
                  WHERE
                        jogador.campeonatoId = '".$campeonatoId."'
                  GROUP BY
                        jogojogador.jogadorId
                  ORDER BY
                        gols DESC,
                        jogadorNome ASC,
                        timeNome ASC ";

            $rs=$conexao->query($sql);
      }
?>

<h2>Goleadores do campeonato</h2>

<div class = "box tabela">                            
      <div class="box box-solid box-success">
            <div class="cabecalhoTabela">
            <h3 class="textoCabecalho">GOLEADORES</h3>
      </div>

      <div class="box-body">
            <div class="box-body no-padding">
                  <table class="table tabela1" >
                        <thead class="thead-default" >
                              <tr class="cabecalho">
                                    <th class="linha">Nº</th>
                                    <th class="linha">Nome</th>
                                    <th class="linha">Gols</th>
                                    <th class="linha">Time</th>
                              </tr> 
                        </thead> 

                        <?PHP
                        if (isset($rs)) {
                              while($reg=mysqli_fetch_array($rs)) {
                                    $id = $reg["id"];
                                    $numero = $reg["numero"];
                                    $jogadorNome = $reg["jogadorNome"];
                                    $timeNome = $reg["timeNome"];
                                    $gols = $reg["gols"];?>                          
                                                                  
                                    <tr>                                     
                                          <td class="linha"><?PHP print $numero; ?></td>
                                          <td class="linha"><?PHP print $jogadorNome; ?></td>
                                          <td class="linha"><?PHP print $gols; ?></td>
                                          <td class="linha"><?PHP print $timeNome; ?></td>                                                      
                                    </tr>							
                              <?PHP 
                              } 
                        } else { ?>                                         
                              <tr>       
                                    <td class="linha">&nbsp;</td>                  
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                                    <td class="linha">&nbsp;</td>
                              </tr>	<?php
                        } ?>                        
                  </table>
            </div>
      </div>    
</div>