<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];
   
    $id = $_POST['botonistaId'];
    $nome = $_POST['nome'];
    $apelido = $_POST['apelido']; 	
	$email = $_POST['email'];

    $nome_imagem = "";
	$caminho_imagem = "";

	$foto = "";
	if (isset($_FILES['foto'])) {
		$foto = $_FILES['foto'];
	}
		
	$caminho_imagem = "../imagens/botonistas/";
	
	$mensagem="";
	$retorno=false;
	$tipoAviso="";
	$resultado = false;	
   
    if ($acao == "inc")  {	 // INCLUSÃO         
        if($id != "") { // se existir botonistaId, então é uma alteração			
			$sql = "UPDATE botonistas SET 
						nome = '$nome', 
						apelido = '$apelido', 
						email = '$email' 	
					WHERE id = '".$id."' ";
		
			$resultado=$conexao->query($sql);
        }
        else { // se não existir botonistaId, então é uma inclusão			
			$sql = "INSERT INTO botonistas (nome, email, apelido) 
			VALUES ('$nome', '$email', '$apelido') ";
			
			$resultado=$conexao->query($sql);
			$id = $conexao->insert_id;                
		}
	} 
    else if ($acao == "exc") { // Ação de exclusão
		$sqlFoto="SELECT foto FROM botonistas WHERE id='" . $id . "' ";		
		$rs=$conexao->query($sqlFoto);
		$reg=mysqli_fetch_array($rs);
		
		$fotoBotonista = $reg['foto'];		
		$caminhoFoto="../imagens/botonistas/".$fotoBotonista;
		
		$sql = "DELETE FROM botonistas WHERE id = '" . $id . "' ";	
		$mensagem="Cadastro excluído com sucesso.";
		$tipoAviso = "sucesso";
		$resultado=$conexao->query($sql);
		
		if ($resultado){
			if($fotoBotonista != ""){
				unlink($caminhoFoto);
			}		
			
			print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=cadastroBotonista.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
		}
	} 
			
	if ($resultado) {
		$sql="UPDATE botonistas SET foto=";
		
		if (!empty($foto["name"])) {
			InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id); 
		}

		$mensagem="Cadastro incluído/alterado com sucesso.";
		$tipoAviso = "sucesso";
				
		print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroBotonista.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
	} 
	else {
		$mensagem="Houve um erro ao inserir/alterar o cadastro.";
		$tipoAviso = "erro";
		print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroBotonista.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
	}
	
	mysqli_close($conexao);
?>