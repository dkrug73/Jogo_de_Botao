<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	
	include "../classes.php";

	$mensagem = null;
	$tipoAviso = "sucesso";
	$resultado = false;	
	
	$data = str_replace("/","-",$_POST['data']); 
	$data = substr($data, 6, 4) .'-'. substr($data, 3, 2) . '-'. substr($data, 0, 2); 

	$ativo = 0;
	if (isset($_POST['checkboxFourInput'])) $ativo = 1;	

	$campeonato = new Campeonato();
	$campeonato->id = $_POST['id'];
	$campeonato->descricao = $_POST['descricao'];
	$campeonato->edicao = $_POST['edicao'];
	$campeonato->local = $_POST['local'];
	$campeonato->classeRanking = $_POST['classeRanking'];
	$campeonato->data = $data;
	$campeonato->ativo = $ativo;	

	// INCLUSÃO
	if (empty($campeonato->id)) {
		$mensagem="Cadastro incluído com sucesso.";

		$campeonato->id = $campeonato->GravarCampeonato($campeonato, $conexao);	

		$resultado = $campeonato->id > 0;

	// EXCLUSÃO / ALTERAÇÃO
	} else {
		if ($_POST['acao'] === "exc") {
			$mensagem="Cadastro excluído com sucesso.";

			$resultado = $campeonato->ExcluirCampeonato($campeonato->id, $conexao);
			if ($resultado) $campeonato->id = null;	

		} else {
			$mensagem="Cadastro alterado com sucesso.";

			$resultado = $campeonato->AlterarCampeonato($campeonato, $conexao);			
		}		
	}
			
	if (!$resultado) {
		$mensagem="Houve um erro ao inserir/alterar o cadastro.";
		$tipoAviso = "erro";
	}

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroCampeonato.php?msg=$mensagem&tipoAviso=$tipoAviso&id=$campeonato->id'>";	
	
	mysqli_close($conexao);
?>