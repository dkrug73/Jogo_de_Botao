<?php
      SESSION_START();
      include "../conexao/dbConexao.php";
      include "../utils/funcoes.php";	

      $mensagem="Jogos incluídos com sucesso.";
      $tipoAviso = "sucesso";

      $resultado = true;
      $resultado2 = true;
      $resultado3 = true;
      $resultado4 = true;

      $campeonatoId = $_GET['campeonatoId'];
      $formulaId = $_GET['formulaId'];

      $sqlConfiguracoes = "SELECT 
                              doisTurnos, 
                              doisGrupos, 
                              evitarConfrontos, 
                              semiFinal, 
                              final, 
                              jogos 
                        FROM 
                              campeonatos 
                        WHERE 
                              id = '".$campeonatoId."' ";

      $rsConfiguracoes=$conexao->query($sqlConfiguracoes);

      $regConfiguracoes=mysqli_fetch_array($rsConfiguracoes);
	
	$grupos = $regConfiguracoes['doisGrupos'];
      $turnos = $regConfiguracoes['doisTurnos'];
      $evitarConfrontos = $regConfiguracoes['evitarConfrontos'];

      $grupoA = RetornaTimesGrupo($conexao, $campeonatoId, "A");
      $grupoB = null;

      if ($grupos) {
            $grupoB = RetornaTimesGrupo($conexao, $campeonatoId, "B");
      }
      
      $sqlFormulaJogos = "SELECT 
                              mandante, 
                              visitante, 
                              rodada 
                        FROM 
                              formulaJogos 
                        WHERE 
                              formulaId = '".$formulaId."'
                        ORDER BY
                              rodada,
                              id";

      $rsFormulaJogos=$conexao->query($sqlFormulaJogos);

      $ordem = 1;
      while($regFormulaJogos=mysqli_fetch_array($rsFormulaJogos)) {
            $mandante=$regFormulaJogos['mandante'];
            $visitante=$regFormulaJogos['visitante'];
            $rodada=$regFormulaJogos['rodada'];

            $mandanteId = $grupoA[$mandante - 1];
            $visitanteId = $grupoA[$visitante - 1];

            if ($evitarConfrontos ) {
                  $botonistaMandante = RetornaBotonista($conexao, $mandanteId);
                  $botonistaVisitante = RetornaBotonista($conexao, $visitanteId);                  

                  if ( $botonistaMandante == $botonistaVisitante) continue;
            }

            $resultado = GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, 1, 'A', $ordem);            

            if ($turnos) {
                  $resultado2 = GravarJogo($conexao,$campeonatoId, $visitanteId, $mandanteId, $rodada, 2, 'A', $ordem);  
            }

            if ($grupos) {
                  $mandanteId = $grupoB[$mandante - 1];
                  $visitanteId = $grupoB[$visitante - 1];

                  $resultado3 = GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, 1, 'B', $ordem); 

                  if ($turnos) {
                        $resultado4 = GravarJogo($conexao,$campeonatoId, $visitanteId, $mandanteId, $rodada, 2, 'B', $ordem);  
                  }
            }

            if (!$resultado or !$resultado2 or !$resultado3 or !$resultado4) {
                  $mensagem="Houve uma falha ao incluir um ou mais jogos.";
                  $tipoAviso = "erro";
            }
            $ordem++;
      }

      // após gravar os jogos irá gravar os times na tabela de classificação

      if ($grupos == "1" OR $turnos == "1") {
            GravarTabela($conexao, $campeonatoId, $grupoA, "1");
      }

      else if ($grupos == "0") {
            GravarTabela($conexao, $campeonatoId, $grupoA, "A");
            GravarTabela($conexao, $campeonatoId, $grupoB, "B");
            GravarTabela($conexao, $campeonatoId, $grupoA, "G");
            GravarTabela($conexao, $campeonatoId, $grupoB, "G");
      }

      else {
            GravarTabela($conexao, $campeonatoId, $grupoA, "1");
            GravarTabela($conexao, $campeonatoId, $grupoA, "2");
            GravarTabela($conexao, $campeonatoId, $grupoA, "G");
      }

      print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastroCampeonato.php?msg=$mensagem&tipoAviso=$tipoAviso&id=$campeonatoId'>";
?>