<?php 
	SESSION_START();
	include "conexao/dbConexao.php";
	include "utils/funcoes.php";

	//LembrarSenha($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$mensagem = "";
	$tipoAviso = "";

	if(isset($_GET['msg'])){
		$mensagem = $_GET['msg'];
	}
	if (isset($_GET['tipoAviso'])) {
		$tipoAviso = $_GET['tipoAviso'];
	} 

	// inicializa valores
	$id = null;
	$campeonatoId = null;
	$descricao = null;
	$edicao = null;
	$local = null;
	$classeRanking = null;	
	$data = null;
	$formulaId = null;
	$doisGrupos = null;
	$doisTurnos = null;
	$evitarConfrontos = null;
	$semiFinal = null;
	$final = null;
	$jogos = null;
	$ativo = 0;

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		if ($id != "") {
			$sql = "SELECT 
					id, 
					descricao,
					edicao,
					local, 
					classeRanking,
					date_format(data, '%d/%m/%Y') AS data,
					formulaId,
					doisGrupos,
					doisTurnos,
					evitarConfrontos,
					semiFinal,
					final,
					jogos, 
					ativo
				FROM 
					campeonatos 
				WHERE id = '" . $id . "' ";
			
			$rs=$conexao->query($sql);
			$reg=mysqli_fetch_array($rs);
			
			$campeonatoId = $id;
			$descricao = $reg['descricao'];
			$edicao = $reg['edicao'];
			$classeRanking = $reg['classeRanking'];
			$local = $reg['local'];
			$data = $reg['data'];
			$formulaId = $reg['formulaId'];
			$doisGrupos = $reg['doisGrupos'];
			$doisTurnos = $reg['doisTurnos'];
			$evitarConfrontos = $reg['evitarConfrontos'];
			$semiFinal = $reg['semiFinal'];
			$final = $reg['final'];
			$jogos = $reg['jogos'];
			$ativo = $reg['ativo'];
		} 		
	}		

	$sql="SELECT
			campeonatos.id, 
			campeonatos.descricao,
			edicao,
			local, 
			classeRanking,
			date_format(data, '%d/%m/%Y') AS data,
			formulas.descricao AS formula,
			CASE doisGrupos 
				WHEN 0 THEN 'não' 
			WHEN 1 THEN 'sim' END AS doisGrupos,
			CASE doisTurnos 
				WHEN 0 THEN 'não' 
				WHEN 1 THEN 'sim' END AS doisTurnos,
			CASE evitarConfrontos 
				WHEN 0 THEN 'não' 
				WHEN 1 THEN 'sim' END AS evitarConfrontos,
			CASE semiFinal 
				WHEN 'semSemiFinal' THEN 'sem semi-final' 
				WHEN 'quatroPrimeirosGeral' THEN 'quatro primeiros' 
				WHEN 'doisMelhoresCadaTurno' THEN 'dois melhores' END AS semiFinal,
			CASE final 
				WHEN 'doisMelhores' THEN 'sem final' 
				WHEN 'semFinal' THEN '2 melhores' 
				WHEN 'campeaoCadaTurno' THEN 'campeão cada turno' END AS final,
			CASE jogos 
				WHEN 'zeroJogo' THEN 'não' 
				WHEN 'umJogo' THEN '1 jogo' 
				WHEN 'doisJogos' THEN '2 jogos' END AS jogos,
			CASE ativo 
				WHEN 0 THEN 'inativo' 
				WHEN 1 THEN 'ativo' END as ativo
		FROM 
			botao.campeonatos LEFT JOIN 
			botao.formulas ON formulas.id = formulaId
		ORDER BY
			campeonatos.descricao, edicao";
	
	$rs=$conexao->query($sql);

	$haJogos = false;
	$temTimesConfigurados = false;

	$haJogos = VerificaSeHaJogosCampeonato($conexao, $campeonatoId);
	$temTimesConfigurados = VerificaSeTemTimesConfigurados($conexao, $campeonatoId);
  ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cadastro de Campeonato</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  
  <link rel="stylesheet" href="dist/css/geral.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="dist/css/redmond/jquery-ui-1.10.1.custom.css" />
  <script src="dist/js/jquery-1.9.1.js" type="text/javascript"></script>
  <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css"> 
 
  <script type="text/javascript">
	function Nova() { location.href="cadastroCampeonato.php" } ;

	function GerarJogos() { 
		var campeonato = "<?php print $campeonatoId; ?>";
		var formula = "<?php print $formulaId; ?>";

		location.href="paginas/gerarJogos.php?campeonatoId="+campeonato+"&formulaId="+formula;
	};

	$(document).ready(function(e) {
		$("#datepicker").datepicker({
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			dateFormat: 'dd/mm/yy',
			nextText: 'Próximo',
			prevText: 'Anterior'
		});
	});
	
  </script>   

</head>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <!-- CABEÇALHO -->
  <?php include("componentes/cabecalho.php"); ?>	

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Cadastro de Campeonatos<small> Incluir/alterar</small></h1>				
		</section>

		<section class="content">				
			<div class="box box-primary">	
				<?php
				
				if($tipoAviso == "erro"){ ?>
					<div class="alert alert-danger alert-dismissable" style="margin: 10px;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-ban"></i>Erro!</h4>
						<?php print $mensagem; ?>
					</div>
					<?php
				}
				else if ($tipoAviso == "alerta"){ ?>
					<div class="alert alert-warning alert-dismissable" style="margin: 10px;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-warning"></i> Aviso!</h4>
						<?php print $mensagem; ?>
					</div>					
				<?php 
				} 
				else if($tipoAviso == "sucesso"){ ?>
					<div class="alert alert-success alert-dismissable" style="margin: 10px;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-check"></i>Sucesso!</h4>
						<?php print $mensagem; ?>
					</div>
				<?php 
				} 
				?>

				<form role="form" name="cadastro" id="cadastro" class="form-horizontal" method="post" action="paginas/cadastroCampeonato1.php" 
					enctype="multipart/form-data" onsubmit="return valida_form();">

					<div class="box-body">

						<div class="form-group">
							<label for="inputCampeonato" class="col-sm-2 control-label">ID</label>
							<div class="col-sm-10">
								<input type="text" class="form-controlCampos" id="campeonatoId" name="campeonatoId" placeholder="ID" 
									value="<?php print $campeonatoId; ?>" readonly>
							</div>
						</div>	
					
						<div class="form-group">
							<label for="inputDescricao" class="col-sm-2 control-label">Descrição</label>
							<div class="col-sm-10">
								<input type="text" class="form-controlCampos" id="descricao" name="descricao" placeholder="Descricao" 
									value="<?php print $descricao; ?>" required>
							</div>
						</div>	

						<div class="form-group">
							<label for="inputEdicao" class="col-sm-2 control-label">Edição</label>
							<div class="col-sm-10">
								<input type="text" class="form-controlCampos" id="edicao" name="edicao" placeholder="Edição" 
									value="<?php print $edicao; ?>">
							</div>
						</div>											
						
						<div class="form-group">
							<label for="inputLocal" class="col-sm-2 control-label">Local</label>
							<div class="col-sm-10">
								<input type="text" class="form-controlCampos" id="local" name="local" placeholder="Local" value="<?php print $local; ?>" required>
							</div>
						</div>

						<div class="form-group">
							<label for="inputClasseRanking" class="col-sm-2 control-label">Classe Ranking</label>
							<div class="col-xs-2">
								<input type="text" class="form-controlCampos" id="classeRanking" name="classeRanking" placeholder="Classe Ranking" 
									value="<?php print $classeRanking; ?>" required>
							</div>
						</div>

						<div class="form-group">
							<label for="inputData" class="col-sm-2 control-label">Data</label>

							<div class="input-group date col-xs-2" style="padding-left: 15px;width: 15.77777777%;/*! width: 16.66666667%;">
								<div class="input-group-addon" style="border-radius: 4px;"><i class="fa fa-calendar"></i></div>
								<input type="text" class="form-control pull-right" style="border-radius: 4px;" id="datepicker" name="data" placeholder="Data do campeonato" 
									value="<?php print $data; ?>" required>								
							</div>							
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="checkbox">
									<label>
										<?php 
										if ($ativo == 0) {  // não receber
											print "<input name='ativo' id='ativo' type='checkbox' > 
												Ativo";
											}
										else {
											print "<input name='ativo' id='ativo' type='checkbox' checked=checked> 
												Ativo";
										}
										?>
									</label>
								</div>
							</div>
						</div>		

					</div>

					<div class="box-footer">
						<button type="submit" id="submit" class="btn btn-primary" name="acao" value="inc" 
							style="width: 80px; margin-right: 15px; margin-left: 275px;">Salvar</button>

						<button type="submit" class="btn btn-danger" name="acao" value="exc" 
							style="width: 80px; margin-right: 15px;">Excluir</button>

						<button type="button" class="btn btn-default" style="width: 80px; margin-right: 15px;" onClick="Nova()" >Cancelar</button>


						<?php
						if($campeonatoId != "") {
							if ($haJogos) { ?>
								<a href="cadastroJogos.php?campeonatoId=<?php print $campeonatoId ?>"  data-target="#"><span class="glyphicon glyphicon-log-in" aria-hidden="true" 
									style="margin-left: 20px"></span> Editar Jogos</a>

                   					<a href="#" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-edit" aria-hidden="true"
							 	style="margin-left: 10px;margin-right: 5px;" ></span> Ver tabela</a>
							<?php 
							} 
							else if (!$haJogos AND $temTimesConfigurados) { ?>
							<button type="button" class="btn btn-default" style="width: 115px; margin-right: 15px;" 
								onClick="GerarJogos()"><span class="glyphicon glyphicon-edit" aria-hidden="true" style="margin-left: -10px;" ></span>
								Gerar jogos</button>
							<?php 
							} 							
							else {?>
								<a href="#" data-toggle="modal" data-target="#myModal6"><span class="glyphicon glyphicon-log-in"
									aria-hidden="true" style="margin-left: 20px"></span> Configurações</a> 
							<?php	
							} 
						}?>
						
					</div>	
				</form>

				<div class="box-body">	
					<h3 class="box-title">Campeonatos</h3>
					<div class="box-tools">
						<div class="input-group" style="width: 150px;">	</div>
					</div>
					
					<div class="box-body table-responsive no-padding">
						<table class="table table-striped">
							<tr>
								<th>ID</th>
								<th>Descrição</th>
								<th>Edicao</th>
								<th>Local</th>
								<th>Classe Ranking</th>
								<th>Data</th>
								<th>Fórmula</th>
								<th>Grupos</th>
								<th>Turnos</th>
								<th>Evitar confrontos</th>
								<th>Semi-final</th>
								<th>Final</th>
								<th>Quantidade jogos na final</th>
								<th>Ativo</th>
							</tr>
							<?PHP
							while($reg=mysqli_fetch_array($rs)) 
							{
								$id = $reg["id"];
								$descricaoT = $reg["descricao"];
								$edicao = $reg["edicao"];
								$local = $reg["local"];
								$classeRanking = $reg["classeRanking"];
								$data = $reg["data"];
								$formula = $reg["formula"];
								$doisGrupos = $reg["doisGrupos"];
								$doisTurnos = $reg["doisTurnos"];
								$evitarConfrontos = $reg["evitarConfrontos"];
								$semiFinal = $reg["semiFinal"];
								$final = $reg["final"];
								$jogos = $reg["jogos"];
								$ativo = $reg["ativo"];?>														
													
								<tr onclick="location.href = 'cadastroCampeonato.php?acao=alt&id=<?PHP print $id;?>&titulo=Alteração de registro'; " 
									style='cursor: pointer;'> 
									
									<td><?PHP print $id; ?></td>
									<td><?PHP print $descricao; ?></td>
									<td><?PHP print $edicao; ?></td>
									<td><?PHP print $local; ?></td>
									<td><?PHP print $classeRanking; ?></td>
									<td><?PHP print $data; ?></td>
									<td><?PHP print $formula; ?></td>
									<td><?PHP print $doisGrupos; ?></td>
									<td><?PHP print $doisTurnos; ?></td>									
									<td><?PHP print $evitarConfrontos; ?></td>
									<td><?PHP print $semiFinal; ?></td>
									<td><?PHP print $final; ?></td>
									<td><?PHP print $jogos; ?></td>
									<td><?PHP print $ativo; ?></td>
								</tr>							
								<?PHP 
							} ?>
						</table>
					</div>
				</div>

			</div>
		</section>
	</div>

	<!-- RODAPÉ -->
	<?php include("componentes/rodape.php"); ?>	

	<!-- ENTRAR -->
	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">			
		<?php include("login/entrar.php"); ?>	
	</div>

	<!-- NOVO CADASTRO -->
	<div class="modal fade" id="myModal5" tabindex="-1" role="dialog">
		<?php include("login/novo_cadastro.php"); ?>
	</div>  

	<!-- CADASTRAR JOGOS -->
	<div class="modal fade" id="myModal6" tabindex="-1" role="dialog">
		<?php include("login/configuracoes.php"); ?>
	</div>  
</div>


<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->

<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
   
  }); 

</script>



</body>
</html>
