<?php
      SESSION_START();
	include "conexao/dbConexao.php";
      include "utils/funcoes.php";  

      $campeonatoId = null;

      if (isset($_GET["campeonatoId"])){
            $campeonatoId = $_GET["campeonatoId"];
      }      
            
      $mensagem = "";
      $tipoAviso = "";

      $_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
      
            
            $campeonatoDescricao = "";
      
            if(isset($_GET['campeonatoId'])){
                  $campeonatoId = $_GET['campeonatoId'];
                  $campeonatoDescricao = RetornaDescricaoCampeonato($conexao, $campeonatoId);
            }
      
            $mensagem = "";
            $tipoAviso = "";
      
            if(isset($_GET['msg'])){
                  $mensagem = $_GET['msg'];
            }
            if (isset($_GET['tipoAviso'])) {
                  $tipoAviso = $_GET['tipoAviso'];
            } 
      
            // inicializa valores
            $jogoId = null;
            $turno = null;
            $grupo = null;
            $ordem = null;
            $rodada = null;
            $mandanteId = null;
            $visitanteId = null;
      
            if(isset($_GET['id'])){
                  $id = $_GET['id'];
      
                  $campeonatoId = $_GET['campeonato'];
                  $campeonatoDescricao = RetornaDescricaoCampeonato($conexao, $campeonatoId);
                  
                  if ($id != "") {
                        $sql = "SELECT 
                                    id,
                                    mandanteId, 
                                    visitanteId, 
                                    rodada, 
                                    turno, 
                                    confronto, 
                                    grupo,
                                    ordem
                              FROM 
                                    jogo 
                              WHERE id = '" . $id . "' ";
                        
                        $rs=$conexao->query($sql);
                        $reg=mysqli_fetch_array($rs);
                        
                        $jogoId = $id;
                        $mandanteId = $reg['mandanteId'];
                        $visitanteId = $reg['visitanteId'];
                        $rodada = $reg['rodada'];
                        $turno = $reg['turno'];
                        $confronto = $reg['confronto'];
                        $grupo = $reg['grupo'];
                        $ordem = $reg['ordem'];
                  } 		
            }
?>      

<!DOCTYPE HTML>

<html>
	<head>
		<title>Edição de jogos</title>
            <link rel="icon" type="image/png" href="imagens/favicon.png">
            <meta name="robots" content="noindex">
		<meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="assets/css/tabs.css" />
            <link rel="stylesheet" href="assets/css/modal.css" />

            <!-- Janela Modal -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

            <script type="text/JavaScript"> 
  
        jogoId = 0;
        mandanteId = 0;
        visitanteId = 0;
        idCampeonato = 0;
        goleadoresMandante = [];
        goleadoresVisitante = [];
        abrirJogo = false;

        window.onload = function(){
            var campeonato = $('#campeonatoId  option:selected').val();
            recebeValor();	
            $("#sucesso").hide();	
        }

        function recebeValor(){	
            idCampeonato = $('#campeonatoId  option:selected').val();

            setTimeout(function(){	
                $("#recebeValor").load("ajax/ajaxTabelaJogos.php",{campeonatoId:idCampeonato})
            });
        } 

	  function Nova() { location.href="cadastroJogos.php" } ;  	  
  </script>

	</head>

	<body>
		<div id="page-wrapper">
                  <div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
                  </div>
                  
                  <div id="main">
				<div class="container">                       
                        
                              <?php include ("componentes/mensagem.php") ?>
                              
                              <div class="row main-row">
                                    <div class="12u">

                                          <section class="content-header">
                                                <h2 class="tituloPagina">Cadastrar ou editar jogos</h2>				
                                          </section>

                                          <section class="cadastro"> 
                                                <form class="contact_form" method="post" action="paginas/cadastroJogos1.php" enctype="multipart/form-data">
                                          
                                                      <div class="row2">
                                                            <div class="col-25">
                                                                  <label for="edicao">Campeonato</label>
                                                            </div>

                                                            <div class="col-75">
                                                                  <select  name="campeonatoId" id="campeonatoId" class="selectCadastro" onchange="recebeValor(this.value, 0)"> <?PHP
                                                                        $sqlCampeonato="SELECT id, concat(descricao, ' - ', edicao, 'ª edição') AS descricao FROM campeonatos ORDER BY ativo DESC";

                                                                        $rsCampeonato=$conexao->query($sqlCampeonato);
                                                                        
                                                                        while($campeonato=mysqli_fetch_array($rsCampeonato))		
                                                                        {	
                                                                              if ($campeonatoId == $campeonato['id']) {
                                                                                    $itensCampeonato = $itensCampeonato."<option value='".$campeonato['id']."' selected='selected'>".$campeonato['descricao']."</option><br /> ";
                                                                              }
                                                                              else {
                                                                                    $itensCampeonato = $itensCampeonato."<option value='".$campeonato['id']."'>".$campeonato['descricao']."</option><br /> ";
                                                                              }
                                                                        }
                                                                        print $itensCampeonato; ?>       
                                                                  </select>	
                                                            </div>
                                                      </div>

                                                      <div class="row2">  <!-- ID -->
                                                            <div class="col-25">
                                                                  <label for="fname">ID do jogo</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" class="desabilitado" id="jogoId" name="jogoId" readonly value="<?php print $jogoId; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2">  <!-- Confronto -->
                                                            <div class="col-25">
                                                                  <label for="fname">Confronto</label>
                                                            </div>

                                                            <div class="col-75">

                                                                  <div class="col-in30">
                                                                        <select  name="mandanteId" id="mandanteId" class="selectCadastro">	<?PHP
                                                                              $sqlmandante="SELECT times.id, times.nome AS time, botonistas.nome AS botonista
                                                                                    FROM times INNER JOIN botonistas ON times.botonistaId = botonistas.id 
                                                                                    ORDER BY times.nome, botonistas.nome";
                                                                              
                                                                              $rsmandante=$conexao->query($sqlmandante);
                                                                              
                                                                              while($mandante=mysqli_fetch_array($rsmandante))		
                                                                              {
                                                                                    $descricao = $mandante['time']." - ".$mandante['botonista'];
                                                                                    if ($mandanteId == $mandante['id']) {
                                                                                          $itens_mandante = $itens_mandante."<option value='".$mandante['id']."' selected='selected'>".
                                                                                                $descricao."</option><br /> ";
                                                                                    }
                                                                                    else {
                                                                                          $itens_mandante = $itens_mandante."<option value='".$mandante['id']."'>".$descricao."</option><br /> ";
                                                                                    }
                                                                              }
                                                                              print $itens_mandante;
                                                                              ?>       
                                                                        </select>
                                                                  </div>

                                                                  <div class="col-in5"> x  </div>

                                                                  <div class="col-in30">
                                                                        <select  name="visitanteId" id="visitanteId" class="selectCadastro"> <?PHP
                                                                              $sqlvisitante="SELECT times.id, times.nome AS time, botonistas.nome AS botonista
                                                                                    FROM times INNER JOIN botonistas ON times.botonistaId = botonistas.id 
                                                                                    ORDER BY times.nome, botonistas.nome";
                                                                              
                                                                              $rsvisitante=$conexao->query($sqlvisitante);
                                                                              
                                                                              while($visitante=mysqli_fetch_array($rsvisitante))		
                                                                              {
                                                                                    $descricao = $visitante['time']." - ".$visitante['botonista'];
                                                                                    if ($visitanteId == $visitante['id']) {
                                                                                          $itens_visitante = $itens_visitante."<option value='".$visitante['id']."' selected='selected'>".
                                                                                                $descricao."</option><br /> ";
                                                                                    }
                                                                                    else {
                                                                                          $itens_visitante = $itens_visitante."<option value='".$visitante['id']."'>".$descricao."</option><br /> ";
                                                                                    }
                                                                              }
                                                                              print $itens_visitante;
                                                                              ?>       
                                                                        </select>
                                                                  </div>
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Rodada -->
                                                            <div class="col-25">
                                                                  <label for="rodada">Rodada</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="rodada" name="rodada" name="rodada" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $rodada; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Turno -->
                                                            <div class="col-25">
                                                                  <label for="turno">Turno</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="turno" name="turno" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $turno; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Grupo -->
                                                            <div class="col-25">
                                                                  <label for="grupo">Grupo</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="grupo" name="grupo" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $grupo; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Ordem -->
                                                            <div class="col-25">
                                                                  <label for="ordem">Ordem</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="ordem" name="ordem" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $ordem; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Botões -->
                                                            <div class="col-25">
                                                                  <label for="data"></label>
                                                            </div>
                                                            <div class="col-75">                                                                
                                                                  <div class="botoes">
                                                                        <button type="submit" id="submit" class="btnSalvar" name="acao" value="inc">Salvar</button>

                                                                        <button type="submit" class="btnExcluir" name="acao" value="exc">Excluir</button>

                                                                        <button type="button" class="btnCancelar" onClick="Nova()" >Cancelar</button>
                                                                  </div>
                                                            </div>
                                                      </div>                          
                                                      
				                        </form>   
                                          </section>

                                          <div name="recebeValor" id="recebeValor"></div>	
                                          
                                    </div>
					</div>
				</div>
                  </div>     
            </div>
            
            <div id="footer-wrapper">
                  <?php include("componentes/rodape.php") ?>
            </div>

            <script> 
                  $(document).ready(function() { 
                        $('#dadosConfiguracoes').submit(function() {
                        var dados = $('#dadosConfiguracoes').serialize();
            
                        $.ajax({
                              type : 'POST',
                              url  : 'modal/configuracoesCampeonato1.php',
                              data : dados,
                              dataType: 'json',
                              success :  function(response){ 
                                    if (response.doisGrupos == 0) {
                                          $('#grupoUnico').modal('toggle');
                                          $('#grupoUnico').modal({
                                                escapeClose: false,
                                                clickClose: false,
                                                showClose: false
                                          });
                                    } 
                                    else {
                                          $('#doisGrupos').modal('toggle');
                                    }                                
                                                                                              
                              },

                              error: function(response){
                                    alert("falha");
                              }
                        });
            
                        return false;
                        });
                  });

                  $(document).ready(function() { 
                        $('#dadosTimes').submit(function() {
                        var dados = $('#dadosTimes').serialize();
            
                        $.ajax({
                              type : 'POST',
                              url  : 'modal/timesCampeonato1.php',
                              data : dados,
                              dataType: 'json',
                              success :  function(response){
                                    alert (response.mensagem);                                                 
                                    //$('#grupoUnico').hide();                               
                              },

                              error: function(response){
                                    alert (response.mensagem); 
                              }
                        });

                        $("#salvarJogo").on("click", function(e) {
                              e.preventDefault();
                              $("#dialog").dialog("open");
                        });
            
                        return false;
                        });
                  });

                  $('a[href="#configuracoes"]').click(function(event) {
                        event.preventDefault();
                        $(this).modal({
                              escapeClose: false,
                              clickClose: false,
                              showClose: false
                        });                        
                  });
            
            </script>
            
            <script src="plugins/select2/select2.full.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script> 
            <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.zebra-datepicker.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/skel-viewport.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>     
          
	</body>
</html>