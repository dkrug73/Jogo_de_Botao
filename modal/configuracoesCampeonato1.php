<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
      include "../utils/funcoes.php";
      include "../classes.php";
 
      if (isset($_POST)) {
            $campeonato = new Campeonato();

            $campeonato->id = (isset($_POST['campeonatoId']))? $_POST['campeonatoId']: '';
            $campeonato->formulaId = (isset($_POST['formulaId']))? $_POST['formulaId']: '';

            $campeonato->doisTurnos = 0;
            if(isset($_POST['turnos'])) {
                  $campeonato->doisTurnos = 1;
            }

            $campeonato->doisGrupos = 0;
            if(isset($_POST['grupos'])) {
                  $campeonato->doisGrupos = 1;
            }

            $campeonato->evitarConfrontos = 0;
            if(isset($_POST['confrontos'])) {
                  $campeonato->evitarConfrontos = 1;
            }

            $campeonato->finais = (isset($_POST['finais']))? $_POST['finais']: ''; // quatroPrimeirosGeral / doisMelhoresCadaTurno / doisMelhores / campeaoCadaTurno           
            $campeonato->jogos = (isset($_POST['jogos']))? $_POST['jogos']: ''; // zeroJogo / umJogo  / doisJogos

            $resultado = $campeonato->InserirConfiguracoes($campeonato, $conexao);
            
            $array  = array('doisGrupos' => $campeonato->doisGrupos, 'mensagem' => $resultado);  
            echo json_encode($array);
      }

      mysqli_close($conexao);
?>