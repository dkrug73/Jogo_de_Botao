<div class="modal-content">
      <div class="modal-header">            
            <h2 style="color: white;">CONFIGURAÇÕES DO CAMPEONATO</h2>
      </div>

      <div class="modal-body">  
            <div class="configuracoes"> 
            
                  <fieldset>
                        <legend>&nbsp;&nbsp;Selecione a fórmula&nbsp;&nbsp;</legend>
                        <select  name="formulaId" id="formulaId" class="selectConfiguracoes">										
                              <?PHP
                              $sql_formula="SELECT id, descricao FROM formulas ORDER BY descricao";
                              $rs_formula=$conexao->query($sql_formula);
                              
                              while($formula=mysqli_fetch_array($rs_formula))		
                              {
                                    if ($formulaId == $formula['id']) {
                                          $itens_formula = $itens_formula."<option value='".$formula['id']."' selected='selected'>".
                                          $formula['descricao']."</option><br /> ";
                                    }
                                    else {
                                          $itens_formula = $itens_formula."<option value='".$formula['id']."'>".$formula['descricao']."</option><br /> ";
                                    }
                              }
                              print $itens_formula;
                              ?>       
                        </select>
                  </fieldset>

                  <fieldset>
                        <legend>&nbsp;&nbsp;Informe o tipo de final&nbsp;&nbsp;</legend>
                        <select name="finais" id="finais" class="selectConfiguracoes">
                              <option value="quatroPrimeirosGeral">Confrontos entre os 4 primeiros da Geral</option>
                              <option value="doisMelhoresCadaTurno">Confrontos entre os 2 melhores de cada turno</option>
                              <option value="doisMelhores">Final entre os 2 melhores colocados da Geral</option>
                              <option value="campeaoCadaTurno">Final entre os campeões de cada turno</option>
                              <option value="campeaoMelhorGeral">Sem final - campeão melhor time da geral</option>
                        </select> 
                  </fieldset>
                  
                  <fieldset>
                        <legend>&nbsp;&nbsp;Turno/grupos&nbsp;&nbsp;</legend>
                        <label class="caixaDialogo"> 2 Turnos
                        <input type="checkbox" name="turnos" id="turnos" value="doisTurnos">
                              <span class="checkmark"></span>
                        </label>

                        <label class="caixaDialogo"> 2 Grupos
                        <input type="checkbox"name="grupos" id="grupos" value="doisGrupos">
                              <span class="checkmark"></span>
                        </label>
                  </fieldset>

                  <fieldset>
                        <legend>&nbsp;&nbsp;Confrontos&nbsp;&nbsp;</legend>
                        <label class="caixaDialogo"> Evitar confrontos entre times do mesmo participante
                        <input type="checkbox" class="teste" name="confrontos" id="confrontos">
                              <span class="checkmark"></span>
                        </label>
                  </fieldset>
                  
                  <fieldset>
                        <legend>&nbsp;&nbsp;Jogos&nbsp;&nbsp;</legend>
                        
                        <label class="containerRadio"> 1 jogo
                              <input type="radio" name="jogos" id="radio-1" value="umJogo">
                              <span class="checkmarkRadio"></span>
                        </label>

                        <label class="containerRadio"> 2 jogos
                              <input type="radio" name="jogos" id="radio-1" value="doisJogos">
                              <span class="checkmarkRadio"></span>
                        </label>

                        <label class="containerRadio"> Sen final
                              <input type="radio" name="jogos" id="radio-1" value="zeroJogo">
                              <span class="checkmarkRadio"></span>
                        
                  </fieldset>	

                  <input type="text" name="campeonatoId" style="display:none;" value="<?php print $campeonatoId ?>">  
            </div>                   
                  
      </div>
</div>