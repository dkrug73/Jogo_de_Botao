<div class="modal-content">
      <div class="modal-header">
            <h2 style="color: white;">CONFIGURAÇÕES DO CAMPEONATO</h2>
      </div>

      <div class="modal-body">
            <div class="configuracoes"> 
                  <fieldset>
                        <legend>&nbsp;&nbsp;Grupo único&nbsp;&nbsp;</legend>           

                        <select class="js-example-basic-multiple" name="timeId[]" id="timeId[]" multiple="multiple" style="width: 100%;"> <?PHP
                              $sqlA = "SELECT timeId, times.nome as nomeTime, botonistas.nome AS nomeBotonista
                                    FROM timescampeonato INNER JOIN 
                                    times ON timeId = times.id INNER JOIN 
                                    botonistas ON botonistaId = botonistas.id 
                                    WHERE campeonatoId = '".$campeonatoId."' AND grupo = 'A'";

                              $rsA=$conexao->query($sqlA);

                              $sql_time="SELECT times.id, times.nome AS nome, botonistas.nome AS botonistaNome FROM times 
                                    INNER JOIN botonistas ON times.botonistaId = botonistas.id 
                                    WHERE times.id not in (SELECT timeId FROM timescampeonato WHERE campeonatoId = '".$campeonatoId."' AND grupo  = 'A') ORDER BY botonistas.nome, times.nome";                       

                              $rs_time=$conexao->query($sql_time);

                              while($idTimeA=mysqli_fetch_array($rsA)) 
                              {
                                    $timeNome = $idTimeA['nomeTime']." - ".$idTimeA['nomeBotonista']; 
                                    $itens_time = $itens_time."<option value='".$idTimeA['timeId']."' selected='selected'>".$timeNome."</option><br /> ";
                              }

                              while($time=mysqli_fetch_array($rs_time))		
                              {	
                                    $timeNome = $time['nome']." - ".$time['botonistaNome']; 
                                    $itens_time = $itens_time."<option value='".$time['id']."'>".$timeNome."</option><br /> ";   
                              }

                              print $itens_time; ?>       
                        </select>   
                  </fieldset>

                  <input type="text" name="campeonatoId" style="display:none;" value="<?php print $campeonatoId ?>">
            </div>
      </div>
</div>
