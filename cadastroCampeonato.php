<?php
      SESSION_START();
	include "conexao/dbConexao.php";
      include "utils/funcoes.php";      
      
      $campeonatoId = RetornaCampeonatoAtivo($conexao)[0];
      
      $_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
      
      $mensagem = "";
      $tipoAviso = "";

      if(isset($_GET['msg'])){
            $mensagem = $_GET['msg'];
      }
      if (isset($_GET['tipoAviso'])) {
            $tipoAviso = $_GET['tipoAviso'];
      } 
        
      // inicializa valores
      $id = null;
      $campeonatoId = null;
      $descricao = null;
      $edicao = null;
      $local = null;
      $classeRanking = null;	
      $data = null;
      $formulaId = null;
      $doisGrupos = null;
      $doisTurnos = null;
      $evitarConfrontos = null;
      $finais = null;
      $jogos = null;
      $ativo = 0;     

      if(isset($_GET['id'])){
            $id = $_GET['id'];
            
            if ($id != "") {
                  $sql = "SELECT 
                              id, 
                              descricao,
                              edicao,
                              local, 
                              classeRanking,
                              date_format(data, '%d/%m/%Y') AS data,
                              formulaId,
                              doisGrupos,
                              doisTurnos,
                              evitarConfrontos,
                              finais,
                              jogos, 
                              ativo
                        FROM 
                              campeonatos 
                        WHERE id = '" . $id . "' ";
                  
                  $rs=$conexao->query($sql);
                  $reg=mysqli_fetch_array($rs);
                  
                  $campeonatoId = $id;
                  $descricao = $reg['descricao'];
                  $edicao = $reg['edicao'];
                  $classeRanking = $reg['classeRanking'];
                  $local = $reg['local'];
                  $data = $reg['data'];
                  $formulaId = $reg['formulaId'];
                  $doisGrupos = $reg['doisGrupos'];
                  $doisTurnos = $reg['doisTurnos'];
                  $evitarConfrontos = $reg['evitarConfrontos'];
                  $finais = $reg['finais'];
                  $jogos = $reg['jogos'];
                  $ativo = $reg['ativo'];
            } 		
      }		
      
      $sql="SELECT
                  campeonatos.id, 
                  campeonatos.descricao,
                  edicao,
                  local, 
                  classeRanking,
                  date_format(data, '%d/%m/%Y') AS data,
                  formulas.descricao AS formula,
                  CASE doisGrupos 
                        WHEN 0 THEN 'não' 
                  WHEN 1 THEN 'sim' END AS doisGrupos,
                  CASE doisTurnos 
                        WHEN 0 THEN 'não' 
                        WHEN 1 THEN 'sim' END AS doisTurnos,
                  CASE evitarConfrontos 
                        WHEN 0 THEN 'não' 
                        WHEN 1 THEN 'sim' END AS evitarConfrontos,
                  CASE finais 
                        WHEN 'quatroPrimeirosGeral' THEN 'quatro primeiros' 
                        WHEN 'doisMelhoresCadaTurno' THEN 'dois melhores' 
                        WHEN 'doisMelhores' THEN 'sem final' 
                        WHEN 'campeaoCadaTurno' THEN 'campeão cada turno' END AS finais,
                  CASE jogos 
                        WHEN 'zeroJogo' THEN 'não' 
                        WHEN 'umJogo' THEN '1 jogo' 
                        WHEN 'doisJogos' THEN '2 jogos' END AS jogos,
                  CASE ativo 
                        WHEN 0 THEN 'inativo' 
                        WHEN 1 THEN 'ativo' END as ativo
            FROM 
                  botao.campeonatos LEFT JOIN 
                  botao.formulas ON formulas.id = formulaId
            ORDER BY
                  campeonatos.descricao, edicao";
            
      $rs=$conexao->query($sql);

      $haJogos = false;
      $temTimesConfigurados = false;

      $haJogos = VerificaSeHaJogosCampeonato($conexao, $campeonatoId);
      $temTimesConfigurados = VerificaSeTemTimesConfigurados($conexao, $campeonatoId);    
?>      

<!DOCTYPE HTML>
<html>
	<head>
		<title>Cadastro de Campeonato</title>
            <link rel="icon" type="image/png" href="imagens/favicon.png">
            <meta name="robots" content="noindex">
		<meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="assets/css/tabs.css" />
            <link rel="stylesheet" href="assets/css/modal.css" />

            <!-- MultiSelect -->
            <link rel="stylesheet" href="plugins/select2/select2.min.css"> 

            <!-- Janela Modal -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
           
            <script type="text/javascript">
                  function Nova() { location.href="cadastroCampeonato.php" } ;    
                  
                  $(document).ready(function() {
                        $('.js-example-basic-multiple').select2();
                  });
                 
            </script>

	</head>

	<body>
		<div id="page-wrapper">
                  <div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
                  </div>
                  
                  <div id="main">
				<div class="container">                       
                        
                              <?php include ("componentes/mensagem.php") ?>
                              
                              <div class="row main-row">
                                    <div class="12u">

                                          <section class="content-header">
                                                <h2 class="tituloPagina">Cadastro de Campeonatos</h2>				
                                          </section>

                                          <section class="cadastro"> 
                                                <form class="contact_form" method="post" action="paginas/cadastroCampeonato1.php" enctype="multipart/form-data">
                                                                                                          
                                                      <div class="row2">  <!-- ID -->
                                                            <div class="col-25">
                                                                  <label for="fname">ID</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" class="desabilitado" id="id" name="id" readonly value="<?php print $campeonatoId; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Descrição -->
                                                            <div class="col-25">
                                                                  <label for="descricao">Descrição</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" id="descricao" name="descricao" required value="<?php print $descricao; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Edição -->
                                                            <div class="col-25">
                                                                  <label for="edicao">Edição</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="edicao" name="edicao" required pattern="[0-9]+$" 
                                                                  title="somente números" value="<?php print $edicao; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Local -->
                                                            <div class="col-25">
                                                                  <label for="local">Local</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" id="local" name="local" required value="<?php print $local; ?>">
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row2"> <!-- Classe Ranking -->
                                                            <div class="col-25">
                                                                  <label for="classe">Classe Ranking</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" id="classeRanking" name="classeRanking" required value="<?php print $classeRanking; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Data -->
                                                            <div class="col-25">
                                                                  <label for="data">Data</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <input type="text" class="p" id="data" name="data" value="<?php print $data; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Campeonato atual -->
                                                            <div class="col-25">
                                                                  <label for="ativo">Campeonato atual</label>
                                                            </div>  
                                                          
                                                            <div class="checkbox">                                                                 
                                                                  <div class="col-ch25">
                                                                        <div class='checkboxFour'>   
                                                                              <?php 
                                                                              if ($ativo == 0) { 
                                                                                    print "                                                                                    
                                                                                          <input type='checkbox' value='0' id='checkboxFourInput' name='checkboxFourInput' style='visibility: hidden;' />                                                                                    
                                                                                          <label for='checkboxFourInput'></label>";
                                                                                    }
                                                                              else {
                                                                                    print "
                                                                                          <input type='checkbox' value='1' id='checkboxFourInput' name='checkboxFourInput' style='visibility: hidden;'checked='true' />                                                                              
                                                                                          <label for='checkboxFourInput'></label>";
                                                                              } ?>
                                                                        </div>
                                                                  </div>
                                                                 
                                                            </div>  
                                                      </div>  

                                                      <div class="row2"> <!-- Botões -->
                                                            <div class="col-25">
                                                                  <label for="data"></label>
                                                            </div>
                                                            <div class="col-75">
                                                                
                                                                  <div class="botoes">
                                                                        <button type="submit" id="submit" class="btnSalvar" name="acao" value="inc">Salvar</button>

                                                                        <button type="submit" class="btnExcluir" name="acao" value="exc">Excluir</button>

                                                                        <button type="button" class="btnCancelar" onClick="Nova()" >Cancelar</button>
                                                                        
                                                                        <?php
                                                                        if(!empty($campeonatoId) AND !$haJogos) { ?>
                                                                              <a class="btn" href="#configuracoes" ><i class="fa fa-edit" style="margin: 0 2px 0 15px;"></i>Configurações</a> <?php	
                                                                        } 
                                                                        
                                                                        if ($haJogos) { ?>
                                                                              <a class="btn" href="cadastroJogos.php?campeonatoId='<?php print $campeonatoId; ?>' " >
                                                                              <i class="fa fa-edit" style="margin: 0 2px 0 15px;"></i>Editar jogos</a> <?php
                                                                        } ?>
                                                                  </div>
                                                            </div>
                                                      </div>                                                    
                                                </form>

                                          </section>
                                          
                                          <!-- Modal Configurações -->
                                          <div class="modal" id="configuracoes" >
                                                <form id="dadosConfiguracoes" action="" method="post">     
                                                      <?php include ("modal/configuracoesCampeonato.php")  ?>

                                                      <div class="rodapeJogo">
                                                            <button type="submit" id="salvarJogo" class="button botaoProximo" name="acao" value="inc" > Próximo </button>           
                                                      </div>
                                                </form>
                                          </div>

                                          <!-- Modal TimesCampeonato -->
                                          <div class="modal" id="grupoUnico" >
                                                <form id="dadosTimes" action="" method="post">     
                                                      <?php include ("modal/timesCampeonato.php")  ?>

                                                      <div class="rodapeJogo">
                                                            <button type="submit" id="salvarJogo" class="button botaoProximo" name="acao" value="inc" > Salvar </button>           
                                                      </div>
                                                </form>   

                                                <a href="#" rel="modal:close">Fechar</a> 
                                          </div>

                                          <!-- Modal TimesCampeonato - 2 grupos -->
                                          <div class="modal" id="doisGrupos" >
                                                <form id="dadosTimes2" action="" method="post">     
                                                      <?php include ("modal/timesCampeonatoGrupos.php")  ?>

                                                      <div class="rodapeJogo">
                                                            <button type="submit" id="salvarJogo" class="button botaoProximo" name="acao" value="inc" > Salvar </button>           
                                                      </div>
                                                </form>   

                                                <a href="#" rel="modal:close">Fechar</a>   
                                          </div>
                                    
                                          <div class = "box tabela">                            
                                                <div class="box box-solid box-success">   
                                                      <div class="box-body">
                                                            <div class="box-body no-padding">
                                                                  <table class="table tabela1" >
                                                                        <thead class="thead-default" >
                                                                              <tr class="cabecalhoTabela fundoCadastro">
                                                                                    <th class="linhaCabecalho">ID</th>
                                                                                    <th class="linhaCabecalho">Descrição</th>
                                                                                    <th class="linhaCabecalho">Data</th>
                                                                                    <th class="linhaCabecalho">Fórmula</th>
                                                                                    <th class="linhaCabecalho">Grupos</th>
                                                                                    <th class="linhaCabecalho">Turnos</th>
                                                                                    <th class="linhaCabecalho">Finais</th>
                                                                                    <th class="linhaCabecalho">Jogos na final</th>
                                                                                    <th class="linhaCabecalho">Ativo</th>
                                                                              </tr> 
                                                                        </thead> 

                                                                        <?PHP
                                                                        if (isset($rs)) {
                                                                              while($reg=mysqli_fetch_array($rs)) { 
                                                                                    $id = $reg["id"];
                                                                                    $descricao = $reg["descricao"];
                                                                                    $edicao = $reg["edicao"];
                                                                                    $local = $reg["local"];
                                                                                    $classeRanking = $reg["classeRanking"];
                                                                                    $data = $reg["data"];
                                                                                    $formula = $reg["formula"];
                                                                                    $doisGrupos = $reg["doisGrupos"];
                                                                                    $doisTurnos = $reg["doisTurnos"];
                                                                                    $evitarConfrontos = $reg["evitarConfrontos"];
                                                                                    $finais = $reg["finais"];
                                                                                    $jogos = $reg["jogos"];
                                                                                    $ativo = $reg["ativo"];?>														
                                                                                                                  
                                                                                    <tr onclick="location.href = 'cadastroCampeonato.php?id=<?PHP print $id;?>'; " 
                                                                                          style='cursor: pointer;'> 
                                                                                          
                                                                                          <td class="linha"><?PHP print $id; ?></td>
                                                                                          <td class="linha"><?PHP print $descricao; ?></td>
                                                                                          <td class="linha"><?PHP print $data; ?></td>
                                                                                          <td class="linha"><?PHP print $formula; ?></td>
                                                                                          <td class="linha"><?PHP print $doisGrupos; ?></td>
                                                                                          <td class="linha"><?PHP print $doisTurnos; ?></td>
                                                                                          <td class="linha"><?PHP print $finais; ?></td>
                                                                                          <td class="linha"><?PHP print $jogos; ?></td>
                                                                                          <td class="linha"><?PHP print $ativo; ?></td>
                                                                                    </tr>	 <?PHP 
                                                                              } 
                                                                        } else { ?>                                         
                                                                              <tr>       
                                                                                    <td class="linha">&nbsp;</td>                  
                                                                                    <td class="linha">&nbsp;</td>
                                                                                    <td class="linha">&nbsp;</td>
                                                                                    <td class="linha">&nbsp;</td>
                                                                              </tr>	<?php
                                                                        } ?>                        
                                                                  </table>
                                                            </div>
                                                      </div> 
                                                </div>   
                                          </div>  
                                    </div>
					</div>
				</div>
                  </div>     
            </div>
            
            <div id="footer-wrapper">
                  <?php include("componentes/rodape.php") ?>
            </div>

            <script> 
                  $(document).ready(function() { 
                        $('#dadosConfiguracoes').submit(function() {
                        var dados = $('#dadosConfiguracoes').serialize();
            
                        $.ajax({
                              type : 'POST',
                              url  : 'modal/configuracoesCampeonato1.php',
                              data : dados,
                              dataType: 'json',
                              success :  function(response){ 
                                    if (response.doisGrupos == 0) {
                                          $('#grupoUnico').modal('toggle');
                                          $('#grupoUnico').modal({
                                                escapeClose: false,
                                                clickClose: false,
                                                showClose: false
                                          });
                                    } 
                                    else {
                                          $('#doisGrupos').modal('toggle');
                                    }                                
                                                                                              
                              },

                              error: function(response){
                                    alert("falha");
                              }
                        });
            
                        return false;
                        });
                  });

                  $(document).ready(function() { 
                        $('#dadosTimes').submit(function() {
                        var dados = $('#dadosTimes').serialize();
            
                        $.ajax({
                              type : 'POST',
                              url  : 'modal/timesCampeonato1.php',
                              data : dados,
                              dataType: 'json',
                              success :  function(response){
                                    alert (response.mensagem);                              
                              },

                              error: function(response){
                                    alert (response.mensagem); 
                              }
                        });

                        $("#salvarJogo").on("click", function(e) {
                              e.preventDefault();
                              $("#dialog").dialog("open");
                        });
            
                        return false;
                        });
                  });

                  $(document).ready(function() { 
                        $('#dadosTimes2').submit(function() {
                        var dados = $('#dadosTimes2').serialize();
            
                        $.ajax({
                              type : 'POST',
                              url  : 'modal/timesCampeonato1.php',
                              data : dados,
                              dataType: 'json',
                              success :  function(response){
                                    alert (response.mensagem);                                                 
                                    //$('#grupoUnico').hide();                               
                              },

                              error: function(response){
                                    alert (response.mensagem); 
                              }
                        });

                        $("#salvarJogo").on("click", function(e) {
                              e.preventDefault();
                              $("#dialog").dialog("open");
                        });
            
                        return false;
                        });
                  });

                  $('a[href="#configuracoes"]').click(function(event) {
                        event.preventDefault();
                        $(this).modal({
                              escapeClose: false,
                              clickClose: false,
                              showClose: false
                        });                        
                  });
            
            </script>
            
            <script src="plugins/select2/select2.full.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script> 
            <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.zebra-datepicker.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/skel-viewport.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>     
          
	</body>
</html>