<style>
      .alert-success {
            color: #fff;
            background-color: #00a65a !important;
      }

      .alert-warning {
            color: #fff;
            background-color: #f39c12 !important;
      }

      .alert-danger {
            background-color: #dd4b39 !important;
            color: #fff;
      }      

      .alert-dismissable, .alert-dismissible {
            padding-right: 35px;
      }
      .alert {
            padding: 15px;
            padding-right: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            border-left-color: transparent;
            border-radius: 4px;
      }

      .alert-dismissable .close, .alert-dismissible .close {
            position: relative;
            top: -2px;
            right: -21px;
            color: inherit;
      }      

      .close {
            float: right;
            font-size: 21px;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            filter: alpha(opacity=20);
            opacity: .2;
      }

      .alert h4 {
            margin-top: 0;
            color: inherit;
      }
      .h4, h4 {
            font-size: 24px;
      }

</style>

<script type="text/javascript">
      setTimeout(function() {
	$('#divdesaparecer').fadeOut('slow');}, 1800);
</script>

<div id="divdesaparecer">

 <?php     
      
      if($tipoAviso == "erro"){ ?>
            <div class="alert alert-danger alert-dismissable" style="margin: -60px 0 70px 0;">
                  <h4><i class="icon fa fa-ban" style="padding-right: 10px;"></i>Erro!</h4>
                  <?php print $mensagem; ?>
            </div>
            <?php
      }
      else if ($tipoAviso == "alerta"){ ?>
            <div class="alert alert-warning alert-dismissable" style="margin: -60px 0 70px 0;">
                  <h4><i class="icon fa fa-warning" style="padding-right: 10px;"></i> Aviso!</h4>
                  <?php print $mensagem; ?>
            </div>					
      <?php 
      } 
      else if($tipoAviso == "sucesso"){ ?>
            <div class="alert alert-success alert-dismissable" style="margin: -60px 0 70px 0;">
                  <h4><i class="icon fa fa-check" style="padding-right: 10px;"></i>Sucesso!</h4>
                  <?php print $mensagem; ?>
            </div>
      <?php 
      } ?> 
</div>  

