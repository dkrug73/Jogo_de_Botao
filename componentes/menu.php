<?php 
    $pagina = $_SERVER['REQUEST_URI'];

?>


<style>
.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	z-index: 1;
	width: 157px;

	background-image: -moz-linear-gradient(top, #008ead, #007b9d);
	background-image: -webkit-linear-gradient(top, #008ead, #007b9d);
	background-image: linear-gradient(#008ead, #008ead, #007b9d);
	-moz-box-sizing: content-box;
	-webkit-box-sizing: content-box;
	-ms-box-sizing: content-box;
	border-radius: 10px;
	border: solid 1px #006e8b;
	background-color: #007b9d;
	
}

.dropdown:hover .dropdown-content {
    	display: block;
}

.drop {
	height: 50px;
	line-height: 44px;
}

</style>

<div class="container">
	<div class="row">
		<div class="12u">

			<header id="header">
				<h1><a href="#" id="logo">FUTEBOL DE BOTÃO</a></h1>
				<nav id="nav">

					<?php 
					if ($pagina == "/index.php") { ?>
						<a href="index.php" class="current-page-item">Início</a> <?php
					} else { ?>
						<a href="index.php" >index</a> <?php
					}?>

					<?php 
					if ($pagina == "/jogos.php") { ?>
						<a href="jogos.php" class="current-page-item">Jogos</a> <?php
					} else { ?>
						<a href="jogos.php" >Jogos</a> <?php
					}?>

					<?php 
					if ($pagina == "/estatisticas.php") { ?>
						<a href="estatisticas.php" class="current-page-item">Estatísticas</a> <?php
					} else { ?>
						<a href="estatisticas.php" >Estatísticas</a> <?php
					}?>					

					<div class="dropdown">

						<?php 
						if ($pagina == "/cadastroCampeonato.php") { ?>
							<a class="current-page-item">Cadastros</a> <?php
						} else { ?>
							<a >Cadastros</a> <?php
						}?>
						
						<div class="dropdown-content">
							<div class="drop">
								<a href="cadastroCampeonato.php" style="width: 157px;">Campeonato</a></div>
							<div class="drop">
								<a href="cadastroJogos.php" style="width: 157px;">Jogos</a></div>
							<div class="drop">
								<a href="cadastro.php" style="width: 157px;">Jogador</a></div>
							<div class="drop">
								<a href="cadastro.php" style="width: 157px;">Time</a></div>
							<div class="drop">
								<a href="cadastroFormula.php" style="width: 157px;">Fórmula</a></div>
								
							<div class="drop">
								<a href="cadastro.php" style="width: 157px;">Usuario</a></div>
						</div>
					</div> 
				</nav>				
			</header>

		</div>
	</div>
</div>