<?php
      //include "../utils/funcoes.php";
 
class Campeonato {
  
      public $id;
      public $descricao;
      public $edicao;  
      public $local;
      public $classeRanking;
      public $data;
      public $ativo;

      public function GravarCampeonato($campeonato, $conexao){
            $sql = "
                  INSERT INTO campeonatos 
                        (descricao, edicao, classeRanking, local, data, ativo) 
                  VALUES 
                        ('$campeonato->descricao', '$campeonato->edicao','$campeonato->classeRanking', 
                        '$campeonato->local', '$campeonato->data', '$campeonato->ativo') ";

            $conexao->query($sql);

            return $conexao->insert_id; 
      } 

      public function ExcluirCampeonato($id, $conexao){
            $sql = "DELETE FROM campeonatos WHERE id = '" . $id . "' ";	
		
		return $conexao->query($sql);	
      }     

      public function AlterarCampeonato($campeonato, $conexao){
            $sql = "
                  UPDATE 
                        campeonatos 
                  SET 
                        descricao = '$campeonato->descricao', 
                        edicao = '$campeonato->edicao', 
                        local = '$campeonato->local',
                        classeRanking = '$campeonato->classeRanking',
                        data = '$campeonato->data',
                        ativo = '$campeonato->ativo'	
                  WHERE 
                        id = '".$campeonato->id."' ";

            return $conexao->query($sql);
      }

      public function InserirConfiguracoes($campeonato, $conexao) {
            $sql = "
                  UPDATE 
                        campeonatos 
                  SET 
                        formulaId = '$campeonato->formulaId', 
                        doisTurnos = '$campeonato->doisTurnos', 
                        doisGrupos = '$campeonato->doisGrupos',
                        evitarConfrontos = '$campeonato->evitarConfrontos',
                        finais = '$campeonato->finais',
                        jogos = '$campeonato->jogos'	
                  WHERE 
                        id = '".$campeonato->id."' ";

            return $conexao->query($sql);
      }

      public function ApagarTimesDoCampeonato($campeonatoId, $conexao) {
            $sql = "DELETE FROM timesCampeonato WHERE campeonatoId = '".$campeonatoId."' ";
            
            return $conexao->query($sql);
      }

      public function GravarTimesCampeonato($campeonatoId, $times, $grupo, $conexao) {
            foreach ($times as $time) {
                  $sql = "INSERT INTO timesCampeonato (campeonatoId, timeId, grupo) VALUES ('$campeonatoId', '$time', '$grupo') ";                 

                  if (!$conexao->query($sql)) return false;
            }

            return true;
      }

      public function GerarJogos($campeonatoId, $times, $grupo, $dadosCampeonato, $conexao) {            
            $doisGrupos = $dadosCampeonato[0];
            $doisTurnos = $dadosCampeonato[1];
            $evitarConfrontos = $dadosCampeonato[2];
            $finais = $dadosCampeonato[3];
            $jogos = $dadosCampeonato[4];
            $formulaId = $dadosCampeonato[5];
            $classeRanking = $dadosCampeonato[6];
            
            $sqlFormulaJogos = "
                  SELECT 
                        mandante, 
                        visitante, 
                        rodada 
                  FROM 
                        formulaJogos 
                  WHERE 
                        formulaId = '".$formulaId."'
                  ORDER BY
                        rodada,
                        id";

            $rsFormulaJogos=$conexao->query($sqlFormulaJogos);

            $ordem = 1;
            $mensagem="Jogos gravados com sucesso.";
            $tipoAviso = "sucesso";

            while($regFormulaJogos=mysqli_fetch_array($rsFormulaJogos)) {
                  $mandante=$regFormulaJogos['mandante'];
                  $visitante=$regFormulaJogos['visitante'];
                  $rodada=$regFormulaJogos['rodada'];

                  $mandanteId = $times[$mandante - 1];
                  $visitanteId = $times[$visitante - 1];

                  if ($evitarConfrontos ) {
                        $botonistaMandante = RetornaBotonista($conexao, $mandanteId);
                        $botonistaVisitante = RetornaBotonista($conexao, $visitanteId);                  

                        if ($botonistaMandante == $botonistaVisitante) continue;
                  }
                  
                  if (!GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, 1, $grupo, $ordem)) return false;

                  if ($doisTurnos OR $doisGrupos) {
                        if (!GravarJogo($conexao,$campeonatoId, $mandanteId, $visitanteId, $rodada, 2, $grupo, $ordem)) return false;
                  }                 

                  $ordem++;
            } 

            if (!GravarTabela($conexao, $campeonatoId, $times, "1")) return false;

            if (!GravarTabela($conexao, $campeonatoId, $times, "G"))  return false;

            if ($doisGrupos == "1" OR $doisTurnos == "1") {
                  if (!GravarTabela($conexao, $campeonatoId, $times, "2")) return false;
            }
            
            return true;
      }           
}   

class Formula {
      public $id;
      public $descricao;
      public $quantidadeTimes;

      public function GravarFormula($formula, $conexao) {
            $sql = "
                  INSERT INTO formulas 
                        (descricao, quantidadeTimes) 
                  VALUES 
                        ('$formula->descricao', '$formula->quantidadeTimes') ";
			
                  $resultado=$conexao->query($sql);
                  
                  return $conexao->insert_id;
      }

      public function AlterarFormula($formula, $conexao) {
            $sql = "
                  UPDATE 
                        formulas 
                  SET 
                        descricao = '$formula->descricao', 
                        quantidadeTimes = '$formula->quantidadeTimes'
                  WHERE 
                        id = '".$formula->id."' ";
		
			return $conexao->query($sql);
      }

      public function ExcluirFormula($id, $conexao) {
            $sql = "DELETE FROM formulas WHERE id = '" . $id . "' ";	
		return $conexao->query($sql);
      }
}

class FormulaJogo {
      public $id;
      public $formulaId;
      public $rodada;
      public $mandante;
      public $visitante;
      
      public function GravarFormula($formulaJogo, $conexao) {
            $sql = "
                  INSERT INTO formulaJogos 
                        (formulaId, rodada, mandante, visitante) 
                  VALUES 
                        ('$formulaJogo->formulaId','$formulaJogo->rodada', '$formulaJogo->mandante', '$formulaJogo->visitante') ";
			
                  $resultado=$conexao->query($sql);
                  
                  return $conexao->insert_id;
      }

      public function AlterarFormula($formulaJogo, $conexao) {
            $sql = "
                  UPDATE 
                        formulaJogos 
                  SET 
                        rodada = '$formulaJogo->rodada', 
                        mandante = '$formulaJogo->mandante',
                        visitante = '$formulaJogo->visitante'
                  WHERE 
                        id = '".$formulaJogo->id."' ";
		
			return $conexao->query($sql);
      }

      public function ExcluirFormula($id, $conexao) {
            $sql = "DELETE FROM formulaJogos WHERE id = '" . $id . "' ";	
		return $conexao->query($sql);
      }
}
?>