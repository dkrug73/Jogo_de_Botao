<?php  
	include "conexao/dbConexao.php";
	include "utils/funcoes.php";

	$campeonatoId = null;
      
      if(isset($_COOKIE['campeonatoId'])) {
            $campeonatoId = $_COOKIE['campeonatoId'];
      }
	
	$dadosCampeonato = RetornaCampeonato($conexao, $campeonatoId);
	
	$campeonatoId = $dadosCampeonato[0];
	$classeRanking = $dadosCampeonato[1];
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Estatísticas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="assets/css/tabs.css" />		
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		<div id="page-wrapper">
			<div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
			</div>
			<div id="main">
				<div class="container">
					<div class="row main-row">
                                    <!-- PRIMEIRA COLUNA -->
						<div class="3u 12u(mobile)">
							<section>
								<?php include("paginas/goleadoresTime.php") ?> 
								 
							</section>
                                    </div>
                                    
                                    <!-- SEGUNDA COLUNA -->
						<div class="6u 12u(mobile) important(mobile)">
							<section >
								<?php include("paginas/goleadoresCampeonato.php") ?>     
							</section>
                                    </div>
                                    
                                    <!-- TERCEIRA COLUNA -->
						<div class="3u 12u(mobile)">
							<section>
								<?php include("paginas/rankingGeral.php") ?> 
							</section>
						</div>
					</div>
				</div>
			</div>

			<div id="footer-wrapper">
				<?php include("componentes/rodape.php") ?>
			</div>
		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>