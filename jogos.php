
<!DOCTYPE HTML>
<html>
	<head>
		<title>Jogos</title>
		<link rel="icon" type="image/png" href="imagens/favicon.png">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/tabs.css" />	
		<link rel="stylesheet" href="assets/css/jogo.css" />	
		<script type="text/javascript" src="assets/js/tabela.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<script type="text/javascript" src="assets/js/jogo.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
				
	</head>

	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('#salvarJogo').click(function() {	
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'paginas/jogo1.php',
					async: true,
					data: {
						jogo: jogoId,
						mandante: mandanteId,
						visitante: visitanteId,
						campeonato: idCampeonato,
						goleadoresMandante: goleadoresMandante,
						goleadoresVisitante: goleadoresVisitante,
						placarMandante: document.getElementById('placarMandante').value, 
						placarVisitante: document.getElementById('placarVisitante').value,
					},       
					success: function(response) {
						recebeTabela();
						FecharModal();
						window.location.hash = 'close';
					}
				});

				return false;
			});
		});
		
	</script>

	<body>
		<div id="page-wrapper">
			<div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
			</div>
			<div id="main">
				<div class="container">
					<div class="row main-row">
						<div class="8u 12u(mobile)">
							<section class="left-content">
								<div name="recebeTabela" id="recebeTabela"></div>
							</section>

						</div>
						<div class="4u 12u(mobile)">
							<section>
								<div name="recebeClassificacao" id="recebeClassificacao"></div>
							</section>

							

						</div>
					</div>
				</div>
			</div>			

			<div id="footer-wrapper">
                        <?php include("componentes/rodape.php") ?>
				
			</div>
		</div>


		<div id="abrirModal" class="modalDialog">
			<div>
				<a class="close" title="Fechar" href="#close" onclick="FecharModal()">X</a>
				
				<?php include("paginas/jogo.php"); ?>				
			</div>
		</div>
		
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/skel-viewport.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>

	</body>
</html>