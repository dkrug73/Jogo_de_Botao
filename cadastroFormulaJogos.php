<?php
      SESSION_START();
	include "conexao/dbConexao.php";
      include "utils/funcoes.php";  

      $_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
      
      // inicializa valores
      $formulaId = $_GET["id"];
      $formulaNome = RetornaFormulaDescricao($conexao, $formulaId);
      $formulaJogoId = null;
      $mandante = null;
      $visitante = null;
      $rodada = null;
      $mensagem = null;
      $tipoAviso = null; 
      $foco = false; 

      if(isset($_GET['rodada'])){
            $rodada = $_GET['rodada'];
            $foco=true;
      }
      
      if (isset($_GET['tipoAviso'])) {
		$tipoAviso = $_GET['tipoAviso'];
      } 
      
	if(isset($_GET['msg'])){
		$mensagem = $_GET['msg'];
      }

      if(isset($_GET['jogoId'])){
		$jogoId = $_GET['jogoId'];
		
		if ($jogoId != "") {
			$sql = "SELECT * FROM formulaJogos WHERE id = '" . $jogoId . "' ";
			
			$rs=$conexao->query($sql);
			$reg=mysqli_fetch_array($rs);
			
			$formulaJogoId = $jogoId;
			$rodada = $reg['rodada'];
			$mandante = $reg['mandante'];
			$visitante = $reg['visitante'];
		} 		
	}	
      
      $sql="SELECT
            id as jogoId, 
            rodada, 
            mandante,
            visitante
      FROM 
            formulaJogos
      WHERE
            formulaId = '".$formulaId."'
      ORDER BY
            rodada, id";

      $rs=$conexao->query($sql);
?>      

<!DOCTYPE HTML>

<html>
	<head>
		<title>Fórmulas dos jogos</title>
            <link rel="icon" type="image/png" href="imagens/favicon.png">
            <meta name="robots" content="noindex">
		<meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="assets/css/tabs.css" />
            <link rel="stylesheet" href="assets/css/modal.css" />

            <!-- Janela Modal -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

            <script type="text/JavaScript"> 
                  function Nova() { location.href="cadastroFormulaJogos.php?id=<?PHP print $formulaId; ?>&nome=<?php print $formulaNome; ?>" } ;  	  
            </script>

	</head>

	<body>
		<div id="page-wrapper">
                  <div id="header-wrapper">
                        <?php include ("componentes/menu.php")?>	
                  </div>
                  
                  <div id="main">
				<div class="container">                       
                        
                              <?php include ("componentes/mensagem.php") ?>
                              
                              <div class="row main-row">
                                    <div class="12u">

                                          <section class="content-header">
                                                <h2 class="tituloPagina">Cadastrar ou editar fórmulas dos jogos</h2>				
                                          </section>

                                          <section class="cadastro"> 
                                                <form class="contact_form" method="post" action="paginas/cadastroFormulaJogos1.php" enctype="multipart/form-data">

                                                      <div class="row2">  <!-- Formula ID -->
                                                            <div class="col-25">
                                                                  <label for="fname">Formula</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" class="desabilitado" id="formula" name="formula" readonly value="<?php print $formulaNome; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2">  <!-- ID -->
                                                            <div class="col-25">
                                                                  <label for="fname">ID</label>
                                                            </div>
                                                            <div class="col-75">
                                                                  <input type="text" class="desabilitado" id="formulaJogoId" name="formulaJogoId" readonly value="<?php print $formulaJogoId; ?>">
                                                            </div>
                                                      </div>

                                                      <div class="row2"> <!-- Rodada -->
                                                            <div class="col-25">
                                                                  <label for="rodada">Rodada</label>
                                                            </div>
                                                            <div class="col-in25">
                                                                  <?php
                                                                  if ($foco) { ?>
                                                                        <input type="text" id="rodada" name="rodada" name="rodada" required pattern="[0-9]+$" 
                                                                        title="somente números" value="<?php print $rodada; ?>" autofocus maxlength="2"> <?php 
                                                                  } else { ?>
                                                                        <input type="text" id="rodada" name="rodada" name="rodada" required pattern="[0-9]+$" 
                                                                        title="somente números" value="<?php print $rodada; ?>" maxlength="2"> <?php 
                                                                  } ?>
                                                            </div>
                                                      </div>

                                                      <div class="row2">  <!-- Confronto -->
                                                            <div class="col-25">
                                                                  <label for="fname">Confronto</label>
                                                            </div>

                                                            <div class="col-75">

                                                                  <div class="col-in12">
                                                                        
                                                                              <input type="text" id="mandante" name="mandante" name="mandante" required pattern="[0-9]+$" 
                                                                              title="somente números" value="<?php print $mandante; ?>" maxlength="2">
                                                                       
                                                                  </div>

                                                                  <div class="col-in5" style="margin-top: -5px;"> x  </div>

                                                                  <div class="col-in12">
                                                                        
                                                                              <input type="text" id="visitante" name="visitante" name="visitante" required pattern="[0-9]+$" 
                                                                              title="somente números" value="<?php print $visitante; ?>" maxlength="2">                                                                       
                                                                  </div>
                                                            </div>
                                                      </div>

                                                      <input type="text" name="formulaId" style="display:none;" value="<?php print $formulaId ?>"> 

                                                      <div class="row2"> <!-- Botões -->
                                                            <div class="col-25">
                                                                  <label for="data"></label>
                                                            </div>
                                                            <div class="col-75">                                                                
                                                                  <div class="botoes">
                                                                        <button type="submit" id="submit" class="btnSalvar" name="acao" value="inc">Salvar</button>

                                                                        <button type="submit" class="btnExcluir" name="acao" value="exc">Excluir</button>

                                                                        <button type="button" class="btnCancelar" onClick="Nova()" >Cancelar</button>
                                                                  </div>
                                                            </div>
                                                      </div>                          
                                                      
				                        </form>   
                                          </section>

                                          <div class = "box tabela">                            
                                                <div class="box box-solid box-success">   
                                                      <div class="box-body">
                                                            <div class="box-body no-padding">
                                                                  <table class="table tabela1" >
                                                                        <thead class="thead-default" >
                                                                              <tr class="cabecalhoTabela fundoCadastro">
                                                                                    <th class="linhaCabecalho">ID</th>
                                                                                    <th class="linhaCabecalho">Rodada</th>
                                                                                    <th class="linhaCabecalho">Jogo</th>
                                                                              </tr> 
                                                                        </thead> 

                                                                        <?PHP
                                                                        if (isset($rs)) {
                                                                              while($reg=mysqli_fetch_array($rs)) { 
                                                                                    $id = $reg["jogoId"];
                                                                                    $rodada = $reg["rodada"]. "ª rodada";
                                                                                    $jogo = $reg["mandante"]." x ".$reg["visitante"];?>														
                                                                                                                  
                                                                                    <tr onclick="location.href='cadastroFormulaJogos.php?id=<?php print $formulaId; ?>&jogoId=<?PHP print $id; ?>'; "
                                                                                    style='cursor: pointer;'>
                                                                                          
                                                                                          <td class="linha"><?PHP print $id; ?></td>
                                                                                          <td class="linha"><?PHP print $rodada; ?></td>
                                                                                          <td class="linha"><?PHP print $jogo; ?></td>
                                                                                    </tr>	 <?PHP 
                                                                              } 
                                                                        } else { ?>                                         
                                                                              <tr>       
                                                                                    <td class="linha">&nbsp;</td>                  
                                                                                    <td class="linha">&nbsp;</td>
                                                                                    <td class="linha">&nbsp;</td>
                                                                              </tr>	<?php
                                                                        } ?>                        
                                                                  </table>
                                                            </div>
                                                      </div> 
                                                </div>   
                                          </div>  

                                    </div>
					</div>
				</div>
                  </div>     
            </div>
            
            <div id="footer-wrapper">
                  <?php include("componentes/rodape.php") ?>
            </div>
            
            
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/skel-viewport.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>     
          
	</body>
</html>