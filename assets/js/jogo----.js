







function clickListener(e){
	//window.location.hash = 'abrirModal';

	




	FecharModal();
	jogoId = e.getAttribute("data-id"); 	
	var placar = null;
	turno = null;

	

	$.ajax({
		type:'post',
		data: {jogoId: jogoId},	
		dataType: 'json',
		url: 'utils/validarJogo.php',
		success: function(placarJogo){                    
			placar = placarJogo[0].placarMandante; 
			
			if (placar != null) {
				var confirmar = confirm("Este jogo está encerrado.\nDeseja apagar o resultado e abrir novamente?");
	
				if (confirmar)
				{					
					$.ajax({
						type:'post',
						data: {jogoId: jogoId},	
						dataType: 'json',
						url: 'utils/zerarPlacarJogo.php',
						success: function(abrir){
							recebeValor();							
							$('.modal').modal('show'); 
							$('#myModal0').modal('hide');
						}
					}); 					
				}
				else {
					$('.modal').modal('hide'); 
				}
			}
			else {				
				//$('.modal').modal('show');
				$('#myModal6').modal('show');
				$('#myModal0').modal('hide');				
			}
		}
	}); 
	
	//carrega os dados dos jogos
	$.ajax({
		type:'post',
		data: {jogoId: jogoId},	
		dataType: 'json',
		url: 'paginas/carregaDadosJogo.php',
		success: function(dados){                    
			document.getElementById("nomeMandante").innerHTML =dados[0].mandante;
			document.getElementById("nomeVisitante").innerHTML =dados[0].visitante; 
			document.getElementById("mandanteFoto").src="../imagens/times/" + dados[0].mandanteId + ".png";  
			document.getElementById("visitanteFoto").src="../imagens/times/" + dados[0].visitanteId + ".png"; 			

			mandanteId = dados[0].mandanteId;
			visitanteId = dados[0].visitanteId;  
			mandanteNome = dados[0].mandante;
			visitanteNome = dados[0].visitante;
			turno = dados[0].turno;    

			document.getElementById("nomeMandantePenaltis").innerHTML = mandanteNome;
			document.getElementById("nomeVisitantePenaltis").innerHTML = visitanteNome; 
			document.getElementById("mandanteFotoPenaltis").src="../imagens/times/" + mandanteId + ".png";  
			document.getElementById("visitanteFotoPenaltis").src="../imagens/times/" + visitanteId + ".png";                    
		}
	});                 

	// carrega os dados do jogador mandante
	$.ajax({
		type:'post',
		data: {jogoId: jogoId, time: 'mandante'},	
		dataType: 'json',	
		url: 'paginas/dadosJogador.php',
		success: function(dados2){
			for(var i=0;dados2.length>i;i++){
			document.getElementById("jogadorMandanteId"+(i+1)).innerHTML=dados2[i].id;
			document.getElementById("jogadorMandanteId"+(i+1)).style.visibility = "hidden";
			document.getElementById("jogadorMandante"+(i+1)).innerHTML=dados2[i].jogador;
			} 
		}
	});

	// carrega os dados do jogador visitante	
	$.ajax({
		type:'post',
		data: {jogoId: jogoId, time: 'visitante'},	
		dataType: 'json',	
		url: 'paginas/dadosJogador.php',
		success: function(dados3){
			for(var i=0;dados3.length>i;i++){
			document.getElementById("jogadorVisitanteId"+(i+1)).innerHTML=dados3[i].id;
			document.getElementById("jogadorVisitanteId"+(i+1)).style.visibility = "hidden";
			document.getElementById("jogadorVisitante"+(i+1)).innerHTML=dados3[i].jogador;
			} 
		}
	});
} 

$(document).ready(function() {
	$('#salvarJogo').click(function() {
		var datateste = {
			jogo: jogoId, 
			mandante: mandanteId,
			visitante: visitanteId,
			campeonato: idCampeonato,
			goleadoresMandante: goleadoresMandante,
			goleadoresVisitante: goleadoresVisitante,
			placarMandante: document.getElementById('placarMandante').value, 
			placarVisitante: document.getElementById('placarVisitante').value
		};

		console.dir(datateste);		

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'paginas/jogo1.php',
			async: true,
			data: {
				jogo: jogoId, 
				mandante: mandanteId,
				visitante: visitanteId,
				campeonato: idCampeonato,
				goleadoresMandante: goleadoresMandante,
				goleadoresVisitante: goleadoresVisitante,
				placarMandante: document.getElementById('placarMandante').value, 
				placarVisitante: document.getElementById('placarVisitante').value
			},                
			success: (response) => {
				// recarregar tabela jogos
				recebeValor();

				var placarMandante = document.getElementById('placarMandante').value;
				var placarVisitante = document.getElementById('placarVisitante').value ;
				
				if (turno == 3 || turno == 4) {
					if (placarMandante == placarVisitante) {
						$('.modal').modal('show');
					}
				}  
								
				$('#myModal6').modal('hide');
				$('#myModal0').modal('hide');	
			}
		});

		return false;
	});	

	$('#salvarJogoPenalti').click(function() {
		var penaltiMandante = document.getElementById('mandante').value; 
		var penaltiVisitante = document.getElementById('visitante').value;

		if (penaltiMandante == penaltiVisitante) {
			alert("A decisão por pênaltis deverá ter um time vencedor.");
		}
		else {			
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'paginas/jogo2.php',
				async: true,
				data: {
					jogo: jogoId, 
					campeonato: idCampeonato,
					penaltiMandante: penaltiMandante, 
					penaltiVisitante: penaltiVisitante
				},                
				success: (response) => {
					recebeValor();
					$('.modal').modal('hide'); 				
					$('.myModal6').modal('hide'); 
				}
			});
		}

		return false;
	});	
});

