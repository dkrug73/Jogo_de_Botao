function clickListener(e){
      jogoId = e.getAttribute("data-id"); 
      mandanteId = 0;
      visitanteId = 0;
      mandanteNome = "";
      visitanteNome = "";
      idCampeonato = 0;
      goleadoresMandante = [];
      goleadoresVisitante = [];
      abrirJogo = false;  
  
      $.ajax({
		type:'post',
		data: {jogoId: jogoId},	
		dataType: 'json',
		url: 'utils/validarJogo.php',
		success: function(placarJogo){                    
			placar = placarJogo[0].placarMandante; 
			
			if (placar != null) {
				var confirmar = confirm("Este jogo está encerrado.\nDeseja apagar o resultado e abrir novamente?");
	
				if (confirmar)
				{					
					$.ajax({
						type:'post',
						data: {jogoId: jogoId},	
						dataType: 'json',
						url: 'utils/zerarPlacarJogo.php',
						success: function(abrir){
                                          recebeTabela();						
							window.location.hash = 'abrirModal';
						}
					}); 					
				}
			}
			else {				
				window.location.hash = 'abrirModal';			
			}
		}
	}); 

      //window.location.hash = 'abrirModal';

      

      // Carrega os dados do cabeçalho do jogo --> Nome dos times + placar
      $.ajax({
            type:'post',
            data: {jogoId: jogoId},	
            dataType: 'json',
            url: 'paginas/carregaDadosJogo.php',
            success: function(dados){                    
                  document.getElementById("nomeMandante").innerHTML =dados[0].mandante;
                  document.getElementById("nomeVisitante").innerHTML =dados[0].visitante; 
                  document.getElementById("mandanteFoto").src="../imagens/times/" + dados[0].mandanteId + ".png";  
                  document.getElementById("visitanteFoto").src="../imagens/times/" + dados[0].visitanteId + ".png";                  			

                  mandanteId = dados[0].mandanteId;
                  visitanteId = dados[0].visitanteId;  
                  mandanteNome = dados[0].mandante;
                  visitanteNome = dados[0].visitante;
                  turno = dados[0].turno;  
                  idCampeonato = dados[0].campeonatoId;        
            }
      });  
      
      // Carrega os dados dos jogadores mandantes
      $.ajax({
		type:'post',
		data: {jogoId: jogoId, time: 'mandante'},	
		dataType: 'json',	
		url: 'paginas/dadosJogador.php',
		success: function(dados2){
			for(var i=0;dados2.length>i;i++){
			document.getElementById("jogadorMandanteId"+(i+1)).innerHTML=dados2[i].id;
			document.getElementById("jogadorMandanteId"+(i+1)).style.visibility = "hidden";
			document.getElementById("jogadorMandante"+(i+1)).innerHTML=dados2[i].jogador;
			} 
		}
	});

      // Carrega os dados dos jogadores visitantes
	$.ajax({
		type:'post',
		data: {jogoId: jogoId, time: 'visitante'},	
		dataType: 'json',	
		url: 'paginas/dadosJogador.php',
		success: function(dados3){
			for(var i=0;dados3.length>i;i++){
			document.getElementById("jogadorVisitanteId"+(i+1)).innerHTML=dados3[i].id;
			document.getElementById("jogadorVisitanteId"+(i+1)).style.visibility = "hidden";
			document.getElementById("jogadorVisitante"+(i+1)).innerHTML=dados3[i].jogador;
			} 
		}
      });
      
}
  
// método para o clique na tabela de jogos
function clique(e){   
      var componenteId = getTarget(e).id; 

      if(componenteId > 100 & componenteId < 200){
            var componenteJogador = 'jogadorMandante' + (parseInt(componenteId) + 400);  
            
            document.getElementById(componenteJogador).value =  parseInt(document.getElementById(componenteJogador).value)+1;
            document.getElementById('placarMandante').value =  parseInt(document.getElementById('placarMandante').value)+1;

            var indiceMandante = componenteId - 100;
            var nomeMandante = 'jogadorMandante' + indiceMandante;
            var idMandante = 'jogadorMandanteId' + indiceMandante;

            var id = document.getElementById(idMandante).innerHTML;  
            var naoAchei = true;

            goleadoresMandante.forEach(function(item) {   
                  if (item.id == id) {
                        item.gols += 1;
                        naoAchei = false;
                  }            
            });

            if (naoAchei) {
                  var item = {};
                  item.id = document.getElementById(idMandante).innerHTML;
                  item.nome = document.getElementById(nomeMandante).innerHTML;
                  item.gols = 1;
                  
                  goleadoresMandante.push(item);
            }                
      }

      else if(componenteId > 200 & componenteId < 300){
            var componenteJogador = 'jogadorMandante' + (parseInt(componenteId) + 300); 

            if (document.getElementById(componenteJogador).value > 0){
                  document.getElementById(componenteJogador).value = parseInt(document.getElementById(componenteJogador).value )-1;
                  document.getElementById('placarMandante').value =  parseInt(document.getElementById('placarMandante').value)-1;

                  var indiceId = componenteId - 200;
                  var idMandante = 'jogadorMandanteId' + indiceId;                    
                  var id = document.getElementById(idMandante).innerHTML; 
                  
                  goleadoresMandante.forEach(function(item) {   
                        if (item.id == id) {
                              if (item.gols > 1) {
                                    item.gols -= 1;
                              } 
                              else {
                                    var idx = goleadoresMandante.findIndex(x => x.id === id);
                                    goleadoresMandante.splice(idx, 1); 
                              }
                        }            
                  });                                      
            }
      }

      else if(componenteId > 300 & componenteId < 400){
            var componenteJogador = 'jogadorVisitante' + (parseInt(componenteId) + 300);

            document.getElementById(componenteJogador).value =  parseInt(document.getElementById(componenteJogador).value)+1;
            document.getElementById('placarVisitante').value =  parseInt(document.getElementById('placarVisitante').value)+1;

            var indiceVisitante = componenteId - 300;
            var nomeVisitante = 'jogadorVisitante' + indiceVisitante;
            var idVisitante = 'jogadorVisitanteId' + indiceVisitante;

            var id = document.getElementById(idVisitante).innerHTML;  
            var naoAchei = true;
            
            goleadoresVisitante.forEach(function(item) {   
                  if (item.id == id) {
                        item.gols += 1;
                        naoAchei = false;
                  }            
            });

            if (naoAchei) {
                  var item = {};
                  item.id = document.getElementById(idVisitante).innerHTML;
                  item.nome = document.getElementById(nomeVisitante).innerHTML;
                  item.gols = 1;
                  
                  goleadoresVisitante.push(item);
            }
      }

      else if(componenteId > 400 & componenteId < 500){
            var componenteJogador = 'jogadorVisitante' + (parseInt(componenteId) + 200);

            if (document.getElementById(componenteJogador).value > 0){
                  document.getElementById(componenteJogador).value = parseInt(document.getElementById(componenteJogador).value )-1;
                  document.getElementById('placarVisitante').value =  parseInt(document.getElementById('placarVisitante').value)-1;

                  var indiceId = componenteId - 400;
                  var idVisitante = 'jogadorVisitanteId' + indiceId;
                  var id = document.getElementById(idVisitante).innerHTML; 
                  
                  goleadoresVisitante.forEach(function(item) {   
                        if (item.id == id) {
                              if (item.gols > 1) {
                                    item.gols -= 1;
                              } 
                              else {
                                    var idx = goleadoresVisitante.findIndex(x => x.id === id);
                                    goleadoresVisitante.splice(idx, 1); 
                              }
                        }            
                  });      
            }
      }             

      var i, j;
      var jogadorMandante = "";
      
      for (i = 0; i < goleadoresMandante.length; i++) {
            var gol = ' gol';
            if (goleadoresMandante[i].gols > 1) gol = ' gols';
                  jogadorMandante += goleadoresMandante[i].nome + " - " + goleadoresMandante[i].gols + gol + "<br>";
      }

      var jogadorVisitante = "";
      
      for (j = 0; j < goleadoresVisitante.length; j++) {
            var gol = ' gol';
            if (goleadoresVisitante[j].gols > 1) gol = ' gols';
                  jogadorVisitante += goleadoresVisitante[j].nome + " - " + goleadoresVisitante[j].gols + gol + "<br>";
      }
                  
      document.getElementById("goleadorMandante").innerHTML = jogadorMandante;
      document.getElementById("goleadorVisitante").innerHTML = jogadorVisitante;         
}  

// método ao fechar a modal
function FecharModal() { 
      document.getElementById("placarMandante").value = '0';
      document.getElementById("placarVisitante").value = '0';
      document.getElementById("goleadorMandante").innerHTML = '';
      document.getElementById("goleadorVisitante").innerHTML = '';

      jogoId = 0;
      mandanteId = 0;
      visitanteId = 0;

      goleadoresMandante = goleadoresMandante.slice(0, 0);
      goleadoresVisitante = goleadoresVisitante.slice(0, 0);

      $('#dadosJogo').each (function(){this.reset(); });
} ;

function SalvarJogo() {
      $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'paginas/jogo1.php',
            async: true,
            data: {
                  jogo: jogoId, 
                  mandante: mandanteId,
                  visitante: visitanteId,
                  campeonato: idCampeonato,
                  goleadoresMandante: goleadoresMandante,
                  goleadoresVisitante: goleadoresVisitante,
                  placarMandante: document.getElementById('placarMandante').value, 
                  placarVisitante: document.getElementById('placarVisitante').value
            },             
                           
            success: (response) => {
                  recebeValor();

                  var placarMandante = document.getElementById('placarMandante').value;
                  var placarVisitante = document.getElementById('placarVisitante').value ;
                  
                  if (turno == 3 || turno == 4) {
                        if (placarMandante == placarVisitante) {
                              $('.modal').modal('show');
                        }
                  }  
                                          
                  window.location.hash = 'close';
            }
      });

      return false;


      //window.location.hash = 'close';
}

function id( el ){
      return document.getElementById( el );
}       
      
function getTarget(e){             
      if(e.srcElement) return e.srcElement;  
      else return e.target;  
} 

