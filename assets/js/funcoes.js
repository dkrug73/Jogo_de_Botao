function AtualizarCampeonato() {
      $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'paginas/jogo1.php',
            async: true,
            data: {
                  campeonato: idCampeonato,
                  goleadoresMandante: goleadoresMandante,
                  goleadoresVisitante: goleadoresVisitante,
                  placarMandante: document.getElementById('placarMandante').value, 
                  placarVisitante: document.getElementById('placarVisitante').value
            },             
                           
            success: (response) => {
                  recebeValor();

                  var placarMandante = document.getElementById('placarMandante').value;
                  var placarVisitante = document.getElementById('placarVisitante').value ;
                  
                  if (turno == 3 || turno == 4) {
                        if (placarMandante == placarVisitante) {
                              $('.modal').modal('show');
                        }
                  }  
                                          
                  window.location.hash = 'close';
            }
      });

      return false;


      //window.location.hash = 'close';
}

function Nova(campo) { 
      location.href=campo 
} ; 

function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(name) {
      var cookies = document.cookie;
      var prefix = name + "=";
      var begin = cookies.indexOf("; " + prefix);
   
      if (begin == -1) {
   
          begin = cookies.indexOf(prefix);
           
          if (begin != 0) {
              return null;
          }
   
      } else {
          begin += 2;
      }
   
      var end = cookies.indexOf(";", begin);
       
      if (end == -1) {
          end = cookies.length;                        
      }
   
      return unescape(cookies.substring(begin + prefix.length, end));
  }

  function deleteCookie(name) {
      if (getCookie(name)) {
             document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
      }
}