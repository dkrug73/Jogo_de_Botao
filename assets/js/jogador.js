// classe jogador
function Jogador() {
	var id;
	var campeonatoId;
	var timeId;
	var nome;
	var numero;
	var titular;
	var foto;
	var gols;
	
	this.getId=function(){
		return this.id;
    	};

	this.getCampeonatoId=function(){
		return this.campeonatoId;
    	};

	this.getTimeId=function(){
		return this.timeId;
	};
 
	this.getNome=function(){
		return this.nome;
	};
	
	this.getNumero=function() {
		return this.numero;
	};
	
	this.getTitular=function() {
		return this.titular;
	};

	this.getFoto=function() {
		return this.titular;
	};

	this.getGols=function() {
		return this.gols;
	};

	this.setId=function(_id) {
		this.id = _id;
	};
	
	this.setCampeonatoId=function(_campeonatoId) {
		this.campeonatoId = _campeonatoId;
	};
	
	this.setTimeId=function(_timeId) {
		this.timeId = _timeId;
	};

	this.setNome=function(_nome) {
		this.nome = _nome;
	};
	
	this.setNumero=function(_numero) {
		this.numero = _numero;
	};
	
	this.setTitular=function(_titular) {
		this.titular = _titular;
	};

	this.setFoto=function(_foto) {
		this.foto = _foto;
	};
	
	this.setGols=function(_gols) {
		this.gols = _gols;
	};


	



}




//Classe Pessoa
function Pessoa() {
 
    //@interface
    var nome;
    var idade;
    var email;
    var carro;
    //@end
 
    //@constructe
    this.carro = new Carro();
    this.carro.setPlaca(7520);
    this.carro.setCor("Amarelo");
    //@end
 
    //@implementation
    this.getCarro=function(){
        return this.carro;
    };
 
    this.getNome=function(){
      return this.nome;
    };
 
    this.getIdade=function() {
      return this.idade;
    };
 
    this.getEmail=function() {
      return this.email;
    };
 
    this.setNome=function(_nome) {
      this.nome = _nome;
    };
 
    this.setIdade=function(_idade) {
      this.idade = _idade;
    };
 
    this.setEmail=function(_email) {
      this.email = _email;
    };
 
    this.mostraValores=function() {
      return 'Nome: '+this.nome+
             '<br/>Idade: '+this.idade+
             ' anos<br/>Email: '+this.email+
             '<br/>Carro:<br/>* Placa:'+this.carro.getPlaca()+
             '<br/>**Cor: '+this.carro.getCor();
    };
 
    //Recebe uma função por parâmetro e executa
    this.executa=function(i,funcao){
          if(i > 0)
          funcao();
    };
 
    //Recebe dados no formato JSON via parâmetro
    this.mostraJson=function(dados){
          return 'Id:'+dados.id_conteudo+'<br/>Nome: '+dados.nome;
    };
    //@end
}