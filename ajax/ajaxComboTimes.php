<?php
    SESSION_START();
    include "../conexao/dbConexao.php";
    include "../utils/funcoes.php";

    $campeonatoId = $_POST['campeonatoId']; 
    $timeId = $_POST['timeId'];

    $sql = "SELECT DISTINCT 
                times.id, 
                times.nome AS nome, 
                botonistas.nome AS botonistaNome 
            FROM 
                times INNER JOIN 
                botonistas ON times.botonistaId = botonistas.id INNER JOIN 
                timescampeonato ON timescampeonato.timeId = times.id
            WHERE 
                timescampeonato.campeonatoId = '".$campeonatoId."'
            ORDER BY 
                botonistas.nome, 
                times.nome ";

    $rs =$conexao->query($sql);


    $opcao = "<option value='0'>== SELECIONE UM TIME ==</option>";
    while($time=mysqli_fetch_array($rs))		
    {	
        $timeNome = $time['nome']." - ".$time['botonistaNome'];

        if ($timeId == $time['id']) {
            $opcao = $opcao . "<option value='".$time['id']."' selected='selected'>".$timeNome."</option><br /> ";
        }
        else {
            $opcao = $opcao . "<option value='".$time['id']."'>".$timeNome."</option><br /> ";
        }
    }

    echo $opcao;
?>