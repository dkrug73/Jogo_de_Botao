<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";

      $campeonatoId = null;
      
      if(isset($_COOKIE['campeonatoId'])) {
            $campeonatoId = $_COOKIE['campeonatoId'];
      }
      
      $classeRaning = RetornaClasseRanking($conexao, $campeonatoId);

      $sql = "SELECT 
                  jogojogador.id,
                  jogador.numero,
                  jogador.nome AS jogadorNome,
                  SUM(jogojogador.gols) AS gols,
                  times.nome AS timeNome
              FROM 
                  jogojogador INNER JOIN 
                  jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                  times ON jogador.timeId = times.id
              WHERE
                  jogador.campeonatoId = '".$campeonatoId."'
              GROUP BY
                  jogojogador.jogadorId
              ORDER BY
                  gols DESC,
                  jogadorNome ASC,
                  timeNome ASC ";

      $rs=$conexao->query($sql);

      $sqlTime = "SELECT 
                        times.id AS timeId,
                        jogador.numero,
                        jogador.nome AS jogadorNome,
                        SUM(jogojogador.gols) AS gols,
                        times.nome AS timeNome
                  FROM 
                        jogojogador INNER JOIN 
                        jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                        times ON jogador.timeId = times.id
                  WHERE
                        jogador.campeonatoId = '".$campeonatoId."'
                  GROUP BY
                        jogojogador.jogadorId
                  ORDER BY
                        timeNome ASC,
                        gols DESC,
                        jogadorNome ASC ";

      $rsTime=$conexao->query($sqlTime); 

      $sqlRanking = "SELECT
                        timeNome AS timeNome,
                        timeId,
                        SUM(pontoGanho) AS pontos
                     FROM 
                        tabela INNER JOIN campeonatos ON tabela.campeonatoId = campeonatos.id 
                     WHERE
                        tabela.turnoGrupo <> 'G' AND
                        campeonatos.classeRanking = '".$classeRaning."'
                     GROUP BY	
                        timeId
                     ORDER BY
                        pontos DESC,
                        timeNome ASC";

      $rsRanking=$conexao->query($sqlRanking); 

      $sqlGoleadores = "SELECT 
                  jogojogador.id,
                  jogador.numero,
                  jogador.nome AS jogadorNome,
                  SUM(jogojogador.gols) AS gols,
                  times.nome AS timeNome
              FROM 
                  jogojogador INNER JOIN 
                  jogador ON jogojogador.jogadorId = jogador.id INNER JOIN 
                  times ON jogador.timeId = times.id INNER JOIN
                  campeonatos ON campeonatos.id = jogador.campeonatoId
              WHERE
                  campeonatos.classeRanking = '".$classeRaning."'
              GROUP BY
                  jogojogador.jogadorId
              ORDER BY
                  gols DESC,
                  jogadorNome ASC,
                  timeNome ASC ";

      $rsGoleadores=$conexao->query($sqlGoleadores);

?>

<div class="row">   
      <!--TABELA GOLEADORES GERAL  -->
      <div class="col-md-6" style="width: 35%; min-height: 320px;" >
            <div class="box" style="width: 600px;">
                  <div class="box-header with-border" style="text-align: center;">
                        <h3 class="box-title"><strong>GOLEADORES GERAL</strong></h3>
                  </div>
                  
                  <div class="box-body">
                        <table class="table table-striped" name = "primeiroTurno" id = "primeiroTurno" style="font-size: 18px;">
                              <thead>
                                    <th style="width: 100px;text-align: center;">Número</th>
                                    <th style="width: 150px;">Nome</th>
                                    <th style="width: 50px;text-align: center;">Gols</th>
                                    <th style="width: 150px;">Time</th>
                              </thead>
                              <?PHP
                              if (isset($rs)) {
                                    while($reg=mysqli_fetch_array($rs)) {
                                          $id = $reg["id"];
                                          $numero = $reg["numero"];
                                          $jogadorNome = $reg["jogadorNome"];
                                          $timeNome = $reg["timeNome"];
                                          $gols = $reg["gols"];?>                          
                                                                        
                                          <tr>                                     
                                                <td style="text-align: center;"><?PHP print $numero; ?></td>
                                                <td><?PHP print $jogadorNome; ?></td>
                                                <td style="text-align: center;"><?PHP print $gols; ?></td>
                                                <td><?PHP print $timeNome; ?></td>                                                      
                                          </tr>							
                                    <?PHP 
                                    } 
                              }  ?>
                        </table>
                  </div>
            </div>
      </div>

      <!--TABELA GOLEADORES POR TIME -->
      <div class="col-md-6" style="width: 35%; min-height: 320px;">
            <div class="box" style="width: 500px;">
                  <div class="box-header with-border" style="text-align: center;">
                        <h3 class="box-title"><strong>GOLEADORES POR TIME</strong></h3>
                  </div>

                  <div class="box-body">
                        <table class="table table-striped" name = "primeiroTurno" id = "primeiroTurno" style="font-size: 18px;">
                              <thead>
                                    <th style="width: 100px;text-align: center;">Número</th>
                                    <th >Nome</th>
                                    <th style="text-align: center;">Gols</th>
                              </thead>

                              <?PHP
                              $timeAnterior = 0;
                              if (isset($rsTime)) {
                                    while($regTime=mysqli_fetch_array($rsTime)) {
                                          $timeId = $regTime["timeId"];
                                          $numeroTime = $regTime["numero"];
                                          $jogadorNomeTime = $regTime["jogadorNome"];
                                          $timeNomeTime = $regTime["timeNome"];
                                          $golsTime = $regTime["gols"];   

                                          if ($timeAnterior != $timeId) { ?>                  
                                          <tr>
                                                <td></td>
                                                <td style="text-align: center;"><strong style="color: red;"><?PHP print $timeNomeTime; ?></strong></td>
                                                <td></td>  
                                          </tr>  <?php                              
                                    } 
                        
                                    $timeAnterior = $timeId;?>                           
                                                                        
                                          <tr>                                     
                                                <td style="text-align: center;"><?PHP print $numeroTime; ?></td>
                                                <td><?PHP print $jogadorNomeTime; ?></td>
                                                <td style="text-align: center;"><?PHP print $golsTime; ?></td>                             
                                          </tr>							
                                    <?PHP 
                                    } 
                              }  ?>
                        </table>                       
                  </div>
            </div>
      </div>
<div class="row">      
      <div class="col-md-6" style="width:29.5%; margin: 0 0px 0 -100px;" >            
            <!--RANKING GERAL -->
            <div class="box" style="width: 600px;">
                  <div class="box-header" style="text-align: center;">
                        <h3 class="box-title"><strong>RANKING</strong></h3>                        
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                        <table class="table" style="font-size: 18px;">
                              <tr>
                                    <th style="text-align: center;">p</th>
                                    <th>Time</th>
                                    <th>Técnico</th>
                                    <th style="text-align: center;">Pontos</th>
                              </tr> <?php

                              if (isset($rsRanking)) {
                                    $posicao = 1;
                                    while($regRanking = mysqli_fetch_array($rsRanking)) {
                                          $timeNome = $regRanking["timeNome"];
                                          $timeId = $regRanking["timeId"];
                                          $pontos = $regRanking["pontos"];
                                          
                                          $botonista = RetornaApelidoBotonista($conexao, $timeId); ?>                          
                                                                        
                                          <tr>       
                                                <td style="text-align: center;"><?PHP print $posicao; ?></td>                  
                                                <td><?PHP print $timeNome; ?></td>
                                                <td><?PHP print $botonista; ?></td>
                                                <td style="text-align: center;"><?PHP print $pontos; ?></td>
                                          </tr>	  <?PHP 

                                          $posicao++;
                                    } 
                              }  ?>
                        </table>
                  </div>
            </div>
            
            <!--RANKING GERAL -->
            <div class="box" style="width: 600px;">
                  <div class="box-header with-border" style="text-align: center;">
                        <h3 class="box-title"><strong>GOLEADORES DO RANKING</strong></h3>
                  </div>
                  
                  <div class="box-body">
                        <table class="table table-striped" name = "primeiroTurno" id = "primeiroTurno" style="font-size: 18px;">
                              <thead>
                                    <th style="width: 100px;text-align: center;">Número</th>
                                    <th style="width: 150px;">Nome</th>
                                    <th style="width: 50px;text-align: center;">Gols</th>
                                    <th style="width: 150px;">Time</th>
                              </thead>
                              <?PHP
                              if (isset($rsGoleadores)) {
                                    while($reg=mysqli_fetch_array($rsGoleadores)) {
                                          $id = $reg["id"];
                                          $numero = $reg["numero"];
                                          $jogadorNome = $reg["jogadorNome"];
                                          $timeNome = $reg["timeNome"];
                                          $gols = $reg["gols"];?>                          
                                                                        
                                          <tr>                                     
                                                <td style="text-align: center;"><?PHP print $numero; ?></td>
                                                <td><?PHP print $jogadorNome; ?></td>
                                                <td style="text-align: center;"><?PHP print $gols; ?></td>
                                                <td><?PHP print $timeNome; ?></td>                                                      
                                          </tr>							
                                    <?PHP 
                                    } 
                              }  ?>
                        </table>
                  </div>
            </div> 
      </div>      
</div>

