<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";      

      $campeonatoId = $_POST['campeonatoId']; 

      /*
      $campeonatoId = null;
      
            if(!isset($_COOKIE['campeonatoId'])) {
                  $campeonatoId = $_COOKIE['campeonatoId'];
            }
*/
      

      $sql = "SELECT 
                  id,
                  (SELECT nome FROM times WHERE id = mandanteId) AS mandanteNome,
                  (SELECT nome FROM times WHERE id = visitanteId) AS visitanteNome,
                  placarMandante, 
                  placarVisitante, 
                  penaltiMandante, 
                  penaltiVisitante, 
                  rodada, 
                  turno, 
                  confronto, 
                  grupo,
                  ordem
              FROM 
                  jogo 
              WHERE
                  campeonatoId = '".$campeonatoId."'
              ORDER BY
                  turno, 
                  grupo, 
                  rodada, 
                  ordem ";

      $rs=$conexao->query($sql); ?>

      <div class = "box tabela">                            
            <div class="box box-solid box-success">   
                  <div class="box-body">
                        <div class="box-body no-padding">
                              <table class="table tabela1" >
                                    <thead class="thead-default" >
                                          <tr class="cabecalhoTabela fundoCadastro">
                                                <th class="linhaCabecalho">ID</th>
                                                <th class="linhaCabecalho">Mandante</th>
                                                <th class="linhaCabecalho">Visitante</th>								
                                                <th class="linhaCabecalho">Rodada</th>
                                                <th class="linhaCabecalho">Turno</th>
                                                <th class="linhaCabecalho">Grupo</th>
                                                <th class="linhaCabecalho">Ordem</th>
                                                <th class="linhaCabecalho">Placar mandante</th>
                                                <th class="linhaCabecalho">Placar visitante</th>
                                                <th class="linhaCabecalho">Pênalti mandante</th>
                                                <th class="linhaCabecalho">Pênalti visitante</th>
                                          </tr> 
                                    </thead> 

                                    <?PHP
                                    if (isset($rs)) {
                                          while($reg=mysqli_fetch_array($rs)) { 
                                                $jogoId = $reg['id'];
                                                $mandanteNome = $reg['mandanteNome'];
                                                $visitanteNome = $reg['visitanteNome'];
                                                $placarMandante = $reg['placarMandante'];
                                                $placarVisitante = $reg['placarVisitante'];
                                                $penaltiMandante = $reg['penaltiMandante'];
                                                $penaltiVisitante = $reg['penaltiVisitante'];
                                                $rodada = $reg['rodada'];
                                                $turno = $reg['turno'];
                                                $grupo = $reg['grupo'];
                                                $rodada = $reg['rodada'];
                                                $ordem = $reg['ordem'];?>
                                                
                                                <tr onclick="location.href = 'cadastroJogos.php?id=<?PHP print $jogoId;?>&campeonato=<?PHP print $campeonatoId;?>'; " 
                                                      style='cursor: pointer;'>

                                                      <td class="linha"><?PHP print $jogoId; ?></td>
                                                      <td class="linha"><?PHP print $mandanteNome; ?></td>
                                                      <td class="linha"><?PHP print $visitanteNome; ?></td>									
                                                      <td class="linha"><?PHP print $rodada; ?></td>
                                                      <td class="linha"><?PHP print $turno; ?></td>									
                                                      <td class="linha"><?PHP print $grupo; ?></td>
                                                      <td class="linha"><?PHP print $ordem; ?></td>
                                                      <td class="linha"><?PHP print $placarMandante; ?></td>
                                                      <td class="linha"><?PHP print $placarVisitante; ?></td>
                                                      <td class="linha"><?PHP print $penaltiMandante; ?></td>
                                                      <td class="linha"><?PHP print $penaltiVisitante; ?></td>
                                                </tr>	 <?PHP 
                                          } 
                                    } else { ?>                                         
                                          <tr>       
                                                <td class="linha">&nbsp;</td>                  
                                                <td class="linha">&nbsp;</td>
                                                <td class="linha">&nbsp;</td>
                                                <td class="linha">&nbsp;</td>
                                          </tr>	<?php
                                    } ?>                        
                              </table>
                        </div>
                  </div> 
            </div>   
      </div> 