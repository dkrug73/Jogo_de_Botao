<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";

      $rs1 = null;
      $rs2 = null;
      $descricaoTabelaJogos1 = "1º TURNO";
      $descricaoTabelaJogos2 = "2º TURNO";
      
      $campeonatoId = null;
      
      if(isset($_COOKIE['campeonatoId'])) {
            $campeonatoId = $_COOKIE['campeonatoId'];
      }

      if (!empty($campeonatoId)) {
		$sqlCampeonato = "SELECT
						classeRanking,
						doisTurnos,
						doisGrupos,
						evitarConfrontos,
						finais,
						jogos,
						ativo, 
						campeao,
						vice
					FROM 
						campeonatos 
					WHERE 
                                    id = '".$campeonatoId."' ";

		$rsCampeonato=$conexao->query($sqlCampeonato);

		$regCampeonato=mysqli_fetch_array($rsCampeonato);
		
		$classeRanking = $regCampeonato['classeRanking'];
		$doisTurnos = $regCampeonato['doisTurnos'];
		$doisGrupos = $regCampeonato['doisGrupos'];
		$evitarConfrontos = $regCampeonato['evitarConfrontos'];
		$finais = $regCampeonato['finais'];
		$ativo = $regCampeonato['ativo'];
		$campeao = $regCampeonato['campeao'];
            $vice = $regCampeonato['vice'];
            
            $_SESSION["doisGrupos"] = $doisGrupos; 
            $_SESSION["doisTurnos"] = $doisTurnos; 
            
		if ($doisGrupos) {
			// carregar grid  grupo A
                  $rs1 = CarregaGridJogos($conexao, $campeonatoId, "1", "A");
                  // carregar grid grupo B
                  $rs2 = CarregaGridJogos($conexao, $campeonatoId, "2", "B");

                  $descricaoTabelaJogos1 = "GRUPO A";
                  $descricaoTabelaJogos2 = "GRUPO B";
		}
		// grupo unico
		else {
			if ($doisTurnos) {
                        
                        
				// carregar grid 1 turno
				$rs1 = CarregaGridJogos($conexao, $campeonatoId, "1", "A");
				//$rsTabela1 = CarregaGridTabela($conexao, $campeonatoId, "1");

				// carregar grid 2 turno
				$rs2 = CarregaGridJogos($conexao, $campeonatoId, "2", "A");
				//$rsTabela2 = CarregaGridTabela($conexao, $campeonatoId, "2");

				//$rsTabelaGeral = CarregaGridTabela($conexao, $campeonatoId, "G");

			}
			else {

			}

		}  
	}
      
?>

<div class="tab">
      <?php 
      if (empty($campeonatoId)) { ?>
            <button class="tablinks" onclick="openCity(event, 'semJogos')" id="defaultOpen"> Jogos</button>
      <?php
      } else { ?>
            <button class="tablinks" onclick="openCity(event, 'turno1')" id="defaultOpen"> <?PHP print $descricaoTabelaJogos1 ?></button>
            <button class="tablinks" onclick="openCity(event, 'turno2')"> <?PHP print $descricaoTabelaJogos2 ?></button>

            <?php // quatroPrimeirosGeral / doisMelhoresCadaTurno / doisMelhores / campeaoCadaTurno 
            if ($finais != "doisMelhores" AND $finais != "campeaoCadaTurno") { ?> 
                  <button class="tablinks" onclick="openCity(event, 'semi')"> SEMI-FINAL </button> <?php  
            }?>

            <?php
            if ($finais != "campeaoMelhorGeral") { ?> 
                  <button class="tablinks" onclick="openCity(event, 'final')"> FINAL </button>  <?php 
            }
      } ?>           
</div>

<!-- SEM JOGOS -->
<div id="semJogos" class="tabcontent">            
      <div class="box">	
            <div class="box-body">
                  <table class="table table-striped" name = "primeiroTurno" id = "primeiroTurno">                              
                        <tr> 
                              <td class="nomeMandante" style="padding-top: 15px;">Time Mandante</td>
                              <td class="distintivoMandante"> <img src="imagens/times/default.png" style="width: 30px;">  </td>
                              <td class="placar" style="padding-top: 15px;"> &nbsp; x &nbsp; </td>
                              <td class="distintivoVisitante"> <img src="imagens/times/default.png" style="width: 30px;">  </td>
                              <td class="nomeVisitante" style="padding-top: 15px;">Time Visitante</td>
                        </tr>
                  </table>
            </div>
      </div>
</div>

<!-- TABELA DO PRIMEIRO TURNO -->
<div id="turno1" class="tabcontent">            
      <div class="box">	
            <div class="box-body">
                  <table class="table table-striped" name = "primeiroTurno" id = "primeiroTurno">                              
                        <?PHP
                        $rodadaAnterior = 0;
                        if (isset($rs1)) {
                              while($reg=mysqli_fetch_array($rs1)) {
                                    $id = $reg["id"];
                                    $grupo = $reg["grupo"];
                                    $mandante = $reg["mandante"];
                                    $visitante = $reg["visitante"];
                                    $rodada = $reg["rodada"];
                                    $turno = $reg["turno"];
                                    $placar = " x ";
                                    $fotoMandante = $reg["fotoMandante"];
                                    $fotoVisitante = $reg["fotoVisitante"];
                                    $placarMandante = $reg["placarMandante"];
                                    $placarVisitante = $reg["placarVisitante"];
                                    $penaltiMandante = $reg["penaltiMandante"];
                                    $penaltiVisitante = $reg["penaltiVisitante"];

                                    if ($placarMandante != "" AND $placarVisitante != "") {
                                          $placar = $placarMandante." x ".$placarVisitante;
                                    }

                                    if ($rodadaAnterior != $rodada) { ?>                                                
                                          <tr>
                                                <td></td>
                                                <td></td>
                                                <td>&nbsp;</td>
                                                <td></td>
                                                <td></td>  
                                          </tr>   
                                          <?php
                                    } 
                                    
                                    $rodadaAnterior = $rodada;
                                    
                                    if ($ativo) {  ?>           
                                          <tr onclick="clickListener(this)" data-id="<?PHP print $id; ?>" style='cursor: pointer;'>  <?php 
                                    }
                                    else { ?> 
                                          <tr>  <?php 
                                    } ?>                                                      
                        
                                          <td class="nomeMandante" style="padding-top: 15px;"><?PHP print $mandante; ?></td>
                                          <td class="distintivoMandante"> <img src="imagens/times/<?PHP print $fotoMandante; ?>" style="width: 30px;">  </td>
                                          <td class="placar" style="padding-top: 15px;"><?PHP print $placar; ?> </td>
                                          <td class="distintivoVisitante"> <img src="imagens/times/<?PHP print $fotoVisitante; ?>" style="width: 30px;">  </td>
                                          <td class="nomeVisitante" style="padding-top: 15px;"><?PHP print $visitante; ?></td>
                                    </tr>							
                              <?PHP 
                              } 
                        }  ?>
                  </table>
            </div>
      </div>
</div>

<!-- TABELA DO SEGUNDO TURNO -->
<div id="turno2" class="tabcontent"> 
      <div class="box">  
            <div class="box-body">
                  <table class="table table-striped"><?PHP

                        $rodadaAnterior = 0;
                        if (isset($rs2)) {
                              while($reg2=mysqli_fetch_array($rs2)) {
                                    $id = $reg2["id"];
                                    $grupo = $reg2["grupo"];
                                    $mandante = $reg2["mandante"];
                                    $visitante = $reg2["visitante"];
                                    $rodada = $reg2["rodada"];
                                    $turno = $reg2["turno"];
                                    $placar = " x ";
                                    $fotoMandante = $reg2["fotoMandante"];
                                    $fotoVisitante = $reg2["fotoVisitante"];
                                    $placarMandante = $reg2["placarMandante"];
                                    $placarVisitante = $reg2["placarVisitante"];
                                    $penaltiMandante = $reg2["penaltiMandante"];
                                    $penaltiVisitante = $reg2["penaltiVisitante"];

                                    if ($placarMandante != "" AND $placarVisitante != "") {
                                          $placar = $placarMandante." x ".$placarVisitante;
                                    }
                                    
                                    if ($rodadaAnterior != $rodada) { ?>                  
                                          <tr>
                                                <td></td>
                                                <td></td>
                                                <td>&nbsp;</td>
                                                <td></td>
                                                <td></td>  
                                          </tr>  <?php                              
                                    } 
                        
                                    $rodadaAnterior = $rodada;                          

                                    if ($ativo) {  ?>           
                                          <tr onclick="clickListener(this)" data-id="<?PHP print $id; ?>" style='cursor: pointer;'>  <?php 
                                    }
                                    else { ?> 
                                          <tr>  <?php 
                                    } ?>    
                                          <td class="nomeMandante" style="padding-top: 15px;"><?PHP print $mandante; ?></td>
                                          <td class="distintivoMandante"> <img src="imagens/times/<?PHP print $fotoMandante; ?>" style="width: 30px;">  </td>
                                          <td class="placar" style="padding-top: 15px;"><?PHP print $placar; ?></td>
                                          <td class="distintivoVisitante"> <img src="imagens/times/<?PHP print $fotoVisitante; ?>" style="width: 30px;">  </td>
                                          <td class="nomeVisitante" style="padding-top: 15px;"><?PHP print $visitante; ?></td>
                                    </tr>	  <?PHP 
                              } 
                        }  ?>
                  </table>
            </div>
      </div>       
</div>    

<!-- TABELA DA SEMI-FINAL -->
<div id="semi" class="tabcontent">      
      <?php  $rs3 = CarregaGridJogos($conexao, $campeonatoId, "3", "A"); ?>  
      
      <div class="box">
            <div class="box-body">
                  <table class="table table-striped"><?PHP  
                        if (isset($rs3)) {
                              while($reg3=mysqli_fetch_array($rs3)) {
                                    $id = $reg3["id"];
                                    $grupo = $reg3["grupo"];
                                    $mandante = $reg3["mandante"];
                                    $visitante = $reg3["visitante"];
                                    $rodada = $reg3["rodada"];
                                    $turno = $reg3["turno"];
                                    $placar = " x ";
                                    $fotoMandante = $reg3["fotoMandante"];
                                    $fotoVisitante = $reg3["fotoVisitante"];  
                                    $placarMandante = $reg3["placarMandante"];
                                    $placarVisitante = $reg3["placarVisitante"];
                                    $penaltiMandante = $reg3["penaltiMandante"];
                                    $penaltiVisitante = $reg3["penaltiVisitante"];

                                    if ($placarMandante != "" AND $placarVisitante != "") {
                                          if ($penaltiMandante != null) {
                                                $placar = 
                                                      $placarMandante."<span style='color:red;font-size: 12px;'>"." ". $penaltiMandante."</span>".
                                                      " x ".
                                                      "<span style='color:red;font-size: 12px;'>".$penaltiVisitante."</span>"." ".$placarVisitante;
                                          }
                                          else {
                                                $placar = $placarMandante." x ".$placarVisitante;
                                          }
                                    }
                                                                  
                                    if ($ativo) {  ?>           
                                          <tr onclick="clickListener(this)" data-id="<?PHP print $id; ?>" style='cursor: pointer;'>  <?php 
                                    }
                                    else { ?> 
                                          <tr>  <?php 
                                    } ?>     
                                          <td class="nomeMandante" style="padding-top: 15px;"><?PHP print $mandante; ?></td>
                                          <td class="distintivoMandante"> <img src="imagens/times/<?PHP print $fotoMandante; ?>" style="width: 30px;">  </td>
                                          <td class="placar" style="padding-top: 15px;"><?PHP print $placar; ?></td>
                                          <td class="distintivoVisitante"> <img src="imagens/times/<?PHP print $fotoVisitante; ?>" style="width: 30px;">  </td>
                                          <td class="nomeVisitante" style="padding-top: 15px;"><?PHP print $visitante; ?></td>
                                    </tr>							
                              <?PHP 
                              } 
                        }
                        else { ?>
                              <tr>                            
                                    <td class="nomeMandante" style="padding-top: 15px;">Time visitante</td>
                                    <td class="distintivoMandante"> <img src="imagens/times/sem_imagem.png" style="width: 30px;">  </td>
                                    <td class="placar" style="padding-top: 15px;"> x </td>
                                    <td class="distintivoVisitante"> <img src="imagens/times/<?PHP print $fotoVisitante; ?>" style="width: 30px;">  </td>
                                    <td class="nomeVisitante" style="padding-top: 15px;"><?PHP print $visitante; ?></td>
                              </tr>	

                              <tr>                            
                                    <td class="nomeMandante" style="padding-top: 15px;">Time mandante</td>
                                    <td class="distintivoMandante"> <img src="imagens/times/sem_imagem.png" style="width: 30px;">  </td>
                                    <td class="placar" style="padding-top: 15px;"> x </td>
                                    <td class="distintivoVisitante"> <img src="imagens/times/sem_imagem.png" style="width: 30px;">  </td>
                                    <td class="nomeVisitante" style="padding-top: 15px;">Time visitante</td>
                              </tr>	
                        
                        <?php
                        }  ?>
                  </table>
            </div>
      </div>
</div>

<!-- TABELA DA FINAL -->
<div id="final" class="tabcontent">
      <?php $rs4 = CarregaGridJogos($conexao, $campeonatoId, "4", "A"); ?>
      <div class="box">
            <div class="box-body">
                  <table class="table table-striped"> <?PHP
                        $rodadaAnterior = 0;
                        if (isset($rs4)) {
                              while($reg=mysqli_fetch_array($rs4)) {
                                    $id = $reg["id"];
                                    $grupo = $reg["grupo"];
                                    $mandante = $reg["mandante"];
                                    $visitante = $reg["visitante"];
                                    $rodada = $reg["rodada"];
                                    $turno = $reg["turno"];
                                    $placar = " x ";
                                    $fotoMandante = $reg["fotoMandante"];
                                    $fotoVisitante = $reg["fotoVisitante"]; 
                                    $placarMandante = $reg["placarMandante"];
                                    $placarVisitante = $reg["placarVisitante"];
                                    $penaltiMandante = $reg["penaltiMandante"];
                                    $penaltiVisitante = $reg["penaltiVisitante"];

                                    if ($placarMandante != "" AND $placarVisitante != "") {
                                          if ($penaltiMandante != null) {
                                                $placar = 
                                                      $placarMandante."<span style='color:red;font-size: 12px;'>"." ". $penaltiMandante."</span>".
                                                      " x ".
                                                      "<span style='color:red;font-size: 12px;'>".$penaltiVisitante."</span>"." ".$placarVisitante;
                                          }
                                          else {
                                                $placar = $placarMandante." x ".$placarVisitante;
                                          }
                                    } 
                                    
                                    if ($ativo) {  ?>           
                                          <tr onclick="clickListener(this)" data-id="<?PHP print $id; ?>" style='cursor: pointer;'>  <?php 
                                    }
                                    else { ?> 
                                          <tr>  <?php 
                                    } ?>                                 
                                          <td class="nomeMandante" style="padding-top: 15px;"><?PHP print $mandante; ?></td>
                                          <td class="distintivoMandante"> <img src="imagens/times/<?PHP print $fotoMandante; ?>" style="width: 30px;">  </td>
                                          <td class="placar" style="padding-top: 15px;"><?PHP print $placar; ?></td>
                                          <td class="distintivoVisitante"> <img src="imagens/times/<?PHP print $fotoVisitante; ?>" style="width: 30px;">  </td>
                                          <td class="nomeVisitante" style="padding-top: 15px;"><?PHP print $visitante; ?></td>
                                    </tr>							
                              <?PHP 
                              } 
                        }
                        else { ?>
                              <tr>                            
                                    <td class="nomeMandante" style="padding-top: 15px;">Time visitante</td>
                                    <td class="distintivoMandante"> <img src="imagens/times/sem_imagem.png">  </td>
                                    <td class="placar" style="padding-top: 15px;"> x </td>
                                    <td class="distintivoVisitante" ><img src="imagens/times/sem_imagem.png">  </td>
                                    <td class="nomeVisitante" style="padding-top: 15px;">Time mandante</td>
                              </tr>	                                  
                        
                        <?php
                        }  ?>
                  </table>
            </div>
      </div>     
</div>

<script>
      function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            
            for (i = 0; i < tabcontent.length; i++) {
                  tabcontent[i].style.display = "none";
            }
            
            tablinks = document.getElementsByClassName("tablinks");
            
            for (i = 0; i < tablinks.length; i++) {
                  tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
           
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
      }
            
      document.getElementById("defaultOpen").click();
</script>