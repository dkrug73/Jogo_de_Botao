<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
      include "../utils/funcoes.php";

      $descricaoTabelaJogos1 = "1º TURNO";
      $descricaoTabelaJogos2 = "2º TURNO";

      $campeonatoId = null;      
      if(isset($_COOKIE['campeonatoId'])) {
            $campeonatoId = $_COOKIE['campeonatoId'];
      }

      $campeonato = RetornaCampeonato($conexao, $campeonatoId);
      $campeonatoId = $campeonato[0];      

      if (isset($campeonatoId)) {    
            $doisGrupos = $campeonato[2];                 
            $doisTurnos = $campeonato[3];            
           
            if ($doisTurnos OR $doisGrupos) {
                  $rsTabela1 = CarregaGridTabela($conexao, $campeonatoId, "1");
                  $rsTabela2 = CarregaGridTabela($conexao, $campeonatoId, "2");
                  $rsTabelaGeral = CarregaGridTabela($conexao, $campeonatoId, "G");
            }
      }
?>

<body>  

      <?php 
      if (empty($campeonatoId)) { ?>
            <!-- tabela vazia --> 
            <div class = "box tabela">                            
                  <div class="box box-solid box-success">
                        <div class="cabecalhoTabela">
                        <h3 class="textoCabecalho">TABELA DE CLASSIFICAÇÃO</h3>
                  </div>
            
                  <div class="box-body">
                        <div class="box-body no-padding">
                              <table class="table tabela1" >
                                    <thead class="thead-default" >
                                          <tr class="cabecalho">
                                                <th class="linha">P</th>
                                                <th class="linha">Time</th>
                                                <th class="linha">PG</th>
                                                <th class="linha">V</th>
                                                <th class="linha">E</th>
                                                <th class="linha">D</th>
                                                <th class="linha">GP</th>
                                                <th class="linha">GC</th>
                                                <th class="linha">SG</th>
                                                <th class="linha">AP</th>
                                          </tr> 
                                    </thead>                     
                                                                                    
                                    <tr>       
                                          <td class="linha">1º</td>                  
                                          <td class="linha">Nome do Time</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0</td>
                                          <td class="linha">0%</td>
                                    </tr>	
                              </table>
                        </div>
                  </div>    
            </div>
            
            <?php
      
      } else { ?>
            <!-- tabela de classificação do 1 turno --> 
            <div class = "box tabela">                            
                  <div class="box box-solid box-success">
                        <div class="cabecalhoTabela">
                              <h3 class="textoCabecalho">CLASSIFICAÇÃO DO <?PHP print $descricaoTabelaJogos1 ?></h3>
                        </div>
            
                  <div class="box-body">
                        <div class="box-body no-padding">
                              <table class="table tabela1" >
                                    <thead class="thead-default" >
                                          <tr class="cabecalho">
                                                <th class="linha">P</th>
                                                <th class="linha">Time</th>
                                                <th class="linha">PG</th>
                                                <th class="linha">V</th>
                                                <th class="linha">E</th>
                                                <th class="linha">D</th>
                                                <th class="linha">GP</th>
                                                <th class="linha">GC</th>
                                                <th class="linha">SG</th>
                                                <th class="linha">AP</th>
                                          </tr> 
                                    </thead> <?php

                                    if (isset($rsTabela1)) {
                                          $posicao = 1;
                                          while($regTabela1=mysqli_fetch_array($rsTabela1)) {
                                                $timeNome = $regTabela1["timeNome"];
                                                $pontoGanho = $regTabela1["pontoGanho"];
                                                $vitoria = $regTabela1["vitoria"];
                                                $empate = $regTabela1["empate"];
                                                $derrota = $regTabela1["derrota"];
                                                $turnoGrupo = $regTabela1["turnoGrupo"];
                                                $golFavor = $regTabela1["golFavor"];
                                                $golContra = $regTabela1["golContra"]; 
                                                $saldo = $regTabela1["saldo"];

                                                $divisor = $pontoGanho * 100;
                                                $dividendo = ($vitoria + $derrota + $empate) * 3;
                                                
                                                $aproveitamento = 0;

                                                if ($dividendo > 0 And $divisor > 0) {
                                                      $aproveitamento = ($divisor / $dividendo) ;
                                                      $aproveitamento = round($aproveitamento, 0)."%"; // Onde 2 são as casas decimais 
                                                }  
                                                else {
                                                      $aproveitamento = $aproveitamento."%";
                                                }?>                          
                                                                              
                                                <tr >       
                                                      <td class="linha"><?PHP print $posicao; ?>º</td>                  
                                                      <td class="linha"><?PHP print $timeNome; ?></td>
                                                      <td class="linha"><?PHP print $pontoGanho; ?></td>
                                                      <td class="linha"><?PHP print $vitoria; ?></td>
                                                      <td class="linha"><?PHP print $empate; ?></td>
                                                      <td class="linha"><?PHP print $derrota; ?></td>
                                                      <td class="linha"><?PHP print $golFavor; ?></td>
                                                      <td class="linha"><?PHP print $golContra; ?></td>

                                                      <?php if ($saldo >= 0) {  ?>
                                                            <td class="linha"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }
                                                      else { ?>
                                                            <td  class="linha" style="color:red;"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }?> 

                                                      <td class="linha"><?PHP print $aproveitamento; ?></td>
                                                </tr>	  <?PHP 

                                                $posicao++;
                                          } 
                                    }  ?>

                              </table>
                        </div>
                  </div>
            </div>

            <!-- tabela de classificação do 2 turno -->   
            <div class = "box tabela">   

           

                  <div class="box box-solid box-success">
                        <div class="cabecalhoTabela">
                        <h3 class="textoCabecalho">CLASSIFICAÇÃO DO <?PHP print $descricaoTabelaJogos2 ?></h3>
                  </div>
                  
                  <div class="box-body">
                        <div class="box-body no-padding">
                              <table class="table tabela1" >
                                    <thead class="thead-default">
                                          <tr class="cabecalho">
                                                <th class="linha">P</th>
                                                <th class="linha">Time</th>
                                                <th class="linha">PG</th>
                                                <th class="linha">V</th>
                                                <th class="linha">E</th>
                                                <th class="linha">D</th>
                                                <th class="linha">GP</th>
                                                <th class="linha">GC</th>
                                                <th class="linha">SG</th>
                                                <th class="linha">AP</th>
                                          </tr> 
                                    </thead> <?php

                                    if (isset($rsTabela2)) {
                                          $posicao = 1;
                                          while($regTabela2=mysqli_fetch_array($rsTabela2)) {
                                                $timeNome = $regTabela2["timeNome"];
                                                $pontoGanho = $regTabela2["pontoGanho"];
                                                $vitoria = $regTabela2["vitoria"];
                                                $empate = $regTabela2["empate"];
                                                $derrota = $regTabela2["derrota"];
                                                $turnoGrupo = $regTabela2["turnoGrupo"];
                                                $golFavor = $regTabela2["golFavor"];
                                                $golContra = $regTabela2["golContra"]; 

                                                $saldo = $golFavor - $golContra;

                                                $divisor = $pontoGanho * 100;
                                                $dividendo = ($vitoria + $derrota + $empate) * 3;
                                                
                                                $aproveitamento = 0;

                                                if ($dividendo > 0 And $divisor > 0) {
                                                      $aproveitamento = ($divisor / $dividendo) ;
                                                      $aproveitamento = round($aproveitamento, 0)."%"; // Onde 2 são as casas decimais 
                                                }  
                                                else {
                                                      $aproveitamento = $aproveitamento."%";
                                                }?>                          
                                                                              
                                                <tr>       
                                                      <td class="linha"><?PHP print $posicao; ?>º</td>                  
                                                      <td class="linha"><?PHP print $timeNome; ?></td>
                                                      <td class="linha"><?PHP print $pontoGanho; ?></td>
                                                      <td class="linha"><?PHP print $vitoria; ?></td>
                                                      <td class="linha"><?PHP print $empate; ?></td>
                                                      <td class="linha"><?PHP print $derrota; ?></td>
                                                      <td class="linha"><?PHP print $golFavor; ?></td>
                                                      <td class="linha"><?PHP print $golContra; ?></td>
                                                      
                                                      <?php if ($saldo >= 0) {  ?>
                                                            <td class="linha"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }
                                                      else { ?>
                                                            <td class="linha" style="color:red;"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }?> 

                                                      <td class="linha"><?PHP print $aproveitamento; ?></td>
                                                </tr>	  <?PHP 

                                                $posicao++;
                                          } 
                                    }  ?>

                              </table>
                        </div>
                  </div><!-- /.box-body -->

                 
            
            </div>
            
            <!-- tabela de classificação GERAL -->
            <div class = "box tabela">                              
                  <div class="box box-solid box-success">
                        <div class="cabecalhoTabela">
                        <h3 class="textoCabecalho">CLASSIFICAÇÃO GERAL</h3>
                  </div>
                  
                  <div class="box-body">
                        <div class="box-body no-padding">
                              <table class="table tabela1" >
                                    <thead class="thead-default">
                                          <tr class="cabecalho">
                                                <th class="linha">P</th>
                                                <th class="linha">Time</th>
                                                <th class="linha">PG</th>
                                                <th class="linha">V</th>
                                                <th class="linha">E</th>
                                                <th class="linha">D</th>
                                                <th class="linha">GP</th>
                                                <th class="linha">GC</th>
                                                <th class="linha">SG</th>
                                                <th class="linha">AP</th>
                                          </tr> 
                                    </thead> <?php

                                    if (isset($rsTabelaGeral)) {
                                          $posicao = 1;
                                          while($regTabela2=mysqli_fetch_array($rsTabelaGeral)) {
                                                $timeNome = $regTabela2["timeNome"];
                                                $pontoGanho = $regTabela2["pontoGanho"];
                                                $vitoria = $regTabela2["vitoria"];
                                                $empate = $regTabela2["empate"];
                                                $derrota = $regTabela2["derrota"];
                                                $turnoGrupo = $regTabela2["turnoGrupo"];
                                                $golFavor = $regTabela2["golFavor"];
                                                $golContra = $regTabela2["golContra"]; 

                                                $saldo = $golFavor - $golContra;

                                                $divisor = $pontoGanho * 100;
                                                $dividendo = ($vitoria + $derrota + $empate) * 3;
                                                
                                                $aproveitamento = 0;

                                                if ($dividendo > 0 And $divisor > 0) {
                                                      $aproveitamento = ($divisor / $dividendo) ;
                                                      $aproveitamento = round($aproveitamento, 0)."%"; // Onde 2 são as casas decimais 
                                                }  
                                                else {
                                                      $aproveitamento = $aproveitamento."%";
                                                }?>                          
                                                                              
                                                <tr>       
                                                      <td class="linha"><?PHP print $posicao; ?>º</td>                  
                                                      <td class="linha"><?PHP print $timeNome; ?></td>
                                                      <td class="linha"><?PHP print $pontoGanho; ?></td>
                                                      <td class="linha"><?PHP print $vitoria; ?></td>
                                                      <td class="linha"><?PHP print $empate; ?></td>
                                                      <td class="linha"><?PHP print $derrota; ?></td>
                                                      <td class="linha"><?PHP print $golFavor; ?></td>
                                                      <td class="linha"><?PHP print $golContra; ?></td>
                                                      
                                                      <?php if ($saldo >= 0) {  ?>
                                                            <td class="linha"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }
                                                      else { ?>
                                                            <td  class="linha"style="color:red;"><?PHP print $saldo; ?></td>
                                                      <?php
                                                      }?> 

                                                      <td class="linha"><?PHP print $aproveitamento; ?></td>
                                                </tr>	  <?PHP 

                                                $posicao++;
                                          } 
                                    }  ?>

                              </table>
                        </div>
                  </div><!-- /.box-body -->
            
            </div>
     

            <?php 
      } ?>    



