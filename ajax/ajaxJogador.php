<?php
      SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";

      $campeonatoId = $_POST['campeonatoId']; 
      $timeId = $_POST['timeId']; 

      $sql="SELECT 
                  jogador.id,
                  CONCAT(campeonatos.descricao, ' - ', campeonatos.edicao, 'ª edição') AS campeonato, 
                  botonistas.nome AS botonista, 
                  jogador.nome as nomeJogador, 
                  jogador.numero, 
                  CASE jogador.titular 
                        WHEN '1' THEN ('Titular')
                        ELSE ('Reserva') END AS titular, 
                  jogador.foto, 
                  times.nome
            FROM 
                  jogador LEFT JOIN campeonatos ON jogador.campeonatoId = campeonatos.id LEFT JOIN 
                  times ON jogador.timeId = times.id LEFT JOIN 
                  botonistas ON times.botonistaId = botonistas.id 
            WHERE
                  jogador.campeonatoId = '".$campeonatoId."' AND
                  jogador.timeId = '".$timeId."' 
            ORDER BY 
                  botonistas.nome, 
                  times.nome,
                  jogador.numero, 
                  jogador.titular DESC";

      $rs=$conexao->query($sql);
?>

<div class="box-body">	
      <h3 class="box-title">Times</h3>
      <div class="box-tools">
            <div class="input-group" style="width: 150px;">	</div>
      </div>
      
      <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                  <tr>
                        <th>ID</th>								
                        <th>Nome</th>
                        <th>Número</th>
                        <th>Gols</th>
                        <th>Titular</th>								
                        <th>Botonista</th>
                        <th>Campeonato</th>
                        <th>Foto</th>
                  </tr>
                  
                  <?PHP
                  // Exibe os registros na tabela
                  while($reg=mysqli_fetch_array($rs)) 
                  {
                        $id = $reg["id"];
                        $nomeJogador = $reg["nomeJogador"];
                        $numero = $reg["numero"];
                        $titular = $reg["titular"]; 
                        $foto = $reg["foto"]; 
                        $botonista = $reg["botonista"]; 
                        $campeonato = $reg["campeonato"]; 

                        $gols = RetornaGolsJogador($conexao, $id, $campeonatoId);
                        
                        //if ($foto == "") $foto = "sem_imagem.png";?>
                                                      
                        <tr onclick="location.href = 'cadastroJogador.php?id=<?PHP print $id; ?>'; " style='cursor: pointer;'> 
                              
                              <td><?PHP print $id; ?></td>
                              <td><?PHP print $nomeJogador; ?></td>
                              <td><?PHP print $numero; ?></td>
                              <td><?PHP print $gols; ?></td>
                              <td><?PHP print $titular; ?></td>
                              <td><?PHP print $botonista; ?></td>	
                              <td><?PHP print $campeonato; ?></td>									
                              <td><img src = "imagens/jogador/<?PHP print $foto;?>" width = "25"></td>
                        </tr>							
                        <?PHP 
                  } ?>
            </table>
      </div>
</div>

